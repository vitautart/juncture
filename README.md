# JUNCTURE
Vulkan rendering engine

# Third Party Dependencies
All thirdparty dependencies are included inside *external* folder.
- GLFW https://github.com/glfw/glfw.git
- VMA https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator.git
