.PHONY: tests

CC = g++
STD = -std=c++20
CFILES = src/renderer.cpp src/surface.cpp
INCLUDE_PATHS = -Isrc -Iexternal/glfw/include -Iexternal/vma
LDLIBS =  -lvulkan -lglfw -ldl -lpthread
LIBSPATH = -Lbuild
DEFINITIONS = -DRENDER_DEBUG

tests1: tests/itest1_basic.cpp build/simple_trig_vert.spv build/simple_trig_frag.spv
	$(CC) $(STD) -ggdb tests/itest1_basic.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/itest1_basic
	./build/itest1_basic

build/simple_trig_vert.spv: tests/simple_trig.vert
	glslc tests/simple_trig.vert -o build/simple_trig_vert.spv
	
build/simple_trig_frag.spv: tests/simple_trig.frag
	glslc tests/simple_trig.frag -o build/simple_trig_frag.spv


tests2: tests/itest2_general.cpp build/simple_vert.spv build/simple_frag.spv
	$(CC) $(STD) -ggdb tests/itest2_general.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/itest2_general
	./build/itest2_general

build/simple_vert.spv: tests/simple.vert
	glslc tests/simple.vert -o build/simple_vert.spv
	
build/simple_frag.spv: tests/simple.frag
	glslc tests/simple.frag -o build/simple_frag.spv


tests3: tests/itest3_cube.cpp build/simple3d_vert.spv build/simple3d_frag.spv
	$(CC) $(STD) -ggdb tests/itest3_cube.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/itest3_cube
	./build/itest3_cube

build/simple3d_vert.spv: tests/simple3d.vert
	glslc tests/simple3d.vert -o build/simple3d_vert.spv
	
build/simple3d_frag.spv: tests/simple3d.frag
	glslc tests/simple3d.frag -o build/simple3d_frag.spv

tests4: tests/itest4_compute_readgpu.cpp build/simple2d_vert.spv build/simple2d_frag.spv build/mat_mul_comp.spv
	$(CC) $(STD) -ggdb tests/itest4_compute_readgpu.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/itest4_compute_readgpu
	./build/itest4_compute_readgpu


tests5: tests/itest5_offscreen.cpp build/simple_vert.spv build/simple_frag.spv build/simple3d_vert.spv build/simple3d_frag.spv
	$(CC) $(STD) -ggdb tests/itest5_offscreen.cpp $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/itest5_offscreen
	./build/itest5_offscreen


build/simple2d_vert.spv: tests/simple2d.vert
	glslc tests/simple2d.vert -o build/simple2d_vert.spv
	
build/simple2d_frag.spv: tests/simple2d.frag
	glslc tests/simple2d.frag -o build/simple2d_frag.spv

build/mat_mul_comp.spv: tests/mat_mul.comp
	glslc tests/mat_mul.comp -o build/mat_mul_comp.spv

clean:
	rm -f *.o *.a

