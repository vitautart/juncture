#ifndef JT_RENDERER_HPP
#define JT_RENDERER_HPP

#include "jtlib/codes.hpp"
#include "jtlib/vector.hpp"
#include "jtlib/datatypes.hpp"

#include "flags.hpp"

namespace jt
{
    // ------------ Forward declaration -------------
    struct NativeSurface;
    
    // ------------ Forward declaration -------------
    struct Pass;
    struct Shader;
    struct DescriptorLayout;
    struct DescriptorSet;
    struct Pipeline;
    struct Buffer;
    struct Image;
    struct CommandBuffer;
    struct Sampler;

    // ------------ Subtypes -------------
    enum class TargetType { COLOR, DEPTH, RESOLVE };
    template<enum TargetType> struct ImageTarget
    {
        const Image* ptr = nullptr;
        operator const Image*() noexcept { return ptr; }
    };

    struct PassOffscreen
    {
        const Pass* ptr = nullptr;
        operator const Pass*() noexcept { return ptr; }
    };

    struct PassOnscreen
    {
        const Pass* ptr = nullptr;
        operator const Pass*() noexcept { return ptr; }
    };

    // -----------------------------------
    // ------------ Helpers --------------
    // -----------------------------------
    struct Descriptor
    {
    // Ex. { .mBinding = 0, mCount = 1, .mType = DESCRIPTOR_TYPE_UNIFORM_BUFFER } 
    //          => layout (set = 0, binding = 0) uniform someData  { vec4 someVec1; vec4 someVec2 } someDataObj;
    // Ex. { .mBinding = 1, mCount = 1, .mType = DESCRIPTOR_TYPE_SAMPLER }
    //          => layout (set = 0, binding = 1) uniform sampler2D someSampler;
    // Ex. { .mBinding = 2, mCount = 8, .mType = DESCRIPTOR_TYPE_SAMPLED_IMAGE }
    //          => layout (set = 0, binding = 2) uniform texture2D someTextures[8];
        uint32_t mBinding;
        uint32_t mCount;
        flags::DescriptorType mTupe;
        flags::ShaderStageBits mUsingInStages;
    };

    struct VertexInputBinding
    {
        uint32_t mBinding;
        uint32_t mStride;
        bool mIsInstanced;
    };

    struct VertexInputAttribute
    {
        uint32_t mBinding;
        uint32_t mLocation;
        uint32_t mOffset;
        flags::Format mFormat;
    };

    struct Viewport
    {
        Position2D pos;
        Size2D size;
    };

    struct IndirectCommand 
    {
        uint32_t    mVertexCount;
        uint32_t    mInstanceCount;
        uint32_t    mFirstVertex;
        uint32_t    mFirstInstance;
    };

    struct IndexedIndirectCommand 
    {
        uint32_t    mIndexCount;
        uint32_t    mInstanceCount;
        uint32_t    mFirstIndex;
        int32_t     mVertexOffset;
        uint32_t    mFirstInstance;
    };

    struct BufferRegion
    {
        const Buffer* mBuffer;
        uint32_t mOffset;
        uint32_t mRange;
    };

    struct BufferRegionBarrier
    {
        BufferRegion mRegion;
        flags::MemoryAccessFlags mSrcAccess;
        flags::MemoryAccessFlags mDstAccess;
    };

    struct ImageData
    {
        const Image* mImage;
        const Sampler* mSampler;
    };

    struct ImageDataBarrier
    {
        const Image* mImage;
        flags::MemoryAccessFlags mSrcAccess;
        flags::MemoryAccessFlags mDstAccess;
        flags::ImageLayout mOldLayout;
        flags::ImageLayout mNewLayout;
    };

    struct TargetBlueprint
    {
        flags::ImageUsageFlags mUsage;
        flags::Format mFormat;
    };

    using DescriptorSetResources =  jt::vector<jt::variant<BufferRegion, ImageData>>;


    class Renderer
    {
    public:
        struct Impl;

        struct Parameters
        {
            bool anisotropyEnabled = false;
            bool multiDrawIndirectEnabled = false;
            bool vertexStoreAtomicallyBuffer = false;
            bool enableComputeDrawMix = false;
            bool enableComputeDedicated = false;
            // Number of frames that can be filled simultaneously while rendering in process
            // -1 - automaticaly detect number of frames that equal number of images that renderer uses
            int simultaneousFrames = -1;
        };

    public:
        Renderer(const Renderer& pRenderer) = delete;
        auto operator = (const Renderer& pRenderer) = delete;

        Renderer(Renderer&& pRenderer) noexcept;
        auto operator = (Renderer&& pRenderer) noexcept -> Renderer&;
        ~Renderer();

        static auto make(Size2D pSize, const NativeSurface& pNativeSurface, 
                const char* pAppName, const Parameters& pParameters) noexcept -> out<Renderer, Code>;

        // TODO: resize doesn't work without debugger warnings for X11 resizing, 
        // because for quick resizing we cannot quickly recreate all stuff.
        // TODO: we need resize also pipelines recreated with pDynamicViewport = false
        auto resize(Size2D pSize) noexcept -> Code;

        auto getSimultaneousFrameCount() const noexcept -> size_t;
        auto getAvailableMultisampling() const noexcept -> jt::vector<flags::Multisampling>;

        // TODO: add ability to create additional image 
        auto createPassOnscreen(bool pHasDepthImage, 
                flags::Multisampling pSampling = flags::Multisampling::MULTISAMPLING_1) 
                noexcept -> out<PassOnscreen, Code>;
        
        auto createPassOffscreen(Size2D pSize, TargetBlueprint pColorTarget,
                option<flags::ImageUsageFlags> pDepthTarget = {},
                option<pair<TargetBlueprint, flags::Multisampling>> pResolveTarget = {}) 
                noexcept -> out<PassOffscreen, Code>;
        
        auto createVertexShader(const char* pFilename) noexcept -> out<const Shader*, Code>;
        
        auto createFragmentShader(const char* pFilename) noexcept -> out<const Shader*, Code>;
        
        auto createComputeShader(const char* pFilename) noexcept -> out<const Shader*, Code>;
        
        auto createDescriptorLayout(const jt::vector<Descriptor>& pDescriptors) noexcept -> out<const DescriptorLayout*, Code>;

        auto createDescriptorSets(const jt::vector<const DescriptorLayout*>& pLayouts, const jt::vector<DescriptorSetResources>& pResources) noexcept -> out<jt::vector<const DescriptorSet*>, Code>;

        // TODO: add ability to use non-dynamic viewport config
        auto createPipelineGraphics(const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorLayouts, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> out<const Pipeline*, Code>;

        auto createPipelineCompute(const Shader* pCompShader, const jt::vector<const DescriptorLayout*>& pDescriptorLayouts) noexcept -> out<const Pipeline*, Code>;
        
        auto createBufferUnmapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Buffer*, Code>;
        
        auto createBufferMapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage, void** pMappedPtr = nullptr) noexcept -> out<const Buffer*, Code>;
        
        auto createImage1D(uint32_t pWidth, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>;
        
        auto createImage2D(Size2D pSize, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>;
        
        auto createImageColorTarget(Size2D pSize, TargetBlueprint pBlueprint,
                flags::Multisampling pSampling = flags::Multisampling::MULTISAMPLING_1) 
                noexcept -> out<ImageTarget<TargetType::COLOR>, Code>;

        auto createImageDepthTarget(Size2D pSize, flags::ImageUsageFlags pImageUsage,
                flags::Multisampling pSampling = flags::Multisampling::MULTISAMPLING_1) 
                noexcept -> out<ImageTarget<TargetType::DEPTH>, Code>;
        
        auto createImageResolveTarget(Size2D pSize, TargetBlueprint pBlueprint) 
            noexcept -> out<ImageTarget<TargetType::RESOLVE>, Code>;
        
        auto createImage3D(Size3D pSize, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>;
        
        /*auto write(const void* pSrcData, jt::tag<MappedBuffer, CPUAlloc> pDstBuffer, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte);
        auto write(const void* pSrcData, jt::tag<NonmappedBuffer, CPUAlloc> pDstBuffer, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte);*/
        auto writeBufferUnmapped(const void* pSrcData, const Buffer* pDstBufferCPU, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto writeBufferUnmapped(const jt::vector<IndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferUnmapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferMapped(const void* pSrcData, const Buffer* pDstBufferCPU, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto writeBufferMapped(const jt::vector<IndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferMapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto createCommandBuffer() noexcept -> out<const CommandBuffer*, Code>;
        auto createSampler(flags::SamplerFilter pMagFilter, flags::SamplerFilter pMinFilter, 
                flags::SamplerMipmapMode pMipmapMode, flags::SamplerAddressMode pAddressModeUVW[3], 
                bool pEnableAnisotropy, uint32_t mipMaps, flags::SamplerBorderColor pBorderColor) noexcept 
            -> out<const Sampler*, Code>;

        auto destroyShader(const Shader* pShader) noexcept -> void;

        // return current frame that is in range [0, getSimultaneousFramesCount())
        // and Code that can be OK, RENDERER_RESIZE_REQUIRE, RENDERER_ERROR
        auto drawBegin() noexcept -> out<uint32_t, Code>;
        auto addCommandBuffer(const CommandBuffer* pCommandBuffer) noexcept ->void;
        auto drawSubmitWait() noexcept -> Code;
        auto drawSubmitLeave() noexcept -> Code;

        auto computeBegin() noexcept -> Code;
        auto computeSubmitWait() noexcept -> Code;

        auto recordCmdBegin(const CommandBuffer* pCBuffer) noexcept -> void;
        auto recordCmdEnd(const CommandBuffer* pCBuffer) noexcept -> void;

        auto cmdPassBegin(const CommandBuffer* pCBuffer, PassOnscreen pPass, float pClearColor [4]) noexcept -> void;
        auto cmdPassBegin(const CommandBuffer* pCBuffer, PassOffscreen pPass, float pClearColor [4],
                ImageTarget<TargetType::COLOR> pColorTarget, 
                jt::option<ImageTarget<TargetType::DEPTH>> pDepthTarget = {}, 
                jt::option<ImageTarget<TargetType::RESOLVE>> pResolveTarget = {}) noexcept -> void;
        auto cmdPassEnd(const CommandBuffer* pCBuffer, const Pass* pPass) noexcept -> void;

        auto cmdBindPipeline(const CommandBuffer* pCBuffer, const Pipeline* pPipeline) noexcept -> void;
        // TODO: reconsider use descriptor templates
        auto cmdBindDescriptorSets(const CommandBuffer* pCBuffer, const Pipeline* pPipeline, const jt::vector<const DescriptorSet*>& pDescriptorSets, const jt::vector<uint32_t>& pDynamicOffsets) noexcept -> void;
        auto cmdBindVertexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte) noexcept -> void;
        auto cmdBindIndexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, flags::IndexType pType) noexcept -> void;

        // This method contain implicit barrier
        auto cmdLoadVertexData(const CommandBuffer* pCBuffer, const Buffer* pSrcBufferCPU, const Buffer* pDstBufferGPU, size_t pSrcOffsetByte = 0, size_t pDstOffsetByte = 0, size_t pCopySizeByte = SIZE_MAX) noexcept -> void;
        // This method contain implicit barrier
        auto cmdLoadIndexData(const CommandBuffer* pCBuffer, const Buffer* pSrcBufferCPU, const Buffer* pDstBufferGPU, size_t pSrcOffsetByte = 0, size_t pDstOffsetByte = 0, size_t pCopySizeByte = SIZE_MAX) noexcept -> void;
        // This method contain implicit barriers
        auto cmdLoadTextureData(const CommandBuffer* pCBuffer, const Buffer* pSrcBufferCPU, const Image* pDstImage) noexcept -> void;
        // Need to be synchronized with barrier
        auto cmdCopyBuffer(const CommandBuffer* pCBuffer, const Buffer* pSrcBuffer, const Buffer* pDstBuffer, size_t pSrcOffsetByte = 0, size_t pDstOffsetByte = 0, size_t pCopySizeByte = SIZE_MAX) noexcept -> void;

        auto cmdDraw(const CommandBuffer* pCBuffer, uint32_t pVertexCount, uint32_t pInstanceCount, uint32_t pFirstVertex = 0, uint32_t pFirstInstance = 0) noexcept -> void;
        auto cmdDrawIndexed(const CommandBuffer* pCBuffer, uint32_t pIndexCount, uint32_t pInstanceCount, uint32_t pFirstIndex = 0, int32_t pVertexOffset = 0, uint32_t pFirstInstance = 0) noexcept -> void;
        // TODO: test it!
        auto cmdDrawIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void;
        auto cmdDrawIndexedIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void;
        auto cmdDispatch(const CommandBuffer* pCBuffer, uint32_t pGroupCountX, uint32_t pGroupCountY, uint32_t pGroupCountZ) noexcept -> void;

        auto cmdSetViewport(const CommandBuffer* pCBuffer, const Viewport& pViewport) noexcept -> void;

        auto cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                uint32_t pBuffersCount, const BufferRegionBarrier* pBuffers,
                uint32_t pImageCount, const ImageDataBarrier* pImages) noexcept -> void;
        auto cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                const jt::vector<BufferRegionBarrier>& pBuffers,
                const jt::vector<ImageDataBarrier>& pImages) noexcept -> void;
        //auto cmdBarrierTransferToCompute();
        //auto cmdBarrierComputeToTransfer();
        //auto cmdBarrierComputeToCompute();
        //auto cmdBarrierComputeToVertex();
        //auto cmdBarrierComputeToFragment();
    private:
        Renderer() = default;
        Impl* mImpl = nullptr;
    };
}

#endif // JT_RENDERER_HPP
