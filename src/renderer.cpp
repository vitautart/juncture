#include "renderer.hpp"
#include "flags.hpp"
#include "jtlib/datatypes.hpp"
#include "jtlib/print.hpp"
#include "native_surface.hpp"

#include "jtlib/allocator.hpp"
#include "jtlib/codes.hpp"
#include "jtlib/vector.hpp"
#include "jtlib/unique_ptr.hpp"
#include "jtlib/filesystem.hpp"
#include "jtlib/map.hpp"

#include <algorithm>
#include <alloca.h>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <exception>
#include <new>
#include <ratio>
#include <thread>
#include <chrono>

#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>
#include <vulkan/vulkan_xlib.h>
#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#include <memory>
#include <cassert>
#include <cstdio>

#define CHECK_VK(expression) {auto res = expression; if (res != VK_SUCCESS){ assert(false); return jt::Code::RENDERER_ERROR; }}

//#define GET_VK_CB(cb) cb->mBuffers[mSwapchain.mCurrentImage]
#define GET_VK_CB(cb) cb->mBuffer

namespace
{
    VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT pSeverity, 
            VkDebugUtilsMessageTypeFlagsEXT pType, 
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, 
            void* pUserData) noexcept
    {
        printf("validation layer: %s\n", pCallbackData->pMessage);
        if (pSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
            assert(false);
        return VK_FALSE;
    }
}

using Vulkan = jt::Renderer::Impl;
template<typename T>
using vkOut = jt::out<T, VkResult>;

namespace jt
{


    struct Renderer::Impl
    {
        auto create(Size2D pSize, const NativeSurface& pNativeSurface, const char* pAppName, const Parameters& pParameters) noexcept -> jt::Code;
        auto createPassOnscreen(bool pHasDepthImage, flags::Multisampling pMultisampling) noexcept -> out<PassOnscreen, Code>;
        auto createPassOffscreen(Size2D pSize, TargetBlueprint pColorTarget,
            option<flags::ImageUsageFlags> pDepthTarget,
            option<pair<TargetBlueprint, flags::Multisampling>> pResolveTarget) 
            noexcept -> out<PassOffscreen, Code>;
        
        auto createVertexShader(const char* pFilename) noexcept -> jt::out<const Shader*, Code>;
        auto createFragmentShader(const char* pFilename) noexcept -> jt::out<const Shader*, Code>;
        auto createComputeShader(const char* pFilename) noexcept -> jt::out<const Shader*, Code>;
        auto createDescriptorLayout(const jt::vector<Descriptor>& pDescriptors) noexcept -> out<const DescriptorLayout*, Code>;
        auto createDescriptorSets(const jt::vector<const DescriptorLayout*>& pLayout, const jt::vector<DescriptorSetResources>& pResources) noexcept -> out<jt::vector<const DescriptorSet*>, Code>;
        auto createDescriptorPoolHelper(const jt::vector<const DescriptorLayout*>& pLayout) noexcept -> out<VkDescriptorPool, Code>;
        auto createPipelineGraphics (const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorSets, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> out<const Pipeline*, Code>;
        auto createPipelineCompute(const Shader* pCompShader, const jt::vector<const DescriptorLayout*>& pDescriptorLayouts) noexcept -> out<const Pipeline*, Code>;
        auto createBufferUnmapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, 
                flags::MemoryUsageType pMemoryUsage) noexcept -> jt::out<const Buffer*, Code>;
        auto createBufferMapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage, void** pMappedPtr) noexcept -> out<const Buffer*, Code>;
        auto createImage(const VkExtent3D& pExtent, VkImageType pImageType, VkImageViewType pImageViewType, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> jt::out<const Image*, Code>;
        auto createImageTarget(const VkExtent3D& pExtent, VkFormat pFormat, VkImageUsageFlags pImageUsage, VkImageAspectFlags pAspectBits, flags::Multisampling pSampling) noexcept -> out<const Image*, Code>;

        auto createSampler(flags::SamplerFilter pMagFilter, flags::SamplerFilter pMinFilter, flags::SamplerMipmapMode pMipmapMode, flags::SamplerAddressMode pAddressModeUVW[3], bool pEnableAnisotropy, uint32_t mipMaps, flags::SamplerBorderColor pBorderColor) noexcept -> out<const Sampler*, Code>;

        auto destroyShader(const Shader* pShader) noexcept -> void;

        auto writeBufferUnmapped(const void* pSrcData, const Buffer* pDstBufferCPU, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto writeBufferUnmapped(const jt::vector<IndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferUnmapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferMapped(const void* pSrcData, const Buffer* pDstBufferCPU, 
                size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto writeBufferMapped(const jt::vector<IndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        auto writeBufferMapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, 
                const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, 
                size_t pCopyCommandCount) noexcept -> void;
        //auto loadDataToGPU (const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU) noexcept -> Code;
        auto createCommandBuffer() noexcept -> out<const CommandBuffer*, Code>;
        auto resize(Size2D pSize) noexcept -> jt::Code;
        //auto recursivlyResize(jt::Code& pCode) noexcept -> bool;
        auto destroy() noexcept -> void;

        auto getAvailableMultisampling() const noexcept -> jt::vector<flags::Multisampling>;

        auto getInstanceExtensions() const noexcept -> const jt::vector<const char *>;
        auto getDeviceExtensions() const noexcept -> const jt::vector<const char *>;
        auto getLayers() const noexcept -> const jt::vector<const char *>;
        auto getDepthFormatProposals() const noexcept ->const jt::vector<VkFormat>;

        auto createInstance(const char* appName) noexcept -> VkResult;
        auto createSurface(const NativeSurface& pNativeSurface) noexcept -> VkResult;
        auto createDebugger() noexcept -> VkResult;
        auto selectPhysicalDeviceAndQueueIds() noexcept ->VkResult;
        auto createDeviceAndQueues() noexcept -> VkResult;
        auto createAllocator() noexcept -> VkResult;
        auto createCommandPool() noexcept ->VkResult;
        auto prepareSwapchainStatic() noexcept -> VkResult;
        auto prepareSwapchainDynamic(Size2D pSize) noexcept -> VkResult;
        auto createSwapchain() noexcept -> VkResult;
        auto createFramesSynchronization() noexcept -> VkResult;
        auto destroyDepthImage() noexcept -> void;
        auto destroyFramesSynchronization() noexcept -> void;
        auto destroyDebugger() noexcept -> void;
        auto destroySwapchain() noexcept -> void;

        auto genDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> VkResult;
        auto destroyDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> void;
        auto hasDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) const noexcept -> bool;
        auto genColorTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> VkResult;
        auto destroyColorTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> void;
        auto hasColorTargetImageFor(VkSampleCountFlagBits pMultisampling) const noexcept -> bool;

        auto drawBegin() noexcept -> out<uint32_t, Code>;
        auto addCommandBuffer(const CommandBuffer* pCommandBuffer) noexcept ->void;
        auto drawSubmitWait() noexcept -> Code;
        auto drawSubmitLeave() noexcept -> Code;

        auto computeBegin() noexcept -> Code;
        auto computeSubmitWait() noexcept -> Code;

        auto recordCmdBegin(const CommandBuffer* pCBuffer) noexcept -> void;
        auto recordCmdEnd(const CommandBuffer* pCBuffer) noexcept -> void;

        auto cmdPassBegin(const CommandBuffer* pCBuffer, PassOnscreen pPass, float pClearColor[4]) noexcept -> void;
        auto cmdPassBegin(const CommandBuffer* pCBuffer, PassOffscreen pPass, float pClearColor [4],
                ImageTarget<TargetType::COLOR> pColorTarget, 
                jt::option<ImageTarget<TargetType::DEPTH>> pDepthTarget = {}, 
                jt::option<ImageTarget<TargetType::RESOLVE>> pResolveTarget = {}) noexcept -> void;
        auto cmdPassEnd(const CommandBuffer* pCBuffer, const Pass* pPass) noexcept -> void;

        auto cmdBindPipeline(const CommandBuffer* pCBuffer, const Pipeline* pPipeline) noexcept -> void;
        auto cmdBindDescriptorSets(const CommandBuffer* pCBuffer, const Pipeline* pPipeline, const jt::vector<const DescriptorSet*>& pDescriptorSets, const jt::vector<uint32_t>& pDynamicOffsets) noexcept -> void;
        auto cmdBindVertexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte) noexcept -> void;
        auto cmdBindIndexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, flags::IndexType pType) noexcept -> void;

        auto cmdLoadVertexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto cmdLoadIndexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;
        auto cmdLoadTextureData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Image* pOutputBufferGPU) noexcept -> void;
        
        auto cmdCopyBuffer(const CommandBuffer* pCBuffer, const Buffer* pSrcBuffer, const Buffer* pDstBuffer, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void;

        auto cmdDraw(const CommandBuffer* pCBuffer, uint32_t pVertexCount, uint32_t pInstanceCount, uint32_t pFirstVertex, uint32_t pFirstInstance) noexcept -> void;
        auto cmdDrawIndexed(const CommandBuffer* pCBuffer, uint32_t pIndexCount, uint32_t pInstanceCount, uint32_t pFirstIndex, int32_t pVertexOffset, uint32_t pFirstInstance) noexcept -> void;
        auto cmdDrawIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void;
        auto cmdDrawIndexedIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void;
        auto cmdDispatch(const CommandBuffer* pCBuffer, uint32_t pGroupCountX, uint32_t pGroupCountY, uint32_t pGroupCountZ) noexcept -> void;
        auto cmdSetViewport(const CommandBuffer* pCBuffer, const Viewport& pViewport) noexcept -> void;
        auto cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                uint32_t pBuffersCount, const BufferRegionBarrier* pBuffers,
                uint32_t pImageCount, const ImageDataBarrier* pImages) noexcept -> void;

        static constexpr VkFormat sMapFormat[flags::Format::FORMAT_MAX] = 
        {
            VK_FORMAT_R8_UNORM,
            VK_FORMAT_R8_SNORM,
            VK_FORMAT_R8_USCALED,
            VK_FORMAT_R8_SSCALED,
            VK_FORMAT_R8_UINT,
            VK_FORMAT_R8_SINT,
            VK_FORMAT_R8_SRGB,
            VK_FORMAT_R8G8_UNORM,
            VK_FORMAT_R8G8_SNORM,
            VK_FORMAT_R8G8_USCALED,
            VK_FORMAT_R8G8_SSCALED,
            VK_FORMAT_R8G8_UINT,
            VK_FORMAT_R8G8_SINT,
            VK_FORMAT_R8G8_SRGB,
            VK_FORMAT_R8G8B8_UNORM,
            VK_FORMAT_R8G8B8_SNORM,
            VK_FORMAT_R8G8B8_USCALED,
            VK_FORMAT_R8G8B8_SSCALED,
            VK_FORMAT_R8G8B8_UINT,
            VK_FORMAT_R8G8B8_SINT,
            VK_FORMAT_R8G8B8_SRGB,
            VK_FORMAT_R8G8B8A8_UNORM,
            VK_FORMAT_R8G8B8A8_SNORM,
            VK_FORMAT_R8G8B8A8_USCALED,
            VK_FORMAT_R8G8B8A8_SSCALED,
            VK_FORMAT_R8G8B8A8_UINT,
            VK_FORMAT_R8G8B8A8_SINT,
            VK_FORMAT_R8G8B8A8_SRGB,
            VK_FORMAT_R16_UNORM,
            VK_FORMAT_R16_SNORM,
            VK_FORMAT_R16_USCALED,
            VK_FORMAT_R16_SSCALED,
            VK_FORMAT_R16_UINT,
            VK_FORMAT_R16_SINT,
            VK_FORMAT_R16_SFLOAT,
            VK_FORMAT_R16G16_UNORM,
            VK_FORMAT_R16G16_SNORM,
            VK_FORMAT_R16G16_USCALED,
            VK_FORMAT_R16G16_SSCALED,
            VK_FORMAT_R16G16_UINT,
            VK_FORMAT_R16G16_SINT,
            VK_FORMAT_R16G16_SFLOAT,
            VK_FORMAT_R16G16B16_UNORM,
            VK_FORMAT_R16G16B16_SNORM,
            VK_FORMAT_R16G16B16_USCALED,
            VK_FORMAT_R16G16B16_SSCALED,
            VK_FORMAT_R16G16B16_UINT,
            VK_FORMAT_R16G16B16_SINT,
            VK_FORMAT_R16G16B16_SFLOAT,
            VK_FORMAT_R16G16B16A16_UNORM,
            VK_FORMAT_R16G16B16A16_SNORM,
            VK_FORMAT_R16G16B16A16_USCALED,
            VK_FORMAT_R16G16B16A16_SSCALED,
            VK_FORMAT_R16G16B16A16_UINT,
            VK_FORMAT_R16G16B16A16_SINT,
            VK_FORMAT_R16G16B16A16_SFLOAT,
            VK_FORMAT_R32_UINT,
            VK_FORMAT_R32_SINT,
            VK_FORMAT_R32_SFLOAT,
            VK_FORMAT_R32G32_UINT,
            VK_FORMAT_R32G32_SINT,
            VK_FORMAT_R32G32_SFLOAT,
            VK_FORMAT_R32G32B32_UINT,
            VK_FORMAT_R32G32B32_SINT,
            VK_FORMAT_R32G32B32_SFLOAT,
            VK_FORMAT_R32G32B32A32_UINT,
            VK_FORMAT_R32G32B32A32_SINT,
            VK_FORMAT_R32G32B32A32_SFLOAT,
        };

        static constexpr inline auto map(const flags::PrimitiveTopology) noexcept -> VkPrimitiveTopology;
        static constexpr inline auto map(const flags::PolygonMode) noexcept -> VkPolygonMode;
        static constexpr inline auto map(const flags::CullMode) noexcept -> VkCullModeFlagBits;
        static constexpr inline auto map(const flags::MemoryUsageType) noexcept -> VmaMemoryUsage;
        static constexpr inline auto map(const flags::BufferUsageFlagBits) noexcept -> VkBufferUsageFlags;
        static constexpr inline auto map(const flags::ImageUsageFlagBits) noexcept -> VkImageUsageFlags;
        static constexpr inline auto map(const flags::SamplerFilter) noexcept -> VkFilter;
        static constexpr inline auto map(const flags::SamplerMipmapMode) noexcept -> VkSamplerMipmapMode;
        static constexpr inline auto map(const flags::SamplerAddressMode) noexcept -> VkSamplerAddressMode;
        static constexpr inline auto map(const flags::SamplerBorderColor) noexcept -> VkBorderColor;
        static constexpr inline auto map(const flags::DescriptorType) noexcept -> VkDescriptorType;
        static constexpr inline auto map(const flags::IndexType) noexcept -> VkIndexType;
        static constexpr inline auto map(const flags::Multisampling) noexcept -> VkSampleCountFlagBits;
        static constexpr inline auto map(const flags::Format) noexcept -> VkFormat;

        Parameters mParameters;
        VkInstance mInstance = VK_NULL_HANDLE;
        VkDevice mDevice = VK_NULL_HANDLE;
        VkPhysicalDevice mPhysicalDevice = VK_NULL_HANDLE;
        VmaAllocator mAllocator = VK_NULL_HANDLE;
        VkQueue mQueueGraphic = VK_NULL_HANDLE;
        VkQueue mQueuePresent = VK_NULL_HANDLE;
        VkQueue mQueueCompute = VK_NULL_HANDLE;
        uint32_t mQueueGraphicId;
        uint32_t mQueuePresentId;
        uint32_t mQueueComputeId;
        VkDebugUtilsMessengerEXT mDebugMessenger = VK_NULL_HANDLE;
        VkSurfaceKHR mSurface = VK_NULL_HANDLE;
        VkCommandPool mCommandPool = VK_NULL_HANDLE;

        jt::map<VkSampleCountFlagBits, Image> mColorTargets;
        jt::map<VkSampleCountFlagBits, Image> mDepthTargets;

        struct Swapchain
        {
            VkSwapchainKHR mObject = VK_NULL_HANDLE;

            jt::vector<VkImage> mImages;
            jt::vector<VkImageView> mViews;
            VkFormat mFormat;
            VkFormat mDepthFormat;
            VkColorSpaceKHR mColorSpace;
            VkExtent2D mExtent;
            VkPresentModeKHR mPresentMode;
            VkSurfaceTransformFlagBitsKHR mPreTransform;
            uint32_t mMinImageCount;
            uint32_t mCurrentImage;
        } mSwapchain;

        struct Frames
        {
            jt::vector<VkSemaphore> mSemaphoresSubmitToPresent;
            jt::vector<VkSemaphore> mSemaphoresAcquireToSubmit;
            jt::vector<VkFence> mFencesPerFrames;
            jt::vector<VkFence> mFencesPerImage;
            uint32_t mCurrentFrame = 0;
            size_t mSimultaneousFrames = 1;

            auto getCurrentAcquireToSubmitSemaphore() noexcept -> VkSemaphore*
            {
                return &mSemaphoresAcquireToSubmit[mCurrentFrame];
            }
            auto getCurrentSubmitToPresentSemaphore() noexcept -> VkSemaphore*
            {
                return &mSemaphoresSubmitToPresent[mCurrentFrame];
            }
            auto getFenceForCurrentFrame() noexcept -> VkFence*
            {
                return &mFencesPerFrames[mCurrentFrame];
            }
            auto getFenceForCurrentImage(size_t pImageId) const noexcept -> const VkFence*
            {
                return &mFencesPerImage[pImageId];
            }
            auto setFenceForCurrentImage(const VkFence* pFence, size_t pImageId) noexcept -> void
            {
                mFencesPerImage[pImageId] = *pFence;
            }
            auto nextCurrentFrame() noexcept -> void
            {
                mCurrentFrame = (mCurrentFrame + 1) % mSimultaneousFrames;
            }
        } mFrames;

        struct DescriptorResource
        {
            auto destroy (const Renderer::Impl& pRenderer) noexcept -> void;
            VkDescriptorPool mPool;
            jt::vector<std::unique_ptr<DescriptorSet>> mSets;
            bool mIsFreeDescriptorSetsEnabled = false;
        };

        // RESOURCES
        jt::vector<std::unique_ptr<Pass>> mRenderPassesOnscreen;
        jt::vector<std::unique_ptr<Pass>> mRenderPassesOffscreen;
        jt::map<const Shader*, std::unique_ptr<Shader>> mShaders;
        jt::vector<std::unique_ptr<Pipeline>> mPipelines;
        jt::vector<std::unique_ptr<DescriptorResource>> mDescriptorResources;
        jt::vector<std::unique_ptr<DescriptorLayout>> mDescriptorLayouts;
        jt::vector<std::unique_ptr<Buffer>> mBuffers;
        jt::vector<std::unique_ptr<Image>> mImages;
        jt::vector<std::unique_ptr<CommandBuffer>> mCommandBuffers;
        jt::vector<std::unique_ptr<Sampler>> mSamplers;

        // CURRENT TRANSIENT DATA
        jt::vector<VkCommandBuffer> mCurrentCommandBuffers;
        jt::vector<VkCommandBuffer> mCurrentCommandBuffersCompute;
        enum class DrawState
        {
            NOT_BEGIN_EVEN_ONCE,
            BEGIN,
            BEGIN_COMPUTE,
            END
        } mCurrentDrawState = DrawState::NOT_BEGIN_EVEN_ONCE;
    };

    struct Image
    {
        VkImage mImage = VK_NULL_HANDLE;
        VkImageView mView = VK_NULL_HANDLE;
        VmaAllocation mAllocation = VK_NULL_HANDLE;
        VkExtent3D mSize;
        VkFormat mFormat;
        uint32_t mMipLevels;
        uint32_t mArrayLayers;
    };

    struct Pass
    {
        /*auto create(const Renderer::Impl& pRenderer, bool pIsOffscreen,
                Size2D pSize, TargetBlueprint pColorTarget,
                option<flags::ImageUsageFlags> pDepthTarget,
                option<TargetBlueprint> pResolveTarget,
                flags::Multisampling pSampling) noexcept -> jt::Code;*/
        auto resize(const Renderer::Impl& pRenderer, Size2D pSize) noexcept -> jt::Code;
        auto destroy(const Renderer::Impl& pRenderer) noexcept -> void;

        auto createRenderPassObject(const Renderer::Impl& pRenderer, bool pIsOffscreen) noexcept -> VkResult;
        auto createFramebuffers(const Renderer::Impl& pRenderer, Size2D pSize) noexcept -> VkResult;
        auto destroyRenderPassObject(const Renderer::Impl& pRenderer) -> void;
        auto destroyFramebuffers(const Renderer::Impl& pRenderer) -> void;

        VkRenderPass mRenderPass = VK_NULL_HANDLE;
        VkFramebuffer mFramebuffer = VK_NULL_HANDLE;
        VkSampleCountFlagBits mMultisampling = VK_SAMPLE_COUNT_1_BIT;
        VkImageUsageFlags mColorTargetUsage = 0;
        VkImageUsageFlags mDepthTargetUsage = 0;
        VkImageUsageFlags mResolveTargetUsage = 0;
        VkFormat mColorFormat;
        VkFormat mDepthFormat;
        VkFormat mResolveFormat;
        bool mHasDepthImage = false;
        bool mHasMultisampledColorImage = false;
        bool mIsOnscreen = true;
    };

    struct Shader
    {
        auto create (const Renderer::Impl& pRenderer, const char* pFilename, VkShaderStageFlagBits pStage) noexcept -> jt::Code;
        VkShaderModule mModule = VK_NULL_HANDLE;
        VkShaderStageFlagBits mStage;
    };

    struct DescriptorSet
    {
        VkDescriptorSet mSet = VK_NULL_HANDLE;
    };

    struct DescriptorLayout
    {
        VkDescriptorSetLayout mLayout = VK_NULL_HANDLE;
        jt::vector<Descriptor> mDescriptors;
    };

    struct Pipeline
    {
        auto create(const Renderer::Impl& pRenderer, const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorSets, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> VkResult;
        VkPipeline mPipeline = VK_NULL_HANDLE;
        VkPipelineLayout mLayout = VK_NULL_HANDLE;
        VkPipelineBindPoint mBindPoint;
    };

    struct Buffer
    {
        VkBuffer mBuffer = VK_NULL_HANDLE;
        VmaAllocation mAllocation = VK_NULL_HANDLE;
        VkDeviceSize mSize = 0;
        void* mMappedPtr = nullptr;
    };

    struct CommandBuffer
    {
        VkCommandBuffer mBuffer = VK_NULL_HANDLE;
    };

    struct Sampler
    {
        VkSampler mSampler = VK_NULL_HANDLE;
    };
}

constexpr inline auto Vulkan::map(const flags::PrimitiveTopology pTopo) noexcept -> VkPrimitiveTopology
{
    switch(pTopo)
    {
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_POINT_LIST:
            return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_LINE_LIST:
            return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_LINE_STRIP:
            return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_FAN:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY:
            return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY:
            return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY:
            return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
        case flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_PATCH_LIST:
            return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
    }

    assert(false);
    return VK_PRIMITIVE_TOPOLOGY_MAX_ENUM;
}

constexpr inline auto Vulkan::map(const flags::PolygonMode pMode) noexcept -> VkPolygonMode
{
    switch(pMode)
    {
        case flags::PolygonMode::POLYGON_MODE_FILL:
            return VK_POLYGON_MODE_FILL;
        case flags::PolygonMode::POLYGON_MODE_LINE:
            return VK_POLYGON_MODE_LINE;
        case flags::PolygonMode::POLYGON_MODE_POINT:
            return VK_POLYGON_MODE_POINT;
    }
    assert(false);
    return VK_POLYGON_MODE_MAX_ENUM;
}

constexpr inline auto Vulkan::map(const flags::CullMode pMode) noexcept -> VkCullModeFlagBits
{
    switch(pMode)
    {
        case flags::CullMode::CULL_MODE_NONE:
            return VkCullModeFlagBits::VK_CULL_MODE_NONE;
        case flags::CullMode::CULL_MODE_FRONT:
            return VkCullModeFlagBits::VK_CULL_MODE_FRONT_BIT;
        case flags::CullMode::CULL_MODE_BACK:
            return VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT;
        case flags::CullMode::CULL_MODE_BOTH:
            return VkCullModeFlagBits::VK_CULL_MODE_FRONT_AND_BACK;
    }

    assert(false);
    return VkCullModeFlagBits::VK_CULL_MODE_NONE;
}

constexpr inline auto Vulkan::map(const flags::MemoryUsageType pType) noexcept -> VmaMemoryUsage
{
    switch(pType)
    {
        case flags::MemoryUsageType::MEMORY_TYPE_CPU: 
            return VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY;
        case flags::MemoryUsageType::MEMORY_TYPE_GPU:
            return VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY;
        case flags::MemoryUsageType::MEMORY_TYPE_GPU_TO_CPU:
            return VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_TO_CPU;
        case flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU:
            return VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU;
    };

    assert(false);
    return VmaMemoryUsage::VMA_MEMORY_USAGE_UNKNOWN;
}

constexpr inline auto Vulkan::map(const flags::BufferUsageFlagBits pFlags) noexcept -> VkBufferUsageFlags
{
    VkBufferUsageFlags output = 0;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_DST & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_STORAGE & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_INDEX & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_VERTEX & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    if ((uint32_t)flags::BufferUsageFlagBits::BUFFER_USAGE_INDIRECT & (uint32_t)pFlags)
        output |= VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
    return output; 
}

constexpr inline auto Vulkan::map(const flags::ImageUsageFlagBits pFlags) noexcept -> VkImageUsageFlags
{
    VkImageUsageFlags output = 0;
    if ((uint32_t)flags::ImageUsageFlagBits::IMAGE_USAGE_TRANSFER_SRC & pFlags)
        output |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    if ((uint32_t)flags::ImageUsageFlagBits::IMAGE_USAGE_TRANSFER_DST & pFlags)
        output |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    if ((uint32_t)flags::ImageUsageFlagBits::IMAGE_USAGE_SAMPLED & pFlags)
        output |= VK_IMAGE_USAGE_SAMPLED_BIT;
    if ((uint32_t)flags::ImageUsageFlagBits::IMAGE_USAGE_STORAGE & pFlags)
        output |= VK_IMAGE_USAGE_STORAGE_BIT;
    return output; 
}

constexpr inline auto Vulkan::map(const flags::SamplerFilter pFilter) noexcept -> VkFilter
{
    switch(pFilter)
    {
        case flags::SamplerFilter::SAMPLER_FILTER_LINEAR: 
            return VkFilter::VK_FILTER_LINEAR;
        case flags::SamplerFilter::SAMPLER_FILTER_NEAREST: 
            return VkFilter::VK_FILTER_NEAREST;
        case flags::SamplerFilter::SAMPLER_FILTER_CUBIC_IMG: 
            return VkFilter::VK_FILTER_CUBIC_IMG;
    };
    assert(false);
    return VkFilter::VK_FILTER_LINEAR;
}

constexpr inline auto Vulkan::map(const flags::SamplerMipmapMode pMode) noexcept -> VkSamplerMipmapMode
{
    switch(pMode)
    {
        case flags::SamplerMipmapMode::SAMPLER_MIPMAP_MODE_LINEAR:
            return VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_LINEAR;
        case flags::SamplerMipmapMode::SAMPLER_MIPMAP_MODE_NEAREST:
            return VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_NEAREST;
    };
    assert(false);
    return VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_LINEAR;
}

constexpr inline auto Vulkan::map(const flags::SamplerAddressMode pMode) noexcept -> VkSamplerAddressMode
{
    switch(pMode)
    {
        case flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_REPEAT:
            return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT;
        case flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT:
            return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        case flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE:
            return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        case flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER:
            return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;

        case flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE:
            return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    };
    assert(false);
    return VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
}

constexpr inline auto Vulkan::map(const flags::SamplerBorderColor pColor) noexcept -> VkBorderColor
{
    switch(pColor)
    {
        case flags::SamplerBorderColor::SAMPLER_BORDER_COLOR_TRANSPARENT:
            return VkBorderColor::VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
        case flags::SamplerBorderColor::SAMPLER_BORDER_COLOR_BLACK:
            return VkBorderColor::VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK;
        case flags::SamplerBorderColor::SAMPLER_BORDER_COLOR_WHITE:
            return VkBorderColor::VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
    };
    assert(false);
    return VkBorderColor::VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
}

constexpr inline auto Vulkan::map(const flags::DescriptorType pType) noexcept -> VkDescriptorType
{
    switch(pType)
    {
        case flags::DescriptorType::DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        
        case flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

        case flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

        case flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        
        case flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
        /*case flags::DescriptorType::DESCRIPTOR_TYPE_SAMPLED_IMAGE:
            return VkDescriptorType::VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;*/
    }

    assert(false);
    return VkDescriptorType::VK_DESCRIPTOR_TYPE_MAX_ENUM;
}

constexpr inline auto Vulkan::map(const flags::IndexType pType) noexcept -> VkIndexType
{
    switch(pType)
    {
        case flags::IndexType::TYPE_UINT16:
            return VkIndexType::VK_INDEX_TYPE_UINT16;
        case flags::IndexType::TYPE_UINT32:
            return VkIndexType::VK_INDEX_TYPE_UINT32;
    }
    assert(false);
    return VkIndexType::VK_INDEX_TYPE_MAX_ENUM;
}

constexpr inline auto Vulkan::map(const flags::Multisampling pType) noexcept -> VkSampleCountFlagBits
{
    switch(pType)
    {
        case flags::Multisampling::MULTISAMPLING_1:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
        case flags::Multisampling::MULTISAMPLING_2:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_2_BIT;
        case flags::Multisampling::MULTISAMPLING_4:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_4_BIT;
        case flags::Multisampling::MULTISAMPLING_8:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_8_BIT;
        case flags::Multisampling::MULTISAMPLING_16:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_16_BIT;
        case flags::Multisampling::MULTISAMPLING_32:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_32_BIT;
        case flags::Multisampling::MULTISAMPLING_64:
            return VkSampleCountFlagBits::VK_SAMPLE_COUNT_64_BIT;
    }
    assert(false);
    return VkSampleCountFlagBits::VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM;
}

constexpr inline auto Vulkan::map(const flags::Format pFormat) noexcept -> VkFormat
{
    return sMapFormat[pFormat];
}

jt::Renderer::Renderer(jt::Renderer&& pRenderer) noexcept
{
    mImpl = pRenderer.mImpl;
    pRenderer.mImpl = nullptr;
}

auto jt::Renderer::operator= (jt::Renderer&& pRenderer) noexcept -> jt::Renderer&
{
    mImpl = pRenderer.mImpl;
    pRenderer.mImpl = nullptr;
    return *this;
}

jt::Renderer::~Renderer()
{
    if (mImpl)
    {
        mImpl->destroy();
        delete mImpl;
    }
}

auto jt::Renderer::make(Size2D pSize, const NativeSurface& pNativeSurface, const char* pAppName, const Parameters& pParameters) noexcept -> jt::out<jt::Renderer, jt::Code>
{
    Renderer r;
    r.mImpl = new (std::nothrow) Renderer::Impl();
    if_null_terminate(r.mImpl);
    auto code = r.mImpl->create(pSize, pNativeSurface, pAppName, pParameters);
    return {std::move(r), code};
}

auto jt::Renderer::getSimultaneousFrameCount() const noexcept -> size_t
{
    return mImpl->mFrames.mSimultaneousFrames;
}

auto jt::Renderer::getAvailableMultisampling() const noexcept -> jt::vector<flags::Multisampling>
{
    return mImpl->getAvailableMultisampling();
}

auto jt::Renderer::resize(Size2D pSize) noexcept -> jt::Code
{
    return mImpl->resize(pSize);
}

auto jt::Renderer::createPassOnscreen(bool pHasDepthImage, flags::Multisampling pMultisampling) noexcept -> out<PassOnscreen, Code>
{
    return mImpl->createPassOnscreen(pHasDepthImage, pMultisampling);
}

auto jt::Renderer::createPassOffscreen(Size2D pSize, TargetBlueprint pColorTarget,
                option<flags::ImageUsageFlags> pDepthTarget,
                option<pair<TargetBlueprint, flags::Multisampling>> pResolveTarget) 
    noexcept -> out<PassOffscreen, Code>
{
    return mImpl->createPassOffscreen(pSize, pColorTarget, pDepthTarget, pResolveTarget);
}

auto jt::Renderer::createVertexShader(const char* pFilename) noexcept -> out<const Shader*, Code>
{
    return mImpl->createVertexShader(pFilename);

}

auto jt::Renderer::createFragmentShader(const char* pFilename) noexcept -> out<const Shader*, Code>
{
    return mImpl->createFragmentShader(pFilename);

}

auto jt::Renderer::createComputeShader(const char* pFilename) noexcept -> out<const Shader*, Code>
{
    return mImpl->createComputeShader(pFilename);

}

auto jt::Renderer::createDescriptorLayout(const jt::vector<Descriptor>& pDescriptors) noexcept -> out<const DescriptorLayout*, Code>
{
    return mImpl->createDescriptorLayout(pDescriptors);
}

auto jt::Renderer::createDescriptorSets(const jt::vector<const DescriptorLayout*>& pLayouts, const jt::vector<DescriptorSetResources>& pResources) noexcept -> out<jt::vector<const DescriptorSet*>, Code>
{
    return mImpl->createDescriptorSets(pLayouts, pResources);
}

auto jt::Renderer::createPipelineGraphics(const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorLayouts, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> out<const Pipeline*, Code>
{
    return mImpl->createPipelineGraphics(pPass, pVertShader,pFragShader, pDescriptorLayouts,
            pInputBindings, pInputAttributes, pViewport, pTopology, pPolygoneMode, pCullMode, 
            pFrontFace, pDynamicViewport, pTestDepth, pWriteDepth, pIsAlphaTransparent);
}

auto jt::Renderer::createPipelineCompute(const Shader* pCompShader, const jt::vector<const DescriptorLayout*>& pDescriptorLayouts) noexcept -> out<const Pipeline*, Code>
{
    return mImpl->createPipelineCompute(pCompShader, pDescriptorLayouts);
}

auto jt::Renderer::createBufferUnmapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Buffer*, Code>
{
    return mImpl->createBufferUnmapped(pSizeByte, pBufferUsage, pMemoryUsage);
}

auto jt::Renderer::createBufferMapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage, void** pMappedPtr) noexcept -> out<const Buffer*, Code>
{
    return mImpl->createBufferMapped(pSizeByte, pBufferUsage, pMemoryUsage, pMappedPtr);
}

auto jt::Renderer::createImage1D(uint32_t pWidth, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>
{
    // TODO: it is possible to add view with _ARRAY type
    VkExtent3D extent = {pWidth, 1, 1};
    return mImpl->createImage(extent, VkImageType::VK_IMAGE_TYPE_1D, VK_IMAGE_VIEW_TYPE_1D, pFormat, pMipLevels, pLayers, pImageUsage, pMemoryUsage);
}

auto jt::Renderer::createImage2D(Size2D pSize, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>
{
    // TODO: it is possible to add view with _ARRAY type
    VkExtent3D extent = {pSize.width, pSize.height, 1};
    return mImpl->createImage(extent, VkImageType::VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_2D, pFormat, pMipLevels, pLayers, pImageUsage, pMemoryUsage);
}

auto jt::Renderer::createImageColorTarget(Size2D pSize, TargetBlueprint pBlueprint, flags::Multisampling pSampling) noexcept -> out<ImageTarget<TargetType::COLOR>, Code>
{
    VkExtent3D extent = {pSize.width, pSize.height, 1};

    VkImageUsageFlags usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT  | Vulkan::map((flags::ImageUsageFlagBits)pBlueprint.mUsage);
    if (pSampling != flags::Multisampling::MULTISAMPLING_1)
        usage |= VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT;
    auto res = mImpl->createImageTarget(extent, Vulkan::map(pBlueprint.mFormat), usage, VK_IMAGE_ASPECT_COLOR_BIT, pSampling);
    return { {res.data}, res.error };
}

auto jt::Renderer::createImageDepthTarget(Size2D pSize, flags::ImageUsageFlags pImageUsage, flags::Multisampling pSampling) noexcept -> out<ImageTarget<TargetType::DEPTH>, Code>
{
    VkExtent3D extent = {pSize.width, pSize.height, 1};
    VkImageUsageFlags usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | Vulkan::map((flags::ImageUsageFlagBits)pImageUsage);
    // TODO: there are different depth formats, aome of them like D32_SFLOAT doesnt support stencil ,therefore we must to do something about that if we want stencil.
    auto res = mImpl->createImageTarget(extent, mImpl->mSwapchain.mDepthFormat, usage, VK_IMAGE_ASPECT_DEPTH_BIT, pSampling);
    return { {res.data}, res.error };
}

auto jt::Renderer::createImageResolveTarget(Size2D pSize, TargetBlueprint pBlueprint) noexcept -> out<ImageTarget<TargetType::RESOLVE>, Code>
{
    VkExtent3D extent = {pSize.width, pSize.height, 1};
    VkImageUsageFlags usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | Vulkan::map((flags::ImageUsageFlagBits)pBlueprint.mUsage);
    auto res = mImpl->createImageTarget(extent, Vulkan::map(pBlueprint.mFormat), usage, VK_IMAGE_ASPECT_COLOR_BIT, flags::Multisampling::MULTISAMPLING_1);
    return { {res.data}, res.error };
}

auto jt::Renderer::createImage3D(Size3D pSize, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>
{
    // TODO: it is possible to add view with _ARRAY type
    VkExtent3D extent = {pSize.width, pSize.height, pSize.depth};
    return mImpl->createImage(extent, VkImageType::VK_IMAGE_TYPE_3D, VK_IMAGE_VIEW_TYPE_3D, pFormat, pMipLevels, pLayers, pImageUsage, pMemoryUsage);
}

auto jt::Renderer::writeBufferUnmapped(const void* pSrcData, const Buffer* pDstBufferCPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    return mImpl->writeBufferUnmapped(pSrcData, pDstBufferCPU, pSrcOffsetByte, pDstOffsetByte, pCopySizeByte);
}

auto jt::Renderer::writeBufferUnmapped(const jt::vector<IndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    static_assert(sizeof(IndirectCommand) == sizeof(VkDrawIndirectCommand));
    return mImpl->writeBufferUnmapped(pSrcIndirectData, pDstBufferCPU, pFirstCommand, pDstOffsetByte, pCopyCommandCount);
}

auto jt::Renderer::writeBufferUnmapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    static_assert(sizeof(IndexedIndirectCommand) == sizeof(VkDrawIndexedIndirectCommand));
    return mImpl->writeBufferUnmapped(pSrcIndirectData, pDstBufferCPU, pFirstCommand, pDstOffsetByte, pCopyCommandCount);
}

auto jt::Renderer::writeBufferMapped(const void* pSrcData, const Buffer* pDstBufferCPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    return mImpl->writeBufferMapped(pSrcData, pDstBufferCPU, pSrcOffsetByte, pDstOffsetByte, pCopySizeByte);
}

auto jt::Renderer::writeBufferMapped(const jt::vector<IndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    static_assert(sizeof(IndirectCommand) == sizeof(VkDrawIndirectCommand));
    return mImpl->writeBufferMapped(pSrcIndirectData, pDstBufferCPU, pFirstCommand, pDstOffsetByte, pCopyCommandCount);
}

auto jt::Renderer::writeBufferMapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    static_assert(sizeof(IndexedIndirectCommand) == sizeof(VkDrawIndexedIndirectCommand));
    return mImpl->writeBufferMapped(pSrcIndirectData, pDstBufferCPU, pFirstCommand, pDstOffsetByte, pCopyCommandCount);
}

auto jt::Renderer::createCommandBuffer() noexcept -> out<const CommandBuffer*, Code>
{
    return mImpl->createCommandBuffer();
}

auto jt::Renderer::createSampler(flags::SamplerFilter pMagFilter, flags::SamplerFilter pMinFilter, flags::SamplerMipmapMode pMipmapMode, flags::SamplerAddressMode pAddressModeUVW[3], bool pEnableAnisotropy, uint32_t mipMaps, flags::SamplerBorderColor pBorderColor) noexcept -> out<const Sampler*, Code>
{
    return mImpl->createSampler(pMagFilter, pMinFilter, pMipmapMode, pAddressModeUVW, 
            pEnableAnisotropy, mipMaps, pBorderColor);
}

auto jt::Renderer::destroyShader(const Shader* pShader) noexcept -> void
{
    return mImpl->destroyShader(pShader);
}

auto jt::Renderer::drawBegin() noexcept -> out<uint32_t, Code>
{
    return mImpl->drawBegin();
}

auto jt::Renderer::addCommandBuffer(const CommandBuffer* pCommandBuffer) noexcept ->void
{
    return mImpl->addCommandBuffer(pCommandBuffer);
}

auto jt::Renderer::drawSubmitWait() noexcept -> Code
{
    return mImpl->drawSubmitWait();
}

auto jt::Renderer::drawSubmitLeave() noexcept -> Code
{
    return mImpl->drawSubmitLeave();
}

auto jt::Renderer::computeBegin() noexcept -> Code
{
    return mImpl->computeBegin();
}

auto jt::Renderer::computeSubmitWait() noexcept -> Code
{
    return mImpl->computeSubmitWait();
}

auto jt::Renderer::recordCmdBegin(const CommandBuffer* pCBuffer) noexcept -> void
{
    return mImpl->recordCmdBegin(pCBuffer);
}

auto jt::Renderer::recordCmdEnd(const CommandBuffer* pCBuffer) noexcept -> void
{
    return mImpl->recordCmdEnd(pCBuffer);
}

auto jt::Renderer::cmdPassBegin(const CommandBuffer* pCBuffer, PassOnscreen pPass, float pClearColor[4]) noexcept -> void
{
    return mImpl->cmdPassBegin(pCBuffer, pPass, pClearColor);
}
auto jt::Renderer::cmdPassBegin(const CommandBuffer* pCBuffer, PassOffscreen pPass, float pClearColor [4],
                ImageTarget<TargetType::COLOR> pColorTarget, 
                jt::option<ImageTarget<TargetType::DEPTH>> pDepthTarget, 
                jt::option<ImageTarget<TargetType::RESOLVE>> pResolveTarget) noexcept -> void
{
    return mImpl->cmdPassBegin(pCBuffer, pPass, pClearColor, pColorTarget, pDepthTarget, pResolveTarget); 
}
auto jt::Renderer::cmdBindPipeline(const CommandBuffer* pCBuffer, const Pipeline* pPipeline) noexcept -> void
{
    return mImpl->cmdBindPipeline(pCBuffer, pPipeline);
}

auto jt::Renderer::cmdBindDescriptorSets(const CommandBuffer* pCBuffer, const Pipeline* pPipeline, const jt::vector<const DescriptorSet*>& pDescriptorSets, const jt::vector<uint32_t>& pDynamicOffsets) noexcept -> void
{
    return mImpl->cmdBindDescriptorSets(pCBuffer, pPipeline, pDescriptorSets, pDynamicOffsets);
}

auto jt::Renderer::cmdBindVertexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte) noexcept -> void
{
    return mImpl->cmdBindVertexBuffer(pCBuffer, pBuffer, pOffsetByte);
}

auto jt::Renderer::cmdBindIndexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, flags::IndexType pType) noexcept -> void
{
    return mImpl->cmdBindIndexBuffer(pCBuffer, pBuffer, pOffsetByte, pType);
}

auto jt::Renderer::cmdLoadVertexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    return mImpl->cmdLoadVertexData(pCBuffer, pInputBufferCPU, pOutputBufferGPU, pSrcOffsetByte, pDstOffsetByte, pCopySizeByte);
}

auto jt::Renderer::cmdLoadIndexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    return mImpl->cmdLoadIndexData(pCBuffer, pInputBufferCPU, pOutputBufferGPU, pSrcOffsetByte, pDstOffsetByte, pCopySizeByte);
}

auto jt::Renderer::cmdLoadTextureData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Image* pOutputImage) noexcept -> void
{
    return mImpl->cmdLoadTextureData(pCBuffer, pInputBufferCPU, pOutputImage);
}

auto jt::Renderer::cmdCopyBuffer(const CommandBuffer* pCBuffer, const Buffer* pSrcBuffer, const Buffer* pDstBuffer, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    return mImpl->cmdCopyBuffer(pCBuffer, pSrcBuffer, pDstBuffer, pSrcOffsetByte, pDstOffsetByte, pCopySizeByte);
}

auto jt::Renderer::cmdDraw(const CommandBuffer* pCBuffer, uint32_t pVertexCount, uint32_t pInstanceCount, uint32_t pFirstVertex, uint32_t pFirstInstance) noexcept -> void
{
    return mImpl->cmdDraw(pCBuffer, pVertexCount, pInstanceCount, pFirstVertex, pFirstInstance);
}

auto jt::Renderer::cmdDrawIndexed(const CommandBuffer* pCBuffer, uint32_t pIndexCount, uint32_t pInstanceCount, uint32_t pFirstIndex, int32_t pVertexOffset, uint32_t pFirstInstance) noexcept -> void
{
    return mImpl->cmdDrawIndexed(pCBuffer, pIndexCount, pInstanceCount, pFirstIndex, 
            pVertexOffset, pFirstInstance);
}

auto jt::Renderer::cmdDrawIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void
{
    return mImpl->cmdDrawIndirect(pCBuffer, pBuffer, pOffsetByte, pDrawCount, pStrideByte);
}

auto jt::Renderer::cmdDrawIndexedIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void
{
    return mImpl->cmdDrawIndexedIndirect(pCBuffer, pBuffer, pOffsetByte, pDrawCount, pStrideByte);
}

auto jt::Renderer::cmdDispatch(const CommandBuffer* pCBuffer, uint32_t pGroupCountX, uint32_t pGroupCountY, uint32_t pGroupCountZ) noexcept -> void
{
    return mImpl->cmdDispatch(pCBuffer, pGroupCountX, pGroupCountY, pGroupCountZ);
}

auto jt::Renderer::cmdSetViewport(const CommandBuffer* pCBuffer, const Viewport& pViewport) noexcept -> void
{

    return mImpl->cmdSetViewport(pCBuffer, pViewport);
}
auto jt::Renderer::cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                uint32_t pBuffersCount, const BufferRegionBarrier* pBuffers,
                uint32_t pImageCount, const ImageDataBarrier* pImages) noexcept -> void
{
    return mImpl->cmdBarrier(pCBuffer, pSrcStages, pDstStages, pBuffersCount, pBuffers, pImageCount, pImages);
}
auto jt::Renderer::cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                const jt::vector<BufferRegionBarrier>& pBuffers,
                const jt::vector<ImageDataBarrier>& pImages) noexcept -> void
{
    return mImpl->cmdBarrier(pCBuffer, pSrcStages, pDstStages, pBuffers.size(), pBuffers.data(), pImages.size(), pImages.data());
}

auto jt::Renderer::cmdPassEnd(const CommandBuffer* pCBuffer, const Pass* pPass) noexcept -> void
{
    return mImpl->cmdPassEnd(pCBuffer, pPass);
}

auto Vulkan::create(Size2D pSize, const NativeSurface& pNativeSurface, const char* pAppName, const Parameters& pParameters) noexcept -> Code
{
    mParameters = pParameters;
    CHECK_VK(createInstance(pAppName));
#ifdef RENDER_DEBUG
    CHECK_VK(createDebugger());
#endif // RENDER_DEBUG
    CHECK_VK(createSurface(pNativeSurface));
    CHECK_VK(selectPhysicalDeviceAndQueueIds());
    CHECK_VK(createDeviceAndQueues());
    CHECK_VK(createAllocator());
    CHECK_VK(createCommandPool());
    CHECK_VK(prepareSwapchainStatic());
    // recreation part
    CHECK_VK(prepareSwapchainDynamic(pSize));
    CHECK_VK(createSwapchain());
    // recreation part
    CHECK_VK(createFramesSynchronization());
    return jt::Code::OK;
}

auto Vulkan::createPassOnscreen(bool pHasDepthImage, flags::Multisampling pMultisampling) noexcept -> out<PassOnscreen, Code>
{
    auto multisampling = Vulkan::map(pMultisampling);

    if (pHasDepthImage && !hasDepthTargetImageFor(multisampling))
    {
        auto res = genDepthTargetImageFor(multisampling);
        if (res != VK_SUCCESS) return {.error = Code::RENDERER_ERROR};
    }
    if (multisampling != VK_SAMPLE_COUNT_1_BIT && !hasColorTargetImageFor(multisampling))
    {
        auto res = genColorTargetImageFor(multisampling);
        if (res != VK_SUCCESS) return {.error = Code::RENDERER_ERROR};
    }

    auto pass = jt::make_unique<Pass>();
    auto ptr = pass.get();
    pass->mMultisampling = Vulkan::map(pMultisampling);
    pass->mHasDepthImage = pHasDepthImage;
    pass->mHasMultisampledColorImage = pMultisampling != flags::Multisampling::MULTISAMPLING_1;

    pass->mColorTargetUsage = pass->mHasMultisampledColorImage ? 
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT :
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    pass->mDepthTargetUsage = VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    pass->mResolveTargetUsage = VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    pass->mColorFormat = mSwapchain.mFormat;
    pass->mDepthFormat = mSwapchain.mDepthFormat;
    pass->mResolveFormat = mSwapchain.mFormat;

    auto size = Size2D{.width = mSwapchain.mExtent.width, .height = mSwapchain.mExtent.height};
    auto res = pass->createRenderPassObject(*this, false);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};
    res = pass->createFramebuffers(*this, size);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    mRenderPassesOnscreen.push_back(std::move(pass));

    return { .data = {ptr}, .error = jt::Code::OK };
}

auto Vulkan::createPassOffscreen(Size2D pSize, TargetBlueprint pColorTarget, option<flags::ImageUsageFlags> pDepthTarget, option<pair<TargetBlueprint, flags::Multisampling>> pResolveTarget) noexcept -> out<PassOffscreen, Code>
{
    assert(pResolveTarget && pResolveTarget.value().second != flags::Multisampling::MULTISAMPLING_1);
    auto pass = jt::make_unique<Pass>();
    auto ptr = pass.get();
    pass->mMultisampling = pResolveTarget ? Vulkan::map(pResolveTarget.value().second) : VK_SAMPLE_COUNT_1_BIT;
    pass->mHasDepthImage = pDepthTarget;
    pass->mHasMultisampledColorImage = pResolveTarget;

    pass->mColorTargetUsage = Vulkan::map((jt::flags::ImageUsageFlagBits)pColorTarget.mUsage);
    if (pass->mHasDepthImage)
        pass->mDepthTargetUsage = Vulkan::map((jt::flags::ImageUsageFlagBits)pDepthTarget.value());
    if (pass->mHasMultisampledColorImage)
        pass->mResolveTargetUsage = Vulkan::map((jt::flags::ImageUsageFlagBits)pResolveTarget.value().first.mUsage);

    pass->mColorTargetUsage |= pass->mHasMultisampledColorImage ? 
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT :
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    pass->mDepthTargetUsage |= VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    pass->mResolveTargetUsage |= VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    pass->mColorFormat = Vulkan::map(pColorTarget.mFormat);
    pass->mDepthFormat = mSwapchain.mDepthFormat;
    if (pass->mHasMultisampledColorImage)
        pass->mResolveFormat = Vulkan::map(pResolveTarget.value().first.mFormat);

    auto res = pass->createRenderPassObject(*this, false);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};
    res = pass->createFramebuffers(*this, pSize);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    mRenderPassesOffscreen.push_back(std::move(pass));
    return { .data = {ptr}, .error = jt::Code::OK };
}

auto Vulkan::createVertexShader(const char* pFilename) noexcept -> jt::out<const Shader*, jt::Code>
{
    auto shader = jt::make_unique<Shader>();
    auto res = shader->create(*this, pFilename, VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT);
    if (res != jt::Code::OK) return { .error = res };
    auto [it, result] = mShaders.emplace(shader.get(), std::move(shader));
    return { .data = it->first, .error = result ? res : jt::Code::RENDERER_ERROR };
}

auto Vulkan::createFragmentShader(const char* pFilename) noexcept -> jt::out<const Shader*, jt::Code>
{
    auto shader = jt::make_unique<Shader>();
    auto res = shader->create(*this, pFilename, VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT);
    if (res != jt::Code::OK) return { .error = res };
    auto [it, result] = mShaders.emplace(shader.get(), std::move(shader));
    return { .data = it->first, .error = result ? res : jt::Code::RENDERER_ERROR };
}

auto Vulkan::createComputeShader(const char* pFilename) noexcept -> jt::out<const Shader*, jt::Code>
{
    auto shader = jt::make_unique<Shader>();
    auto res = shader->create(*this, pFilename, VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT);
    if (res != jt::Code::OK) return { .error = res };
    auto [it, result] = mShaders.emplace(shader.get(), std::move(shader));
    return { .data = it->first, .error = result ? res : jt::Code::RENDERER_ERROR };
}

auto Vulkan::createDescriptorLayout(const jt::vector<Descriptor>& pDescriptors) noexcept -> out<const DescriptorLayout*, Code>
{
    jt::vector<VkDescriptorSetLayoutBinding> bindings(pDescriptors.size()); 
    for (size_t i = 0; i < bindings.size(); i++)
    {
        const auto& descriptor = pDescriptors[i];
        VkShaderStageFlags stages = flags::ShaderStageBits::SHADER_STAGE_VERTEX & descriptor.mUsingInStages ? VK_SHADER_STAGE_VERTEX_BIT : 0
            | flags::ShaderStageBits::SHADER_STAGE_FRAGMENT & descriptor.mUsingInStages ? VK_SHADER_STAGE_FRAGMENT_BIT : 0
            | flags::ShaderStageBits::SHADER_STAGE_COMPUTE & descriptor.mUsingInStages ? VK_SHADER_STAGE_COMPUTE_BIT : 0;
        assert(stages != 0);
        bindings[i] =
        {
            .binding = descriptor.mBinding,
            .descriptorType = Vulkan::map(descriptor.mTupe),
            .descriptorCount = descriptor.mCount,
            .stageFlags = stages,
            .pImmutableSamplers = nullptr
        };

    }

    VkDescriptorSetLayoutCreateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .bindingCount = (uint32_t)bindings.size(),
        .pBindings = bindings.data()
    };

    VkDescriptorSetLayout layout;
    auto res = vkCreateDescriptorSetLayout(mDevice, &info, nullptr, &layout);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    mDescriptorLayouts.push_back(jt::make_unique<DescriptorLayout>());
    mDescriptorLayouts.back()->mLayout = layout;
    mDescriptorLayouts.back()->mDescriptors = pDescriptors;
    return { .data = mDescriptorLayouts.back().get(), .error = jt::Code::OK };
}

auto Vulkan::createDescriptorPoolHelper(const jt::vector<const DescriptorLayout*>& pLayouts) noexcept -> out<VkDescriptorPool, Code>
{
    jt::vector<VkDescriptorPoolSize> poolSizes;

    for (const auto& layout : pLayouts)
    for (const auto& desc : layout->mDescriptors)
    {
        auto vkType = Vulkan::map(desc.mTupe);
        bool hasType = false;
        for (auto& poolSize :poolSizes)
        {
            if (vkType == poolSize.type)
            {
                poolSize.descriptorCount ++;
                hasType = true;
                break;
            }
        }
        if (!hasType)
        {
            VkDescriptorPoolSize size = 
            {
                .type = vkType,
                .descriptorCount = 1
            };
            poolSizes.push_back(size);
        }
    }

    VkDescriptorPool pool;
    VkDescriptorPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .maxSets = (uint32_t)pLayouts.size(),
        .poolSizeCount = (uint32_t)poolSizes.size(),
        .pPoolSizes = poolSizes.data()
    };
    auto res = vkCreateDescriptorPool(mDevice, &poolInfo, nullptr, &pool);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    return { .data = pool, .error = jt::Code::OK};
}

auto Vulkan::createDescriptorSets(const jt::vector<const DescriptorLayout*>& pLayouts, const jt::vector<DescriptorSetResources>& pResources) noexcept -> out<jt::vector<const DescriptorSet*>, Code>
{
    auto [pool, vkResult] = createDescriptorPoolHelper(pLayouts); 
    if (vkResult != jt::Code::OK) return {.error = vkResult};

    jt::vector<VkDescriptorSetLayout> vkLayouts;
    jt::vector<VkDescriptorSet> vkSets(pLayouts.size());
    vkSets.resize(vkLayouts.size());
    for (const auto& layout : pLayouts)
        vkLayouts.push_back(layout->mLayout);

    VkDescriptorSetAllocateInfo allocInfo = 
    {   
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = pool,
        .descriptorSetCount = (uint32_t)vkLayouts.size(),
        .pSetLayouts = vkLayouts.data()
    };
    auto res = vkAllocateDescriptorSets(mDevice, &allocInfo, vkSets.data());
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    /*uint32_t writesCount = 0;
    for (const auto& res : pResources) 
        writesCount += res.size();*/

    jt::vector<VkWriteDescriptorSet> writes;
    jt::vector<jt::vector<VkDescriptorBufferInfo>> bufferInfos;
    jt::vector<jt::vector<VkDescriptorImageInfo>> imageInfos;

    for (int layoutId = 0; layoutId < pLayouts.size(); layoutId++)
    {
        size_t currentResourceId = 0;
        const auto& layout = pLayouts[layoutId];
        const auto& resources = pResources[layoutId];
        auto set = vkSets[layoutId];

        for (const auto& descriptor : layout->mDescriptors)
        {
            bool isBuffer = resources[currentResourceId].is<BufferRegion>();
            bool isImage = resources[currentResourceId].is<ImageData>();
            auto vkType = Vulkan::map(descriptor.mTupe);
            if (isBuffer)
            {
                bufferInfos.emplace_back();
                for (size_t resId = 0; resId < descriptor.mCount; resId++)
                {
                    auto buf = resources[resId + currentResourceId].get<BufferRegion>();
                    bufferInfos.back().push_back(
                    {
                        .buffer = buf.mBuffer->mBuffer,
                        .offset = buf.mOffset,
                        .range = buf.mRange
                    });
                }
            }
            else if (isImage) 
            {
                imageInfos.emplace_back();
                for (size_t resId = 0; resId < descriptor.mCount; resId++)
                {
                    auto img = resources[resId + currentResourceId].get<ImageData>();
                    imageInfos.back().push_back(
                    {
                        .sampler = img.mSampler->mSampler,
                        .imageView = img.mImage->mView,
                        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
                    });
                }
            }
            else
            {
                assert(false);
            }

            writes.push_back( 
            {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = set,
                .dstBinding = descriptor.mBinding,
                .dstArrayElement = 0,
                .descriptorCount = descriptor.mCount,
                .descriptorType = vkType,
                .pImageInfo = isImage ? imageInfos.back().data() : nullptr,
                .pBufferInfo  = isBuffer ? bufferInfos.back().data() : nullptr,
                .pTexelBufferView = nullptr
            });

            currentResourceId += descriptor.mCount;
        }
    }

    vkUpdateDescriptorSets(mDevice, writes.size(), writes.data(), 0, nullptr);

    jt::vector<const DescriptorSet*> result;
    mDescriptorResources.push_back(jt::make_unique<DescriptorResource>());
    auto resource = mDescriptorResources.back().get();
    resource->mPool = pool;
    resource->mIsFreeDescriptorSetsEnabled = false;
    for (int layoutId = 0; layoutId < pLayouts.size(); layoutId++)
    {
        auto set = vkSets[layoutId];
        resource->mSets.push_back(jt::make_unique<DescriptorSet>());
        resource->mSets.back()->mSet = set;
        result.push_back(resource->mSets.back().get());
    }

    return { .data = result, .error = jt::Code::OK };;
}

auto Vulkan::createPipelineGraphics(const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorSets, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> out<const Pipeline*, Code>
{
    auto pipeline = jt::make_unique<Pipeline>();
    auto res = pipeline->create(*this, pPass, pVertShader,pFragShader, pDescriptorSets,pInputBindings, pInputAttributes, pViewport, pTopology, pPolygoneMode, pCullMode, pFrontFace, pDynamicViewport, pTestDepth, pWriteDepth, pIsAlphaTransparent);
    if (res != VK_SUCCESS)
        return {.error = jt::Code::RENDERER_ERROR};
    pipeline->mBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    mPipelines.push_back(std::move(pipeline));
    return { .data = mPipelines.back().get(), .error = jt::Code::OK };
}

auto Vulkan::createPipelineCompute(const Shader* pCompShader, const jt::vector<const DescriptorLayout*>& pDescriptorLayouts) noexcept -> out<const Pipeline*, Code>
{
    assert(pCompShader->mStage & VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT);

    jt::vector<VkDescriptorSetLayout> sets;
    for (const auto set : pDescriptorLayouts)
        sets.push_back(set->mLayout);
    VkPipelineLayout layout;
    VkPipelineLayoutCreateInfo layoutInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = (uint32_t)sets.size(),
        .pSetLayouts = sets.data(),
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr
    };
    auto res = vkCreatePipelineLayout(mDevice, &layoutInfo, nullptr, &layout);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};

    VkPipeline pipeline;
    VkComputePipelineCreateInfo info =
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = 
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT,
            .module = pCompShader->mModule,
            .pName = "main",
            .pSpecializationInfo = nullptr
        },
        .layout = layout,
        .basePipelineHandle = nullptr,
        .basePipelineIndex = 0
    };

    res = vkCreateComputePipelines(mDevice, VK_NULL_HANDLE, 1, &info, nullptr, &pipeline);
    if (res != VK_SUCCESS) return {.error = jt::Code::RENDERER_ERROR};
    mPipelines.push_back(jt::make_unique<Pipeline>());
    mPipelines.back()->mPipeline = pipeline;
    mPipelines.back()->mLayout = layout;
    mPipelines.back()->mBindPoint = VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_COMPUTE;
    return { .data = mPipelines.back().get(), .error = jt::Code::OK };
}

auto Vulkan::getAvailableMultisampling() const noexcept -> jt::vector<flags::Multisampling>
{
    jt::vector<flags::Multisampling> output;
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(mPhysicalDevice, &props);

    VkSampleCountFlags samples = props.limits.framebufferColorSampleCounts & 
        props.limits.framebufferDepthSampleCounts;
    if (samples & VK_SAMPLE_COUNT_1_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_1);
    if (samples & VK_SAMPLE_COUNT_2_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_2);
    if (samples & VK_SAMPLE_COUNT_4_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_4);
    if (samples & VK_SAMPLE_COUNT_8_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_8);
    if (samples & VK_SAMPLE_COUNT_16_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_16);
    if (samples & VK_SAMPLE_COUNT_32_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_32);
    if (samples & VK_SAMPLE_COUNT_64_BIT) output.push_back(flags::Multisampling::MULTISAMPLING_64);
    return output;
}

auto Vulkan::getInstanceExtensions() const noexcept -> const jt::vector<const char *>
{
    return 
    {
        "VK_KHR_surface", 
        "VK_KHR_xlib_surface",
        VK_EXT_DEBUG_UTILS_EXTENSION_NAME
    };
}

auto Vulkan::getDeviceExtensions() const noexcept -> const jt::vector<const char *>
{
    return 
    {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
}

auto Vulkan::getLayers() const noexcept -> const jt::vector<const char *>
{
    return 
    {
#ifdef RENDER_DEBUG
        "VK_LAYER_KHRONOS_validation",
#endif // RENDER_DEBUG
    };
}

auto Vulkan::getDepthFormatProposals() const noexcept ->const jt::vector<VkFormat>
{
    return 
    {
        VkFormat::VK_FORMAT_D32_SFLOAT,
        VkFormat::VK_FORMAT_D32_SFLOAT_S8_UINT,
        VkFormat::VK_FORMAT_D24_UNORM_S8_UINT
    };
}

auto Vulkan::createInstance(const char* pAppName) noexcept -> VkResult
{
    auto layers = getLayers();
    auto extensionsInstance = getInstanceExtensions();

    VkApplicationInfo appInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = nullptr,
        .pApplicationName = pAppName,
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "JUNCTURE",
        .engineVersion = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = VK_API_VERSION_1_2
    };

    VkInstanceCreateInfo instanceInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = nullptr, .flags = 0,
        .pApplicationInfo = &appInfo,
        .enabledLayerCount = (uint32_t)(layers.size()),
        .ppEnabledLayerNames = layers.empty() ? nullptr : layers.data(),
        .enabledExtensionCount = (uint32_t)extensionsInstance.size(),
        .ppEnabledExtensionNames = extensionsInstance.data()
    };
    return vkCreateInstance(&instanceInfo, NULL, &mInstance);
}

#ifdef RENDER_DEBUG
auto Vulkan::createDebugger() noexcept -> VkResult
{
    VkDebugUtilsMessengerCreateInfoEXT debuggerInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .pNext = nullptr,
        .flags = 0,
        .messageSeverity = 
            /*VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | */
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = debugCallback,
        .pUserData = nullptr
    };
    auto vkCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr
        (mInstance, "vkCreateDebugUtilsMessengerEXT");
    assert(vkCreateDebugUtilsMessengerEXT);
    if (!vkCreateDebugUtilsMessengerEXT)
        return VK_ERROR_UNKNOWN;
    return vkCreateDebugUtilsMessengerEXT(mInstance, &debuggerInfo, nullptr, &mDebugMessenger);
}
#endif // RENDER_DEBUG 

auto Vulkan::createSurface(const NativeSurface& pNativeSurface) noexcept -> VkResult
{
#if __linux__
    VkXlibSurfaceCreateInfoKHR surfaceInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
        .pNext = nullptr,
        .flags = 0,
        .dpy = pNativeSurface.mDisplay,
        .window = pNativeSurface.mWindow
    }; return vkCreateXlibSurfaceKHR(mInstance, &surfaceInfo, nullptr, &mSurface);
#endif
}

auto Vulkan::selectPhysicalDeviceAndQueueIds() noexcept -> VkResult
{
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(mInstance, &deviceCount, nullptr);
    if (deviceCount == 0) return VK_ERROR_UNKNOWN;
    auto devices = jt::vector<VkPhysicalDevice>(deviceCount);
    auto res = vkEnumeratePhysicalDevices(mInstance, &deviceCount, devices.data());
    if (res != VK_SUCCESS) return res;

    for (const auto& device : devices)
    {
        uint32_t queuesCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queuesCount, nullptr);
        auto props = jt::vector<VkQueueFamilyProperties>(queuesCount);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queuesCount, props.data());

        bool hasQueueGraphicId = false, hasQueuePresentId = false, hasQueueComputeId = false;
        
        for (size_t i = 0; i < props.size(); i++)
        {
            auto flags = VK_QUEUE_GRAPHICS_BIT | (mParameters.enableComputeDrawMix ? VK_QUEUE_COMPUTE_BIT : 0);
            hasQueueGraphicId = props[i].queueFlags & flags;
            if (hasQueueGraphicId)
            {
                mQueueGraphicId = i;
                break;
            }
        }
        
        for (size_t i = 0; i < props.size(); i++)
        {
            VkBool32 hasSurfaceSupport;
            res = vkGetPhysicalDeviceSurfaceSupportKHR(device, i, mSurface, &hasSurfaceSupport);
            if (res != VK_SUCCESS) return res;
            if (hasSurfaceSupport)
            {
                hasQueuePresentId = true;
                mQueuePresentId = i;
                break;
            }
        }

        if (mParameters.enableComputeDedicated)
        {
            uint32_t neededMinQueuesCountInFamily = ((mQueueGraphicId != mQueueComputeId) 
                    && (mQueuePresentId != mQueueComputeId)) ? 1 : 2;
            for (size_t i = 0; i < props.size(); i++)
            {
                hasQueueComputeId = (props[i].queueFlags & VK_QUEUE_COMPUTE_BIT) 
                    && (props[i].queueCount >= neededMinQueuesCountInFamily);
                if (hasQueueComputeId)
                {
                    mQueueComputeId = i;
                    break;
                }
            }
        }

        if (hasQueueGraphicId && hasQueuePresentId) 
        {
            if (!mParameters.enableComputeDedicated || (mParameters.enableComputeDedicated && hasQueueComputeId))
            {
                VkPhysicalDeviceProperties deviceProps;
                vkGetPhysicalDeviceProperties(device, &deviceProps);
                if (deviceProps.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) continue;

                mPhysicalDevice = device;
                printf("\nDEVICE: \t\t%s\nDRIVERS VERISON: \t%u\n", 
                        deviceProps.deviceName, deviceProps.driverVersion);
                //printf("GRAPHIC: %u; PRESENT %u; COMPUTE %u\n", mQueueGraphicId, mQueuePresentId, mQueueComputeId);
                return VK_SUCCESS;
            }
        }
    }
    return VK_ERROR_UNKNOWN;
}
auto Vulkan::createDeviceAndQueues() noexcept -> VkResult
{
    float queuePriority = 1.0f;
    bool queueIdsSame = mQueueGraphicId == mQueuePresentId;
    auto queueInfos = jt::vector<VkDeviceQueueCreateInfo>();
    queueInfos.push_back({
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr, .flags = 0,
            .queueFamilyIndex = mQueueGraphicId,
            .queueCount = 1,
            .pQueuePriorities = &queuePriority
        });
    if (!queueIdsSame)
        queueInfos.push_back({
                .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                .pNext = nullptr, .flags = 0,
                .queueFamilyIndex = mQueuePresentId,
                .queueCount = 1,
                .pQueuePriorities = &queuePriority
            });
    if (mParameters.enableComputeDedicated)
    {
        if (mQueueGraphicId == mQueueComputeId)
            queueInfos[0].queueCount++; 
        else
            queueInfos.push_back({
                    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                    .pNext = nullptr, .flags = 0,
                    .queueFamilyIndex = mQueueComputeId,
                    .queueCount = 1,
                    .pQueuePriorities = &queuePriority
                });
    }

    VkPhysicalDeviceImagelessFramebufferFeatures imagelessFeature = 
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES,
        .pNext = nullptr,
        .imagelessFramebuffer = true
    };

    VkPhysicalDeviceFeatures2 features2 = 
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = &imagelessFeature,
        .features = 
        {
           //.robustBufferAccess;
           //.fullDrawIndexUint32;
           //.imageCubeArray;
           //.independentBlend;
           //.geometryShader;
           //.tessellationShader;
           //.sampleRateShading;
           //.dualSrcBlend;
           //.logicOp;
           .multiDrawIndirect = mParameters.multiDrawIndirectEnabled,
           //.drawIndirectFirstInstance;
           //.depthClamp;
           //.depthBiasClamp;
           //.fillModeNonSolid;
           //.depthBounds;
           //.wideLines;
           //.largePoints;
           //.alphaToOne;
           //.multiViewport;
            .samplerAnisotropy = mParameters.anisotropyEnabled, 
           //.textureCompressionETC2;
           //.textureCompressionASTC_LDR;
           //.textureCompressionBC;
           //.occlusionQueryPrecise;
           //.pipelineStatisticsQuery;
           .vertexPipelineStoresAndAtomics = mParameters.vertexStoreAtomicallyBuffer,
           //.fragmentStoresAndAtomics;
           //.shaderTessellationAndGeometryPointSize;
           //.shaderImageGatherExtended;
           //.shaderStorageImageExtendedFormats;
           //.shaderStorageImageMultisample;
           //.shaderStorageImageReadWithoutFormat;
           //.shaderStorageImageWriteWithoutFormat;
           //.shaderUniformBufferArrayDynamicIndexing;
           //.shaderSampledImageArrayDynamicIndexing;
           //.shaderStorageBufferArrayDynamicIndexing;
           //.shaderStorageImageArrayDynamicIndexing;
           //.shaderClipDistance;
           //.shaderCullDistance;
           //.shaderFloat64;
           //.shaderInt64;
           //.shaderInt16;
           //.shaderResourceResidency;
           //.shaderResourceMinLod;
           //.sparseBinding;
           //.sparseResidencyBuffer;
           //.sparseResidencyImage2D;
           //.sparseResidencyImage3D;
           //.sparseResidency2Samples;
           //.sparseResidency4Samples;
           //.sparseResidency8Samples;
           //.sparseResidency16Samples;
           //.sparseResidencyAliased;
           //.variableMultisampleRate;
           //.inheritedQueries;
        }
    };
    auto extensions = getDeviceExtensions();
    auto layers = getLayers();
    VkDeviceCreateInfo deviceInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = &features2, 
        .flags = 0,
        .queueCreateInfoCount = (uint32_t)(queueInfos.size()),
        .pQueueCreateInfos = queueInfos.data(),
        .enabledLayerCount = (uint32_t)(layers.size()),
        .ppEnabledLayerNames = layers.empty() ? nullptr : layers.data(),
        .enabledExtensionCount = (uint32_t)extensions.size(),
        .ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data(),
        .pEnabledFeatures =  nullptr//&features
    };

    auto res = vkCreateDevice(mPhysicalDevice, &deviceInfo, nullptr, &mDevice);
    if (res != VK_SUCCESS) return res;

    uint32_t graphicPresentId = 0;
    uint32_t computeId = ((mQueueGraphicId != mQueueComputeId) && (mQueuePresentId != mQueueComputeId)) ?
        0 : (graphicPresentId + 1);
    vkGetDeviceQueue(mDevice, mQueueGraphicId, graphicPresentId, &mQueueGraphic);
    vkGetDeviceQueue(mDevice, mQueuePresentId, graphicPresentId, &mQueuePresent);
    if (mParameters.enableComputeDedicated)
        vkGetDeviceQueue(mDevice, mQueueComputeId, computeId, &mQueueCompute);
    return res;
}

auto Vulkan::createAllocator() noexcept -> VkResult
{
    VmaAllocatorCreateInfo info = 
    {
        .flags = 0,
        .physicalDevice = mPhysicalDevice,
        .device = mDevice,
        /// Preferred size of a single `VkDeviceMemory` block to be allocated from large heaps > 1 GiB. Optional.
        /** Set to 0 to use default, which is currently 256 MiB. */
        .preferredLargeHeapBlockSize = 0,
        .pAllocationCallbacks = nullptr,
        .pDeviceMemoryCallbacks = nullptr,
        .frameInUseCount = 2,
        .pHeapSizeLimit = nullptr,
        .pVulkanFunctions = nullptr,
        .pRecordSettings = nullptr,
        .instance = mInstance,
        .vulkanApiVersion = VK_API_VERSION_1_2
    };
    return vmaCreateAllocator(&info, &mAllocator);
}

// TODO: maybe create different pools for different situation and flags
auto Vulkan::createCommandPool() noexcept ->VkResult
{
    VkCommandPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = mQueueGraphicId
    };
    return vkCreateCommandPool(mDevice, &poolInfo, nullptr, &mCommandPool);
}

auto Vulkan::prepareSwapchainStatic() noexcept -> VkResult
{
    jt::vector<VkSurfaceFormatKHR> formats;
    jt::vector<VkPresentModeKHR> presentModes;
    //mSwapchain.mHasDepthImage = false;

    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(mPhysicalDevice, mSurface, &formatCount, nullptr);
    if (formatCount != 0)
    {
        formats.resize(formatCount);
        auto res = vkGetPhysicalDeviceSurfaceFormatsKHR(mPhysicalDevice, mSurface, &formatCount, formats.data());
        if (res != VK_SUCCESS)
            return res;
    }

    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(mPhysicalDevice, mSurface, &presentModeCount, nullptr);
    if (presentModeCount != 0)
    {
        presentModes.resize(presentModeCount);
        auto res = vkGetPhysicalDeviceSurfacePresentModesKHR(mPhysicalDevice, mSurface, &presentModeCount, presentModes.data());
        if (res != VK_SUCCESS)
            return res;
    }

    bool hasFormat = false;
    for(const auto& format : formats)
    {
        hasFormat = format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
        if (hasFormat)
        {
            mSwapchain.mFormat = format.format;
            mSwapchain.mColorSpace = format.colorSpace;
            break;
        }
    }
    if (!hasFormat)
        return VK_ERROR_UNKNOWN;

    bool hasPresentMode = false;
    for (const auto& mode : presentModes)
    {
        hasPresentMode = mode == VK_PRESENT_MODE_MAILBOX_KHR || mode == VK_PRESENT_MODE_FIFO_KHR;
        if (hasPresentMode)
        {
            mSwapchain.mPresentMode = mode;
            break;
        }
    }
    if (!hasPresentMode)
        return VK_ERROR_UNKNOWN;

    auto depthFormatProposals = getDepthFormatProposals();

    bool hasDepthFormat = false;
    for (const auto& f : depthFormatProposals)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(mPhysicalDevice, f, &props);
        hasDepthFormat = props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;
        if (hasDepthFormat)
        {
            mSwapchain.mDepthFormat = f;
            break;
        }
    }
    if (!hasDepthFormat)
        return VK_ERROR_UNKNOWN;

    return VK_SUCCESS;
}

auto Vulkan::prepareSwapchainDynamic(Size2D pSize) noexcept -> VkResult
{
    VkSurfaceCapabilitiesKHR capabilities;
    auto res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(mPhysicalDevice, mSurface, &capabilities);
    if (res != VK_SUCCESS)
        return res;
    mSwapchain.mPreTransform = capabilities.currentTransform;

    if (capabilities.currentExtent.width != UINT32_MAX)
        mSwapchain.mExtent = capabilities.currentExtent;
    else
        mSwapchain.mExtent = 
        { 
            .width = std::clamp(pSize.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
            .height = std::clamp(pSize.height , capabilities.minImageExtent.height, capabilities.maxImageExtent.height)
        };
    mSwapchain.mMinImageCount = std::clamp(capabilities.minImageCount+1, capabilities.minImageCount, capabilities.maxImageCount > 0 ? capabilities.maxImageCount : 256);
    return VK_SUCCESS;
}

auto Vulkan::createSwapchain() noexcept ->VkResult
{
    uint32_t indices[] = {mQueueGraphicId, mQueuePresentId};
    bool isUniqueQueueIds = mQueueGraphicId != mQueuePresentId;
    VkSwapchainCreateInfoKHR swapchainInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = nullptr,
        .flags = 0,
        .surface = mSurface,
        .minImageCount = mSwapchain.mMinImageCount,
        .imageFormat = mSwapchain.mFormat,
        .imageColorSpace = mSwapchain.mColorSpace,
        .imageExtent = mSwapchain.mExtent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = isUniqueQueueIds ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = isUniqueQueueIds ? 2u : 0u,
        .pQueueFamilyIndices = isUniqueQueueIds ? indices : nullptr,
        .preTransform = mSwapchain.mPreTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = mSwapchain.mPresentMode,
        .clipped = VK_TRUE,
        .oldSwapchain = mSwapchain.mObject
    };

    /*jt::Code code;
    if (recursivlyResize(code)) return code != jt::Code::OK ? VK_ERROR_UNKNOWN : VK_SUCCESS;*/

    auto res = vkCreateSwapchainKHR(mDevice, &swapchainInfo, nullptr, &mSwapchain.mObject);
    if (res != VK_SUCCESS) return res;

    uint32_t imageCount;
    res = vkGetSwapchainImagesKHR(mDevice, mSwapchain.mObject, &imageCount, nullptr);
    if (res != VK_SUCCESS) return res;

    mSwapchain.mImages.resize(imageCount);
    res = vkGetSwapchainImagesKHR(mDevice, mSwapchain.mObject, &imageCount, mSwapchain.mImages.data());
    if (res != VK_SUCCESS) return res;

    mSwapchain.mViews.resize(imageCount);
    for (int i = 0; i < mSwapchain.mImages.size(); i++)
    {
        VkImageViewCreateInfo viewInfo = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .image = mSwapchain.mImages[i],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = VkFormat::VK_FORMAT_B8G8R8A8_SRGB,
            .components = 
            {
                .r = VK_COMPONENT_SWIZZLE_IDENTITY, 
                .g = VK_COMPONENT_SWIZZLE_IDENTITY, 
                .b = VK_COMPONENT_SWIZZLE_IDENTITY, 
                .a = VK_COMPONENT_SWIZZLE_IDENTITY
            },
            .subresourceRange = 
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1
            }
        };
        res = vkCreateImageView(mDevice, &viewInfo, nullptr, &mSwapchain.mViews[i]);
        if (res != VK_SUCCESS) return res;
    }

    return VK_SUCCESS;
}

auto Vulkan::genDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> VkResult
{
    VkImage depthImage;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY,
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO add more apropriate
        .imageType = VkImageType::VK_IMAGE_TYPE_2D,
        .format = mSwapchain.mDepthFormat,
        .extent = { mSwapchain.mExtent.width, mSwapchain.mExtent.height, 1}, 
        .mipLevels = 1,
        .arrayLayers = 1,
        // TODO: set samples
        .samples = pMultisampling,
        .tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
        .usage = VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    auto res = vmaCreateImage(mAllocator, &imageInfo, &allocCreateInfo, &depthImage, &allocation, &allocInfo);
    if (res != VK_SUCCESS) return res;

    VkImageView depthImageView;
    VkImageViewCreateInfo imageViewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = depthImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = mSwapchain.mDepthFormat,
        .components = 
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        // TODO: aspectMask can contain also STENCIL BIT, need to do something with this
        .subresourceRange = 
        {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    res = vkCreateImageView(mDevice, &imageViewInfo, nullptr, &depthImageView);
    if (res != VK_SUCCESS) return res;

    mDepthTargets[pMultisampling] = 
    {
        .mImage = depthImage,
        .mView = depthImageView,
        .mAllocation = allocation,
        .mSize = imageInfo.extent,
        .mFormat = imageInfo.format,
        .mMipLevels = imageInfo.mipLevels,
        .mArrayLayers = imageInfo.arrayLayers,
    };

    return VK_SUCCESS;
}

auto Vulkan::destroyDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> void
{
    auto it = mDepthTargets.find(pMultisampling);

    if (it == mDepthTargets.end()) return;

    if (it->second.mView != VK_NULL_HANDLE)
        vkDestroyImageView(mDevice, it->second.mView, nullptr);

    if (it->second.mImage != VK_NULL_HANDLE)
        vmaDestroyImage(mAllocator, it->second.mImage, it->second.mAllocation);

    it->second.mView = VK_NULL_HANDLE;   
    it->second.mImage = VK_NULL_HANDLE;   
    it->second.mAllocation = VK_NULL_HANDLE;   
}

auto Vulkan::genColorTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> VkResult
{
    VkImage colorImage;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY,
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO add more apropriate
        .imageType = VkImageType::VK_IMAGE_TYPE_2D,
        .format = mSwapchain.mFormat,
        .extent = { mSwapchain.mExtent.width, mSwapchain.mExtent.height, 1}, 
        .mipLevels = 1,
        .arrayLayers = 1,
        // TODO: set samples
        .samples = pMultisampling,
        .tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };
    auto res = vmaCreateImage(mAllocator, &imageInfo, &allocCreateInfo, &colorImage, &allocation, &allocInfo);
    if (res != VK_SUCCESS) return res;

    VkImageView colorImageView;
    VkImageViewCreateInfo imageViewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = colorImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = mSwapchain.mFormat,
        .components = 
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = 
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    res = vkCreateImageView(mDevice, &imageViewInfo, nullptr, &colorImageView);
    if (res != VK_SUCCESS) return res;

    mColorTargets[pMultisampling] = 
    {
        .mImage = colorImage,
        .mView = colorImageView,
        .mAllocation = allocation,
        .mSize = imageInfo.extent,
        .mFormat = imageInfo.format,
        .mMipLevels = imageInfo.mipLevels,
        .mArrayLayers = imageInfo.arrayLayers,
    };

    return VK_SUCCESS;
}

auto Vulkan::destroyColorTargetImageFor(VkSampleCountFlagBits pMultisampling) noexcept -> void
{
    auto it = mColorTargets.find(pMultisampling);

    if (it == mColorTargets.end()) return;

    if (it->second.mView != VK_NULL_HANDLE)
        vkDestroyImageView(mDevice, it->second.mView, nullptr);

    if (it->second.mImage != VK_NULL_HANDLE)
        vmaDestroyImage(mAllocator, it->second.mImage, it->second.mAllocation);

    it->second.mView = VK_NULL_HANDLE;   
    it->second.mImage = VK_NULL_HANDLE;   
    it->second.mAllocation = VK_NULL_HANDLE;   
}

auto Vulkan::hasDepthTargetImageFor(VkSampleCountFlagBits pMultisampling) const noexcept -> bool
{
    return mDepthTargets.contains(pMultisampling);
}

auto Vulkan::hasColorTargetImageFor(VkSampleCountFlagBits pMultisampling) const noexcept -> bool
{
    return mColorTargets.contains(pMultisampling);
}

auto Vulkan::createFramesSynchronization() noexcept ->VkResult
{
    assert(mSwapchain.mImages.size());

    if (mParameters.simultaneousFrames <= 0)
        mFrames.mSimultaneousFrames = mSwapchain.mImages.size();
    else 
        mFrames.mSimultaneousFrames = std::clamp(size_t(mParameters.simultaneousFrames), 1ul, mSwapchain.mImages.size());
    
    mFrames.mFencesPerImage.resize(mSwapchain.mImages.size(), VK_NULL_HANDLE);
    mFrames.mFencesPerFrames.resize(mFrames.mSimultaneousFrames, VK_NULL_HANDLE);
    mFrames.mSemaphoresAcquireToSubmit.resize(mFrames.mSimultaneousFrames, VK_NULL_HANDLE);
    mFrames.mSemaphoresSubmitToPresent.resize(mFrames.mSimultaneousFrames, VK_NULL_HANDLE);

    VkSemaphoreCreateInfo semaphoreInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0
    };
    VkFenceCreateInfo fenceInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    auto res = VK_SUCCESS;
    for (size_t i = 0; i < mFrames.mSimultaneousFrames; i++)
    {
        res = vkCreateSemaphore(mDevice, &semaphoreInfo, nullptr, &mFrames.mSemaphoresAcquireToSubmit[i]);
        if (res != VK_SUCCESS) break; 

        res = vkCreateSemaphore(mDevice, &semaphoreInfo, nullptr, &mFrames.mSemaphoresSubmitToPresent[i]);
        if (res != VK_SUCCESS) break;

        res = vkCreateFence(mDevice, &fenceInfo, nullptr, &mFrames.mFencesPerFrames[i]);
        if (res != VK_SUCCESS) break;
    }

    return VK_SUCCESS;
}

auto Vulkan::destroyFramesSynchronization() noexcept -> void
{
    for (size_t i = 0; i < mFrames.mSimultaneousFrames; i++)
    {
        vkDestroySemaphore(mDevice, mFrames.mSemaphoresAcquireToSubmit[i], nullptr);
        vkDestroySemaphore(mDevice, mFrames.mSemaphoresSubmitToPresent[i], nullptr);
        vkDestroyFence(mDevice, mFrames.mFencesPerFrames[i], nullptr);
    }
}

#ifdef RENDER_DEBUG
auto Vulkan::destroyDebugger() noexcept -> void
{
    auto vkDestroyDebugUtilsMessengerEXT = 
        (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(mInstance, "vkDestroyDebugUtilsMessengerEXT");
    assert(mDebugMessenger);
    assert(vkDestroyDebugUtilsMessengerEXT);
    if (vkDestroyDebugUtilsMessengerEXT)
        vkDestroyDebugUtilsMessengerEXT(mInstance, mDebugMessenger, nullptr);
}
#endif

auto Vulkan::destroySwapchain() noexcept -> void
{
    for (size_t i = 0; i < mSwapchain.mViews.size(); i++)
        vkDestroyImageView(mDevice, mSwapchain.mViews[i], nullptr);

    mSwapchain.mImages.clear();
    mSwapchain.mViews.clear();
    vkDestroySwapchainKHR(mDevice, mSwapchain.mObject, nullptr);
    mSwapchain.mObject = VK_NULL_HANDLE;
}

auto jt::Pass::resize(const Renderer::Impl& pRenderer, Size2D pSize) noexcept -> jt::Code
{
    destroyFramebuffers(pRenderer);
    CHECK_VK(createFramebuffers(pRenderer, pSize));
    return jt::Code::OK;
}


auto jt::Pass::destroy(const Renderer::Impl& pRenderer) noexcept -> void
{
    destroyFramebuffers(pRenderer);
    destroyRenderPassObject(pRenderer);
}

// TODO: check if layouts for offscreen renderer is good
auto jt::Pass::createRenderPassObject(const Renderer::Impl& pRenderer, bool pIsOffscreen) noexcept -> VkResult
{
    uint32_t colorId = UINT32_MAX;
    uint32_t depthId = UINT32_MAX;
    uint32_t resolveId = UINT32_MAX;   

    jt::vector<VkAttachmentDescription> attachments;
    jt::vector<VkAttachmentReference> attachmentRefs;

    if (!mHasMultisampledColorImage)
    {
        colorId = attachments.size();
        attachmentRefs.push_back({ colorId, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
        attachments.push_back(
        {
            .flags = 0,
            //.format = pRenderer.mSwapchain.mFormat,
            .format = mColorFormat,
            .samples = mMultisampling,
            .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = !pIsOffscreen ? VK_IMAGE_LAYOUT_PRESENT_SRC_KHR : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        });
    }
    else
    {
        colorId = attachments.size();
        attachmentRefs.push_back({ colorId, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
        attachments.push_back(
        {
            .flags = 0,
            .format = mColorFormat,
            .samples = mMultisampling,
            .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        });
    }

    if (mHasDepthImage)
    {
        depthId = attachments.size();
        attachmentRefs.push_back({ depthId, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL });
        attachments.push_back(
        {
            .flags = 0,
            .format = mDepthFormat,
            .samples = mMultisampling,
            .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        });
    }

    if (mHasMultisampledColorImage)
    {
        resolveId = attachments.size();
        attachmentRefs.push_back({ resolveId, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL });
        attachments.push_back(
        {
            .flags = 0,
            .format = mResolveFormat,
            .samples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = !pIsOffscreen ? VK_IMAGE_LAYOUT_PRESENT_SRC_KHR : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        });
    }

    VkSubpassDescription subpasses = 
    {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = nullptr,
        .colorAttachmentCount = 1,
        .pColorAttachments = colorId != UINT32_MAX ? &attachmentRefs[colorId] : nullptr,
        .pResolveAttachments = resolveId != UINT32_MAX ? &attachmentRefs[resolveId] : nullptr,
        .pDepthStencilAttachment = depthId != UINT32_MAX ? &attachmentRefs[depthId] : nullptr,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = nullptr
    };
    VkSubpassDependency dependencies = 
    {
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        .dependencyFlags = 0
    };
    if (mHasDepthImage)
    {
        dependencies.srcStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependencies.dstStageMask |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        dependencies.srcAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
    }
    VkRenderPassCreateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .attachmentCount = (uint32_t)attachments.size(),
        .pAttachments = attachments.data(),
        .subpassCount = 1,
        .pSubpasses = &subpasses,
        .dependencyCount = 1,
        .pDependencies = &dependencies
    };

    return vkCreateRenderPass(pRenderer.mDevice, &info, nullptr, &mRenderPass);
}

auto jt::Pass::createFramebuffers(const Renderer::Impl& pRenderer, Size2D pSize) noexcept -> VkResult
{
    uint32_t imagesCount = 1u;
    if (mHasDepthImage) imagesCount++;
    if (mHasMultisampledColorImage) imagesCount++;

    VkFramebuffer framebuffer;
    VkFramebufferAttachmentImageInfo imageInfo[3] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .usage = mColorTargetUsage,
            .width = pSize.width,
            .height = pSize.height,
            .layerCount = 1,
            .viewFormatCount = 1,
            .pViewFormats = &mColorFormat
        },
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .usage = mDepthTargetUsage,
            .width = pSize.width,
            .height = pSize.height,
            .layerCount = 1,
            .viewFormatCount = 1,
            .pViewFormats = &mDepthFormat
        },
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .usage = mResolveTargetUsage,
            .width = pSize.width,
            .height = pSize.height,
            .layerCount = 1,
            .viewFormatCount = 1,
            .pViewFormats = &mResolveFormat
        },
    };
    VkFramebufferAttachmentsCreateInfo attachmentInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO,
        .pNext = nullptr,
        .attachmentImageInfoCount = imagesCount,
        .pAttachmentImageInfos = imageInfo
    };
    VkFramebufferCreateInfo framebufferInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = &attachmentInfo,
        .flags = VkFramebufferCreateFlagBits::VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT,
        .renderPass = mRenderPass,
        .attachmentCount = imagesCount,
        .pAttachments = nullptr,
        .width = pSize.width,
        .height = pSize.height,
        .layers = 1
    };
    return vkCreateFramebuffer(pRenderer.mDevice, &framebufferInfo, nullptr, &mFramebuffer);
}

auto jt::Pass::destroyRenderPassObject(const Renderer::Impl& pRenderer) -> void
{
    vkDestroyRenderPass(pRenderer.mDevice, mRenderPass, nullptr);
}

auto jt::Pass::destroyFramebuffers(const Renderer::Impl& pRenderer) -> void
{
    /*for (size_t i = 0; i < mFramebuffers.size(); i++)
        vkDestroyFramebuffer(pRenderer.mDevice, mFramebuffers[i], nullptr);

    mFramebuffers.clear();*/
    vkDestroyFramebuffer(pRenderer.mDevice, mFramebuffer, nullptr);
}

auto jt::Shader::create(const Renderer::Impl& pRenderer, const char* pFilename, VkShaderStageFlagBits pStage) noexcept -> jt::Code 
{
    jt::vector<uint8_t> code;
    auto res = jt::readFile(pFilename, true, code);
    if (res != jt::Code::OK)
    {
        assert(false);
        return res;
    }
    VkShaderModuleCreateInfo info = 
    {
		.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.codeSize = (uint32_t)code.size(),
		.pCode = reinterpret_cast<uint32_t*>(code.data()),
    };
    CHECK_VK(vkCreateShaderModule(pRenderer.mDevice, &info, nullptr, &mModule));
    mStage = pStage;
    return jt::Code::OK;
}

auto jt::Pipeline::create(const Renderer::Impl& pRenderer, const Pass* pPass, 
                const Shader* pVertShader, const Shader* pFragShader, 
                const jt::vector<const DescriptorLayout*>& pDescriptorLayouts, 
                const jt::vector<VertexInputBinding>& pInputBindings,
                const jt::vector<VertexInputAttribute>& pInputAttributes,
                const Viewport& pViewport,
                flags::PrimitiveTopology pTopology,
                flags::PolygonMode pPolygoneMode,
                flags::CullMode pCullMode,
                flags::FrontFace pFrontFace,
                bool pDynamicViewport,
                bool pTestDepth,
                bool pWriteDepth,
                bool pIsAlphaTransparent) noexcept -> VkResult
{
    jt::vector<VkDescriptorSetLayout> descSets;
    for (const auto& descriptor : pDescriptorLayouts)
        descSets.push_back(descriptor->mLayout);
    VkPipelineLayoutCreateInfo layoutInfo = 
    {
		.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
		.pNext = nullptr,
		.flags = 0,
		.setLayoutCount = (uint32_t)descSets.size(),
		.pSetLayouts = descSets.data(),
		.pushConstantRangeCount = 0,
		.pPushConstantRanges = nullptr
    };
    auto res = vkCreatePipelineLayout(pRenderer.mDevice, &layoutInfo, nullptr, &mLayout);
    if (res != VK_SUCCESS)
        return res;

    VkPipelineShaderStageCreateInfo stages[2];
    stages[0] = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = VK_SHADER_STAGE_VERTEX_BIT,
        .module = pVertShader->mModule,
        .pName = "main",
        .pSpecializationInfo = nullptr,
    };

    stages[1] = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = pFragShader->mModule,
        .pName = "main",
        .pSpecializationInfo = nullptr,
    };

    jt::vector<VkVertexInputBindingDescription> bindingDesc;
    for (const auto& binding : pInputBindings)
    {
        VkVertexInputBindingDescription desc = 
        {
            .binding = binding.mBinding,
            .stride = binding.mStride,
            .inputRate = binding.mIsInstanced ? VK_VERTEX_INPUT_RATE_INSTANCE : VK_VERTEX_INPUT_RATE_VERTEX
        };
        bindingDesc.push_back(desc);
    }
    jt::vector<VkVertexInputAttributeDescription> attachmentDesc;
    for (const auto& attachment : pInputAttributes)
    {
        VkVertexInputAttributeDescription desc = 
        {
            .location = attachment.mLocation,
            .binding = attachment.mBinding,
            .format = Vulkan::map(attachment.mFormat),
            .offset = attachment.mOffset,
        };
        attachmentDesc.push_back(desc);
    }
    VkPipelineVertexInputStateCreateInfo vertexInputState = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = (uint32_t)bindingDesc.size(),
        .pVertexBindingDescriptions = bindingDesc.data(),
        .vertexAttributeDescriptionCount = (uint32_t)attachmentDesc.size(),
        .pVertexAttributeDescriptions = attachmentDesc.data(),
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyState = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .topology = Vulkan::map(pTopology),
        .primitiveRestartEnable = VK_FALSE
    };

    VkViewport viewport = 
    {
        .x = (float)pViewport.pos.x,
        .y = (float)pViewport.pos.y,
        .width = (float)pViewport.size.width,
        .height = (float)pViewport.size.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    VkRect2D scissors = 
    {    
        .offset = { pViewport.pos.x, pViewport.pos.y},
        .extent = { pViewport.size.width, pViewport.size.height}
    };
    VkPipelineViewportStateCreateInfo viewportState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .viewportCount = 1,
        .pViewports = &viewport,
        .scissorCount = 1,
        .pScissors = &scissors
    };
    VkPipelineRasterizationStateCreateInfo rasterizationState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = Vulkan::map(pPolygoneMode),
        .cullMode = Vulkan::map(pCullMode),
        .frontFace = pFrontFace == flags::FrontFace::FRONT_FACE_CLOCKWISE ? VK_FRONT_FACE_CLOCKWISE : VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0,
        .depthBiasClamp = 0,
        .depthBiasSlopeFactor = 0,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisampleState = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .rasterizationSamples = pPass->mMultisampling,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 0,
        .pSampleMask = 0,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    VkPipelineDepthStencilStateCreateInfo depthStencilState = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthTestEnable = (VkBool32)pTestDepth,
        .depthWriteEnable = (VkBool32)pWriteDepth,
        .depthCompareOp = VkCompareOp::VK_COMPARE_OP_LESS, 
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0,
        .maxDepthBounds = 0
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachment = 
    {
       .blendEnable = (VkBool32)pIsAlphaTransparent,
       .srcColorBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA,
       .dstColorBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
       .colorBlendOp = VkBlendOp::VK_BLEND_OP_ADD,
       .srcAlphaBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA,
       .dstAlphaBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
       .alphaBlendOp = VkBlendOp::VK_BLEND_OP_ADD,
       .colorWriteMask = VK_COLOR_COMPONENT_R_BIT 
           | VK_COLOR_COMPONENT_G_BIT 
           | VK_COLOR_COMPONENT_B_BIT 
           | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo colorBlendState =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachment,
        .blendConstants = { 0.0f, 0.0f, 0.0f, 0.0f}
    };



    bool hasDynamicStates = pDynamicViewport;
    VkPipelineDynamicStateCreateInfo dynamicState;
    jt::vector<VkDynamicState> dynamicStatesArray;
    if (hasDynamicStates)
    {
        if (pDynamicViewport)
        {
            dynamicStatesArray.push_back(VK_DYNAMIC_STATE_VIEWPORT);
            dynamicStatesArray.push_back(VK_DYNAMIC_STATE_SCISSOR);
        }
        dynamicStatesArray[0] = VK_DYNAMIC_STATE_VIEWPORT;
        dynamicStatesArray[1] = VK_DYNAMIC_STATE_SCISSOR;
        dynamicState =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .dynamicStateCount = (uint32_t)dynamicStatesArray.size(),
            .pDynamicStates = dynamicStatesArray.data()
        };
    }
    VkGraphicsPipelineCreateInfo pipelineInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stageCount = 2,
        .pStages = stages,
        .pVertexInputState = &vertexInputState,
        .pInputAssemblyState = &inputAssemblyState,
        .pTessellationState = nullptr,
        .pViewportState = &viewportState,
        .pRasterizationState = &rasterizationState,
        .pMultisampleState = &multisampleState,
        .pDepthStencilState = &depthStencilState,
        .pColorBlendState = &colorBlendState,
        .pDynamicState = hasDynamicStates ? &dynamicState : nullptr,
        .layout = mLayout,
        .renderPass = pPass->mRenderPass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = 0
    };

    return vkCreateGraphicsPipelines(pRenderer.mDevice, nullptr, 1, &pipelineInfo, nullptr, &mPipeline);
}

auto Vulkan::DescriptorResource::destroy (const jt::Renderer::Impl& pRenderer) noexcept -> void
{
    if (mIsFreeDescriptorSetsEnabled)
    {
        jt::vector<VkDescriptorSet> sets;
        for (size_t i = 0; i < mSets.size(); i++)
            sets.push_back(mSets[i]->mSet);
        vkFreeDescriptorSets(pRenderer.mDevice, mPool, (uint32_t)sets.size(), sets.data());
    }
    vkDestroyDescriptorPool(pRenderer.mDevice, mPool, nullptr);
}

auto Vulkan::createBufferUnmapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Buffer*, Code>
{
    VkBuffer buffer;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = Vulkan::map(pMemoryUsage),
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkBufferCreateInfo bufferInfo = 
    {
       .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
       .pNext = nullptr,
       .flags = 0, // TODO ADD MORE APPROPRIATE
       .size = pSizeByte,
       .usage = Vulkan::map((jt::flags::BufferUsageFlagBits)pBufferUsage),
       .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
       .queueFamilyIndexCount = 0,
       .pQueueFamilyIndices = 0
    };

    auto res = vmaCreateBuffer(mAllocator, &bufferInfo, &allocCreateInfo, &buffer, &allocation, &allocInfo);
    if (res != VK_SUCCESS)
        return { .error = jt::Code::RENDERER_ERROR };

    mBuffers.push_back(jt::make_unique<Buffer>());
    mBuffers.back()->mBuffer = buffer;
    mBuffers.back()->mSize = bufferInfo.size;
    mBuffers.back()->mAllocation = allocation;
    mBuffers.back()->mMappedPtr = nullptr;
    return { .data = mBuffers.back().get(), .error = jt::Code::OK };
}

auto Vulkan::createBufferMapped(size_t pSizeByte, flags::BufferUsageFlags pBufferUsage, flags::MemoryUsageType pMemoryUsage, void** pMappedPtr) noexcept -> out<const Buffer*, Code>
{
    VkBuffer buffer;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = VmaAllocationCreateFlagBits::VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY,
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkBufferCreateInfo bufferInfo = 
    {
       .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
       .pNext = nullptr,
       .flags = 0, // TODO ADD MORE APPROPRIATE
       .size = pSizeByte,
       .usage = Vulkan::map((jt::flags::BufferUsageFlagBits)pBufferUsage),
       .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
       .queueFamilyIndexCount = 0,
       .pQueueFamilyIndices = 0
    };

    auto res = vmaCreateBuffer(mAllocator, &bufferInfo, &allocCreateInfo, &buffer, &allocation, &allocInfo);
    if (res != VK_SUCCESS)
        return { .error = jt::Code::RENDERER_ERROR };

    mBuffers.push_back(jt::make_unique<Buffer>());
    mBuffers.back()->mBuffer = buffer;
    mBuffers.back()->mSize = bufferInfo.size;
    mBuffers.back()->mAllocation = allocation;
    mBuffers.back()->mMappedPtr = allocInfo.pMappedData;
    if (pMappedPtr)
        *pMappedPtr = allocInfo.pMappedData;
    return { .data = mBuffers.back().get(), .error = jt::Code::OK };
}
auto Vulkan::createImage(const VkExtent3D& pExtent, VkImageType pImageType, VkImageViewType pImageViewType, flags::Format pFormat, uint32_t pMipLevels, uint32_t pLayers, flags::ImageUsageFlags pImageUsage, flags::MemoryUsageType pMemoryUsage) noexcept -> out<const Image*, Code>
{
    VkImage image;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = Vulkan::map(pMemoryUsage),
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO add more apropriate
        .imageType = pImageType,
        .format = Vulkan::map(pFormat),
        .extent = pExtent, 
        .mipLevels = pMipLevels,
        .arrayLayers = pLayers,
        .samples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT, 
        .tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
        .usage = Vulkan::map((jt::flags::ImageUsageFlagBits)pImageUsage),
        .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };
    auto res = vmaCreateImage(mAllocator, &imageInfo, &allocCreateInfo, &image, &allocation, &allocInfo);
    if (res != VK_SUCCESS) return { .error = jt::Code::RENDERER_ERROR };

    VkImageView imageView;
    VkImageViewCreateInfo imageViewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = image,
        .viewType = pImageViewType,
        .format = Vulkan::map(pFormat),
        .components = 
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = 
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = pMipLevels,
            .baseArrayLayer = 0,
            .layerCount = pLayers
        }
    };

    res = vkCreateImageView(mDevice, &imageViewInfo, nullptr, &imageView);
    if (res != VK_SUCCESS) return { .error = jt::Code::RENDERER_ERROR };

    mImages.push_back(jt::make_unique<Image>());
    mImages.back()->mImage = image;
    mImages.back()->mView = imageView;
    mImages.back()->mSize = imageInfo.extent;
    mImages.back()->mFormat = imageInfo.format;
    mImages.back()->mAllocation = allocation;
    mImages.back()->mMipLevels = pMipLevels;
    mImages.back()->mArrayLayers = pLayers;

    return { .data = mImages.back().get(), .error = jt::Code::OK };
}

auto Vulkan::createImageTarget(const VkExtent3D& pExtent, VkFormat pFormat, VkImageUsageFlags pImageUsage, VkImageAspectFlags pAspectBits, flags::Multisampling pSampling) noexcept -> out<const Image*, Code>
{
    VkImage image;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };
    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO add more apropriate
        .imageType = VkImageType::VK_IMAGE_TYPE_2D,
        .format = pFormat,
        .extent = pExtent, 
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = Vulkan::map(pSampling), 
        .tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
        .usage = pImageUsage,
        .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    auto res = vmaCreateImage(mAllocator, &imageInfo, &allocCreateInfo, &image, &allocation, &allocInfo);
    if (res != VK_SUCCESS) return { .error = jt::Code::RENDERER_ERROR };

    VkImageView imageView;
    VkImageViewCreateInfo imageViewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = image,
        .viewType = VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
        .format = pFormat,
        .components = 
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = 
        {
            .aspectMask = pAspectBits,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    res = vkCreateImageView(mDevice, &imageViewInfo, nullptr, &imageView);
    if (res != VK_SUCCESS) return { .error = jt::Code::RENDERER_ERROR };

    mImages.push_back(jt::make_unique<Image>());
    mImages.back()->mImage = image;
    mImages.back()->mView = imageView;
    mImages.back()->mSize = imageInfo.extent;
    mImages.back()->mFormat = imageInfo.format;
    mImages.back()->mAllocation = allocation;
    mImages.back()->mMipLevels = 1;
    mImages.back()->mArrayLayers = 1;

    return { .data = mImages.back().get(), .error = jt::Code::OK };
}

auto Vulkan::createSampler(flags::SamplerFilter pMagFilter, flags::SamplerFilter pMinFilter, flags::SamplerMipmapMode pMipmapMode, flags::SamplerAddressMode pAddressModeUVW[3], bool pEnableAnisotropy, uint32_t mipMaps, flags::SamplerBorderColor pBorderColor) noexcept -> out<const Sampler*, Code>
{
    VkSampler sampler;

    VkPhysicalDeviceProperties props {};
    vkGetPhysicalDeviceProperties(mPhysicalDevice, &props);

    VkSamplerCreateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .magFilter = Vulkan::map(pMagFilter),
        .minFilter = Vulkan::map(pMinFilter),
        .mipmapMode = Vulkan::map(pMipmapMode),
        .addressModeU = Vulkan::map(pAddressModeUVW[0]),
        .addressModeV = Vulkan::map(pAddressModeUVW[1]),
        .addressModeW = Vulkan::map(pAddressModeUVW[2]),
        .mipLodBias = 0.0f,
        .anisotropyEnable = pEnableAnisotropy ? VK_TRUE : VK_FALSE,
        .maxAnisotropy = props.limits.maxSamplerAnisotropy,
        .compareEnable = VK_FALSE,
        .compareOp = VkCompareOp::VK_COMPARE_OP_ALWAYS,
        .minLod = 0,
        .maxLod = (float)mipMaps,
        .borderColor = Vulkan::map(pBorderColor),
        .unnormalizedCoordinates = VK_FALSE
    };

    auto res = vkCreateSampler(mDevice, &info, nullptr, &sampler);
    if (res != VK_SUCCESS)
        return { .error = jt::Code::RENDERER_ERROR };

    mSamplers.push_back(jt::make_unique<Sampler>());
    mSamplers.back()->mSampler = sampler;
    return { .data = mSamplers.back().get(), .error = jt::Code::OK };
}

auto Vulkan::destroyShader(const Shader* pShader) noexcept -> void
{
    auto it = mShaders.find(pShader);
    assert(it != mShaders.end());
    if (it != mShaders.end())
    {
        vkDestroyShaderModule(mDevice, it->first->mModule, nullptr);
        mShaders.erase(it);
    }
}

// WARNING: thread unsafe operation, because we can map same memory
// RESOLVING: but if we use vma library it is not the case
// WARNING: if memory is non coherent we need call vkFlushMappedMemoryRanges after memcpy in raw Vulkan case
auto Vulkan::writeBufferUnmapped(const void* pSrcData, const Buffer* pDstBufferCPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopySizeByte + pDstOffsetByte);
    assert(!pDstBufferCPU->mMappedPtr);

    void* mappedData;
    vmaMapMemory(mAllocator, pDstBufferCPU->mAllocation, &mappedData);
    memcpy((uint8_t*)mappedData + pDstOffsetByte, (uint8_t*)pSrcData + pSrcOffsetByte, pCopySizeByte);
    vmaUnmapMemory(mAllocator, pDstBufferCPU->mAllocation);
}

auto Vulkan::writeBufferUnmapped(const jt::vector<IndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopyCommandCount * pSrcIndirectData.size() * sizeof(IndirectCommand) + pDstOffsetByte);
    assert(!pDstBufferCPU->mMappedPtr);

    void* mappedData;
    vmaMapMemory(mAllocator, pDstBufferCPU->mAllocation, &mappedData);
    memcpy((uint8_t*)mappedData + pDstOffsetByte, pSrcIndirectData.data() + pFirstCommand, pCopyCommandCount * sizeof(IndirectCommand));
    vmaUnmapMemory(mAllocator, pDstBufferCPU->mAllocation);
}

auto Vulkan::writeBufferUnmapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopyCommandCount * pSrcIndirectData.size() * sizeof(IndexedIndirectCommand) + pDstOffsetByte);
    assert(!pDstBufferCPU->mMappedPtr);

    void* mappedData;
    vmaMapMemory(mAllocator, pDstBufferCPU->mAllocation, &mappedData);
    memcpy((uint8_t*)mappedData + pDstOffsetByte, pSrcIndirectData.data() + pFirstCommand, pCopyCommandCount * sizeof(IndexedIndirectCommand));
    vmaUnmapMemory(mAllocator, pDstBufferCPU->mAllocation);
}

auto Vulkan::writeBufferMapped(const void* pSrcData, const Buffer* pDstBufferCPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopySizeByte + pDstOffsetByte);
    assert(pDstBufferCPU->mMappedPtr);

    memcpy((uint8_t*)pDstBufferCPU->mMappedPtr + pDstOffsetByte, (uint8_t*)pSrcData + pSrcOffsetByte, pCopySizeByte);
}

auto Vulkan::writeBufferMapped(const jt::vector<IndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopyCommandCount * pSrcIndirectData.size() * sizeof(IndirectCommand) + pDstOffsetByte);
    assert(pDstBufferCPU->mMappedPtr);

    memcpy((uint8_t*)pDstBufferCPU->mMappedPtr + pDstOffsetByte, pSrcIndirectData.data() + pFirstCommand, pCopyCommandCount * sizeof(IndirectCommand));
}

auto Vulkan::writeBufferMapped(const jt::vector<IndexedIndirectCommand>& pSrcIndirectData, const Buffer* pDstBufferCPU, size_t pFirstCommand, size_t pDstOffsetByte, size_t pCopyCommandCount) noexcept -> void
{
    assert(pDstBufferCPU->mSize >= pCopyCommandCount * pSrcIndirectData.size() * sizeof(IndexedIndirectCommand) + pDstOffsetByte);
    assert(pDstBufferCPU->mMappedPtr);

    memcpy((uint8_t*)pDstBufferCPU->mMappedPtr + pDstOffsetByte, pSrcIndirectData.data() + pFirstCommand, pCopyCommandCount * sizeof(IndexedIndirectCommand));
}

auto Vulkan::createCommandBuffer() noexcept -> out<const CommandBuffer*, Code>
{
    VkCommandBuffer buffer;
    VkCommandBufferAllocateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = mCommandPool,
        .level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };
    auto res = vkAllocateCommandBuffers(mDevice, &info, &buffer);
    if (res != VK_SUCCESS)
        return {.error = jt::Code::RENDERER_ERROR};

    mCommandBuffers.push_back(jt::make_unique<CommandBuffer>());
    mCommandBuffers.back()->mBuffer = buffer;
    return { .data = mCommandBuffers.back().get(), .error = jt::Code::OK };
}

auto Vulkan::resize(Size2D pSize) noexcept -> jt::Code
{
    vkDeviceWaitIdle(mDevice);

    //destroyFramesSynchronization();
    destroySwapchain();
    for (auto [key, _] : mDepthTargets) destroyDepthTargetImageFor(key);
    for (auto [key, _] : mColorTargets) destroyColorTargetImageFor(key);

    CHECK_VK(prepareSwapchainDynamic(pSize));
    CHECK_VK(createSwapchain());
    for (auto [key, _] : mDepthTargets) genDepthTargetImageFor(key);
    for (auto [key, _] : mColorTargets) genColorTargetImageFor(key);
    //CHECK_VK(createFramesSynchronization());

    for (size_t i = 0; i < mRenderPassesOnscreen.size(); i++)
        mRenderPassesOnscreen[i]->resize(*this, {mSwapchain.mExtent.width, mSwapchain.mExtent.height});

    return jt::Code::OK;
}

// TODO: some hack for X11 resizing events
/*auto Vulkan::recursivlyResize(jt::Code& pCode) noexcept -> bool
{
    VkSurfaceCapabilitiesKHR capabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(mPhysicalDevice, mSurface, &capabilities);
    if (capabilities.currentExtent.width != UINT32_MAX)
        if (capabilities.currentExtent.width != mSwapchain.mExtent.width 
                || capabilities.currentExtent.height != mSwapchain.mExtent.height)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(256));
            pCode = resize(capabilities.currentExtent.width, capabilities.currentExtent.height);
            return true;
        }

    return false;
}*/

auto Vulkan::drawBegin() noexcept -> out<uint32_t, Code>
{
    assert(mCurrentDrawState != DrawState::BEGIN);
    mCurrentDrawState = DrawState::BEGIN;

    vkWaitForFences(mDevice, 1, mFrames.getFenceForCurrentFrame(), VK_TRUE, UINT64_MAX);
    vkResetFences(mDevice, 1, mFrames.getFenceForCurrentFrame());

    uint32_t imageId;
    VkResult res = vkAcquireNextImageKHR(mDevice, mSwapchain.mObject, 
            UINT64_MAX, *mFrames.getCurrentAcquireToSubmitSemaphore(), VK_NULL_HANDLE, &imageId);

    mSwapchain.mCurrentImage = imageId;
    mCurrentCommandBuffers.clear();

    // TODO: double check why here we dont use also VK_SUBOPTIMAL_KHR
    return 
    {
        .data = mFrames.mCurrentFrame,
        .error = res == VK_SUCCESS ? 
            jt::Code::OK : res == VK_ERROR_OUT_OF_DATE_KHR? 
            jt::Code::RENDERER_RESIZE_REQUIRE : 
            jt::Code::RENDERER_ERROR
    };
}

auto Vulkan::addCommandBuffer(const CommandBuffer* pCommandBuffer) noexcept ->void
{
    if (mCurrentDrawState == DrawState::BEGIN)
        mCurrentCommandBuffers.push_back(GET_VK_CB(pCommandBuffer));
    else if (mCurrentDrawState == DrawState::BEGIN_COMPUTE)
        mCurrentCommandBuffersCompute.push_back(pCommandBuffer->mBuffer);
    else
        assert(false);
}

auto Vulkan::drawSubmitWait() noexcept -> Code
{
    assert(mCurrentDrawState == DrawState::BEGIN);
    mCurrentDrawState = DrawState::END;

    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    VkSubmitInfo submitInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = mFrames.getCurrentAcquireToSubmitSemaphore(),
        .pWaitDstStageMask = waitStages,
        .commandBufferCount = (uint32_t)mCurrentCommandBuffers.size(),
        .pCommandBuffers = mCurrentCommandBuffers.data(),
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = mFrames.getCurrentSubmitToPresentSemaphore()
    };

    auto res = vkQueueSubmit(mQueueGraphic, 1, &submitInfo, *mFrames.getFenceForCurrentFrame());

    VkPresentInfoKHR presentInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = mFrames.getCurrentSubmitToPresentSemaphore(),
        .swapchainCount = 1,
        .pSwapchains = &mSwapchain.mObject,
        .pImageIndices = &mSwapchain.mCurrentImage
    };

    res = vkQueuePresentKHR(mQueuePresent, &presentInfo);

    mFrames.nextCurrentFrame();

    //vkDeviceWaitIdle(mDevice);
    vkQueueWaitIdle(mQueuePresent);

    return res == VK_SUCCESS ? 
        jt::Code::OK : res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR? 
        jt::Code::RENDERER_RESIZE_REQUIRE : 
        jt::Code::RENDERER_ERROR;
}

auto Vulkan::drawSubmitLeave() noexcept -> Code
{
    assert(mCurrentDrawState == DrawState::BEGIN);
    mCurrentDrawState = DrawState::END;

    // We need this block if we would have more simultaneous frames than images
    {
        auto fenceForCurrentImage = mFrames.getFenceForCurrentImage(mSwapchain.mCurrentImage);
        if (*fenceForCurrentImage != VK_NULL_HANDLE) 
            vkWaitForFences(mDevice, 1, fenceForCurrentImage, VK_TRUE, UINT64_MAX);
        mFrames.setFenceForCurrentImage(mFrames.getFenceForCurrentFrame(), mSwapchain.mCurrentImage);
    }
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

    VkSubmitInfo submitInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = mFrames.getCurrentAcquireToSubmitSemaphore(),
        .pWaitDstStageMask = waitStages,
        .commandBufferCount = (uint32_t)mCurrentCommandBuffers.size(),
        .pCommandBuffers = mCurrentCommandBuffers.data(),
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = mFrames.getCurrentSubmitToPresentSemaphore()
    };

    auto res = vkQueueSubmit(mQueueGraphic, 1, &submitInfo, *mFrames.getFenceForCurrentFrame());

    VkPresentInfoKHR presentInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = mFrames.getCurrentSubmitToPresentSemaphore(),
        .swapchainCount = 1,
        .pSwapchains = &mSwapchain.mObject,
        .pImageIndices = &mSwapchain.mCurrentImage
    };

    res = vkQueuePresentKHR(mQueuePresent, &presentInfo);

    mFrames.nextCurrentFrame();

    return res == VK_SUCCESS ? 
        jt::Code::OK : res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR? 
        jt::Code::RENDERER_RESIZE_REQUIRE : 
        jt::Code::RENDERER_ERROR;
}

auto Vulkan::computeBegin() noexcept -> Code
{
    assert(mCurrentDrawState != DrawState::BEGIN && mCurrentDrawState != DrawState::BEGIN_COMPUTE);
    mCurrentDrawState = DrawState::BEGIN_COMPUTE;
    mCurrentCommandBuffersCompute.clear();
    return jt::Code::OK;
}

auto Vulkan::computeSubmitWait() noexcept -> Code
{
    assert(mCurrentDrawState == DrawState::BEGIN_COMPUTE);
    assert(!mCurrentCommandBuffersCompute.empty());
    mCurrentDrawState = DrawState::END;

    VkSubmitInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = nullptr,
        .pWaitDstStageMask = 0,
        .commandBufferCount = (uint32_t)mCurrentCommandBuffersCompute.size(),
        .pCommandBuffers = mCurrentCommandBuffersCompute.data(),
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = nullptr
    };
    auto res = vkQueueSubmit(mQueueCompute, 1, &info, nullptr);
    if (res != VK_SUCCESS) return jt::Code::RENDERER_ERROR;

    res = vkQueueWaitIdle(mQueueCompute);
    if (res != VK_SUCCESS) return jt::Code::RENDERER_ERROR;

    return jt::Code::OK;
}

auto Vulkan::recordCmdBegin(const CommandBuffer* pCBuffer) noexcept -> void
{
    auto cb = GET_VK_CB(pCBuffer);
    if (cb != VK_NULL_HANDLE)
    {
        vkResetCommandBuffer(cb, VkCommandBufferResetFlagBits::VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
    }
    VkCommandBufferBeginInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = 0,
        .pInheritanceInfo = nullptr
    };
    // TODO: this return VK_RESULT
    vkBeginCommandBuffer(cb, &info);
}

auto Vulkan::recordCmdEnd(const CommandBuffer* pCBuffer) noexcept -> void
{
    // TODO: this return VK_RESULT
    vkEndCommandBuffer(GET_VK_CB(pCBuffer));
}

// TODO: with imageless frambuffer we can extend this api to add more images target
auto Vulkan::cmdPassBegin(const CommandBuffer* pCBuffer, PassOnscreen pPass, float pClearColor[4]) noexcept -> void
{
    auto pass = pPass.ptr;
    uint32_t imagesCount = 1u;
    if (pass->mHasDepthImage) imagesCount++;
    if (pass->mHasMultisampledColorImage) imagesCount++;

    VkImageView images[3]; // it controled by imagesCount variable
    if (!pass->mHasMultisampledColorImage)
        images[0] = mSwapchain.mViews[mSwapchain.mCurrentImage];
    else
    {
        // TODO: maybe cache this inside RenderPass to remove map finding each frame
        images[0] = mColorTargets.at(pass->mMultisampling).mView;
        images[2] = mSwapchain.mViews[mSwapchain.mCurrentImage];
    }
    if (pass->mHasDepthImage)
        // TODO: maybe cache this inside RenderPass to remove map finding each frame
        images[1] = mDepthTargets.at(pass->mMultisampling).mView;

    VkClearValue clearColors[2] = 
    {
        { .color = {pClearColor[0], pClearColor[1], pClearColor[2], pClearColor[3]} },
        { .depthStencil = {.depth = 1.0f, .stencil = 0} }, // it controled by imagesCount variable
    }; 

    VkRenderPassAttachmentBeginInfo attachment_info = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO,
        .pNext = nullptr,
        .attachmentCount = imagesCount,
        .pAttachments = images 
    };

    VkRenderPassBeginInfo info = 
    {
       .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
       .pNext = &attachment_info,
       .renderPass = pass->mRenderPass,
       .framebuffer = pass->mFramebuffer,
       .renderArea = 
       {
           .offset = {0, 0},
           .extent = mSwapchain.mExtent
       },
       .clearValueCount = imagesCount,
       .pClearValues = clearColors
    };

    vkCmdBeginRenderPass(GET_VK_CB(pCBuffer), &info, VK_SUBPASS_CONTENTS_INLINE);
}

auto Vulkan::cmdPassBegin(const CommandBuffer* pCBuffer, PassOffscreen pPass, float pClearColor [4],
                ImageTarget<TargetType::COLOR> pColorTarget, 
                jt::option<ImageTarget<TargetType::DEPTH>> pDepthTarget, 
                jt::option<ImageTarget<TargetType::RESOLVE>> pResolveTarget) noexcept -> void
{
    auto pass = pPass.ptr;
    assert(pass->mHasDepthImage ? pDepthTarget : !pDepthTarget);
    assert(pass->mHasMultisampledColorImage ? pResolveTarget : !pResolveTarget);
    uint32_t imagesCount = 1u;
    if (pass->mHasDepthImage) imagesCount++;
    if (pass->mHasMultisampledColorImage) imagesCount++;

    VkImageView images[3]; // it controled by imagesCount variable
    images[0] = pColorTarget.ptr->mView;
    if (pass->mHasDepthImage)
        images[1] = pDepthTarget.value().ptr->mView;
    if (pass->mHasMultisampledColorImage && pResolveTarget)
        images[2] = pResolveTarget.value().ptr->mView;


    VkClearValue clearColors[2] = 
    {
        { .color = {pClearColor[0], pClearColor[1], pClearColor[2], pClearColor[3]} },
        { .depthStencil = {.depth = 1.0f, .stencil = 0} }, // it controled by imagesCount variable
    }; 

    VkRenderPassAttachmentBeginInfo attachment_info = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO,
        .pNext = nullptr,
        .attachmentCount = imagesCount,
        .pAttachments = images 
    };

    VkRenderPassBeginInfo info = 
    {
       .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
       .pNext = &attachment_info,
       .renderPass = pass->mRenderPass,
       .framebuffer = pass->mFramebuffer,
       .renderArea = 
       {
           .offset = {0, 0},
           // TODO: maybe use data directly from render pass
           .extent = {pColorTarget.ptr->mSize.width, pColorTarget.ptr->mSize.height}
       },
       .clearValueCount = imagesCount,
       .pClearValues = clearColors
    };

    vkCmdBeginRenderPass(GET_VK_CB(pCBuffer), &info, VK_SUBPASS_CONTENTS_INLINE);
}

auto Vulkan::cmdPassEnd(const CommandBuffer* pCBuffer, const Pass* /*pPass*/) noexcept -> void
{
    vkCmdEndRenderPass(GET_VK_CB(pCBuffer));
}

auto Vulkan::cmdBindPipeline(const CommandBuffer* pCBuffer, const Pipeline* pPipeline) noexcept -> void
{
    vkCmdBindPipeline(GET_VK_CB(pCBuffer), pPipeline->mBindPoint, pPipeline->mPipeline);
}

auto Vulkan::cmdBindDescriptorSets(const CommandBuffer* pCBuffer, const Pipeline* pPipeline, const jt::vector<const DescriptorSet*>& pDescriptorSets, const jt::vector<uint32_t>& pDynamicOffsets) noexcept -> void
{
    auto sets = (VkDescriptorSet*)alloca(sizeof(VkDescriptorSet) * pDescriptorSets.size());

    for (size_t i = 0; i < pDescriptorSets.size(); i++)
        sets[i] = pDescriptorSets[i]->mSet;

    vkCmdBindDescriptorSets(GET_VK_CB(pCBuffer), pPipeline->mBindPoint, pPipeline->mLayout, 0, pDescriptorSets.size(), sets, pDynamicOffsets.size(), pDynamicOffsets.data() );
}

auto Vulkan::cmdBindVertexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetInBytes) noexcept -> void
{
    VkDeviceSize offsets = pOffsetInBytes;
    vkCmdBindVertexBuffers(GET_VK_CB(pCBuffer), 0, 1, &pBuffer->mBuffer, &offsets);
}

auto Vulkan::cmdBindIndexBuffer(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, flags::IndexType pType) noexcept -> void
{
    vkCmdBindIndexBuffer(GET_VK_CB(pCBuffer), pBuffer->mBuffer, pOffsetByte, Vulkan::map(pType));
}

auto Vulkan::cmdLoadVertexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    VkDeviceSize size = pCopySizeByte != SIZE_MAX ? pCopySizeByte : pInputBufferCPU->mSize;

    assert(size <= pOutputBufferGPU->mSize);
    VkBufferCopy region = 
    {
        .srcOffset = pSrcOffsetByte,
        .dstOffset = pDstOffsetByte,
        .size = size
    };

    vkCmdCopyBuffer(GET_VK_CB(pCBuffer), pInputBufferCPU->mBuffer, pOutputBufferGPU->mBuffer, 1, &region);

    VkBufferMemoryBarrier barrier = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = pOutputBufferGPU->mBuffer,
        .offset = pDstOffsetByte,
        .size = size
    };
    vkCmdPipelineBarrier(GET_VK_CB(pCBuffer), VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 0, nullptr, 1, &barrier, 0, nullptr);
}

auto Vulkan::cmdLoadIndexData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Buffer* pOutputBufferGPU, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    VkDeviceSize size = pCopySizeByte != SIZE_MAX ? pCopySizeByte : pInputBufferCPU->mSize;

    assert(size <= pOutputBufferGPU->mSize);
    VkBufferCopy region = 
    {
        .srcOffset = pSrcOffsetByte,
        .dstOffset = pDstOffsetByte,
        .size = size
    };

    vkCmdCopyBuffer(GET_VK_CB(pCBuffer), pInputBufferCPU->mBuffer, pOutputBufferGPU->mBuffer, 1, &region);

    VkBufferMemoryBarrier barrier = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_INDEX_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = pOutputBufferGPU->mBuffer,
        .offset = pDstOffsetByte,
        .size = size
    };
    vkCmdPipelineBarrier(GET_VK_CB(pCBuffer), VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 0, nullptr, 1, &barrier, 0, nullptr);
}

auto Vulkan::cmdLoadTextureData(const CommandBuffer* pCBuffer, const Buffer* pInputBufferCPU, const Image* pOutputImage) noexcept -> void
{
    VkImageSubresourceRange subresourceRange = 
    {
        // TODO: add suport for different mip levels !!!!!!!!!!!!!!!!!!!!!!!!!!!!
        .aspectMask = VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1
    };

    // ------------- TRANSFER FROM UNDEFINED TO TRANSFER DST LAYOUT ---------------
    VkImageMemoryBarrier undefinedToTransferDst = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = pOutputImage->mImage,
        .subresourceRange = subresourceRange
    };
    // TODO: used only for fragment shader, maybe parametrize where we want this image
    vkCmdPipelineBarrier(GET_VK_CB(pCBuffer), VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &undefinedToTransferDst);

    // ------------- COPY DATA FROM BUFFER FROM SYSTEM MEMORY TO IMAGE MEMORY ---------------
    VkBufferImageCopy region = 
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = 
        {
            .aspectMask = subresourceRange.aspectMask,
            .mipLevel = subresourceRange.baseMipLevel,
            .baseArrayLayer = subresourceRange.baseArrayLayer,
            .layerCount = subresourceRange.layerCount
        },
        .imageOffset = {0, 0, 0},
        .imageExtent = pOutputImage->mSize
    };
    vkCmdCopyBufferToImage(GET_VK_CB(pCBuffer), pInputBufferCPU->mBuffer, pOutputImage->mImage, VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    // ------------- TRANSFER FROM TRANSFER DST TO SHADER READ BIT LAYOUT ---------------
    VkImageMemoryBarrier transferDstToShaderRead = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = pOutputImage->mImage,
        .subresourceRange = subresourceRange
    };
    // TODO: used only for fragment shader, maybe parametrize where we want this image
    vkCmdPipelineBarrier(GET_VK_CB(pCBuffer), VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &transferDstToShaderRead);
}

auto Vulkan::cmdCopyBuffer(const CommandBuffer* pCBuffer, const Buffer* pSrcBuffer, const Buffer* pDstBuffer, size_t pSrcOffsetByte, size_t pDstOffsetByte, size_t pCopySizeByte) noexcept -> void
{
    VkDeviceSize size = pCopySizeByte != SIZE_MAX ? pCopySizeByte : pSrcBuffer->mSize;

    assert(size <= pDstBuffer->mSize);
    VkBufferCopy region = 
    {
        .srcOffset = pSrcOffsetByte,
        .dstOffset = pDstOffsetByte,
        .size = size
    };

    vkCmdCopyBuffer(GET_VK_CB(pCBuffer), pSrcBuffer->mBuffer, pDstBuffer->mBuffer, 1, &region);
}

auto Vulkan::cmdDraw(const CommandBuffer* pCBuffer, uint32_t pVertexCount, uint32_t pInstanceCount, uint32_t pFirstVertex, uint32_t pFirstInstance) noexcept -> void
{
    vkCmdDraw(GET_VK_CB(pCBuffer), pVertexCount, pInstanceCount, pFirstVertex, pFirstInstance); 
}

auto Vulkan::cmdDrawIndexed(const CommandBuffer* pCBuffer, uint32_t pIndexCount, uint32_t pInstanceCount, uint32_t pFirstIndex, int32_t pVertexOffset, uint32_t pFirstInstance) noexcept -> void
{
    vkCmdDrawIndexed(GET_VK_CB(pCBuffer), pIndexCount, pInstanceCount, pFirstIndex, pVertexOffset, pFirstInstance);
}

auto Vulkan::cmdDrawIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void
{
    vkCmdDrawIndirect(GET_VK_CB(pCBuffer), pBuffer->mBuffer, pOffsetByte, pDrawCount, pStrideByte);
}

auto Vulkan::cmdDrawIndexedIndirect(const CommandBuffer* pCBuffer, const Buffer* pBuffer, size_t pOffsetByte, uint32_t pDrawCount, uint32_t pStrideByte) noexcept -> void
{
    vkCmdDrawIndexedIndirect(GET_VK_CB(pCBuffer), pBuffer->mBuffer, pOffsetByte, pDrawCount, pStrideByte);
}

auto Vulkan::cmdDispatch(const CommandBuffer* pCBuffer, uint32_t pGroupCountX, uint32_t pGroupCountY, uint32_t pGroupCountZ) noexcept -> void
{
    vkCmdDispatch(GET_VK_CB(pCBuffer), pGroupCountX, pGroupCountY, pGroupCountZ);
}

auto Vulkan::cmdSetViewport(const CommandBuffer* pCBuffer, const Viewport& pViewport) noexcept -> void
{
    VkViewport viewport = 
    {
        .x = (float)pViewport.pos.x,
        .y = (float)pViewport.pos.y,
        .width = (float)pViewport.size.width,
        .height = (float)pViewport.size.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };

    VkRect2D scissors = 
    {    
        .offset = { pViewport.pos.x, pViewport.pos.y},
        .extent = { pViewport.size.width, pViewport.size.height}
    };

    vkCmdSetViewport(GET_VK_CB(pCBuffer), 0, 1, &viewport);
    vkCmdSetScissor(GET_VK_CB(pCBuffer), 0, 1, &scissors);
}

auto Vulkan::cmdBarrier(const CommandBuffer* pCBuffer, 
                flags::PipelineStageFlags pSrcStages, flags::PipelineStageFlags pDstStages,
                uint32_t pBuffersCount, const BufferRegionBarrier* pBuffers,
                uint32_t pImageCount, const ImageDataBarrier* pImages) noexcept -> void
{
    auto bufferBarriers = pBuffersCount ? (VkBufferMemoryBarrier*)alloca(pBuffersCount * sizeof(VkBufferMemoryBarrier)) : nullptr;
    auto imageBarriers = pImageCount ? (VkImageMemoryBarrier*)alloca(pImageCount * sizeof(VkImageMemoryBarrier)) : nullptr;
    for (size_t i = 0; i < pBuffersCount; i++)
    {
        const auto& buffer = pBuffers[i];
        bufferBarriers[i] = 
        {
            .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
            .pNext = nullptr,
            .srcAccessMask = (VkAccessFlags)buffer.mSrcAccess,
            .dstAccessMask = (VkAccessFlags)buffer.mDstAccess,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .buffer = buffer.mRegion.mBuffer->mBuffer,
            .offset = buffer.mRegion.mOffset,
            .size = buffer.mRegion.mRange
        };
    }
    for (size_t i = 0; i < pImageCount; i++)
    {
        const auto& image = pImages[i];
        imageBarriers[i] = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = nullptr,
            .srcAccessMask = (VkAccessFlags)image.mSrcAccess,
            .dstAccessMask = (VkAccessFlags)image.mDstAccess,
            .oldLayout = (VkImageLayout)image.mOldLayout,
            .newLayout = (VkImageLayout)image.mNewLayout,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = image.mImage->mImage,
            .subresourceRange = 
            {
                // TODO: add support for different aspect
                .aspectMask = VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = image.mImage->mMipLevels,
                .baseArrayLayer = 0,
                .layerCount = image.mImage->mArrayLayers
            }
        };
    }
    vkCmdPipelineBarrier(GET_VK_CB(pCBuffer), 
            (VkPipelineStageFlagBits)pSrcStages, (VkPipelineStageFlagBits)pDstStages, 0, 
            0, nullptr, 
            pBuffersCount, bufferBarriers, 
            pImageCount, imageBarriers);
}

auto Vulkan::destroy() noexcept -> void
{
    for (size_t i = 0; i < mFrames.mSimultaneousFrames; i++)
    {
        if (mFrames.mFencesPerFrames[i] != VK_NULL_HANDLE)
            vkWaitForFences(mDevice, 1, &mFrames.mFencesPerFrames[i], VK_TRUE, UINT64_MAX);
    }

    for (size_t i = 0; i < mPipelines.size(); i++)
    {
        vkDestroyPipeline(mDevice, mPipelines[i]->mPipeline, nullptr);
        vkDestroyPipelineLayout(mDevice, mPipelines[i]->mLayout, nullptr);
    }
    mPipelines.clear();

    for (size_t i = 0; i < mDescriptorLayouts.size(); i++)
    {
        vkDestroyDescriptorSetLayout(mDevice, mDescriptorLayouts[i]->mLayout, nullptr);
    }
    mDescriptorLayouts.clear();

    for (size_t i = 0; i < mDescriptorResources.size(); i++)
    {
        mDescriptorResources[i]->destroy(*this);
    }
    mDescriptorResources.clear();

    for (const auto& shader : mShaders)
    {
        vkDestroyShaderModule(mDevice, shader.first->mModule, nullptr);
    }
    mShaders.clear();

    for (size_t i = 0; i < mRenderPassesOnscreen.size(); i++)
    {
        mRenderPassesOnscreen[i]->destroy(*this);
    }
    mRenderPassesOnscreen.clear();

    for (size_t i = 0; i < mRenderPassesOffscreen.size(); i++)
    {
        mRenderPassesOffscreen[i]->destroy(*this);
    }
    mRenderPassesOffscreen.clear();
    
    for (size_t i = 0; i < mSamplers.size(); i++)
    {
        vkDestroySampler(mDevice, mSamplers[i]->mSampler, nullptr);
    }
    mSamplers.clear();

    for (size_t i = 0; i < mBuffers.size(); i++)
    {
        vmaDestroyBuffer(mAllocator, mBuffers[i]->mBuffer, mBuffers[i]->mAllocation);
    }
    mBuffers.clear();

    for (size_t i = 0; i < mImages.size(); i++)
    {
        vkDestroyImageView(mDevice, mImages[i]->mView, nullptr);
        vmaDestroyImage(mAllocator, mImages[i]->mImage, mImages[i]->mAllocation);
    }
    mImages.clear();

    for (size_t i = 0; i < mCommandBuffers.size(); i++)
    {
        auto cmb = mCommandBuffers[i].get();
        vkFreeCommandBuffers(mDevice, mCommandPool, 1, &cmb->mBuffer);
    }
    mCommandBuffers.clear();

    // DESTROY SYSTEM DATA
    destroyFramesSynchronization();

    for (auto [key, _] : mDepthTargets) destroyDepthTargetImageFor(key);
    mDepthTargets.clear();

    for (auto [key, _] : mColorTargets) destroyColorTargetImageFor(key);
    mColorTargets.clear();

    destroySwapchain();

    vkDestroyCommandPool(mDevice, mCommandPool, nullptr);
    
    vmaDestroyAllocator(mAllocator);

    vkDestroyDevice(mDevice, nullptr);

    vkDestroySurfaceKHR(mInstance, mSurface, nullptr);

#ifdef RENDER_DEBUG
    destroyDebugger();
#endif

    vkDestroyInstance(mInstance, nullptr);
}
