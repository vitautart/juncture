#ifndef JT_LIB_MAP_HPP
#define JT_LIB_MAP_HPP

#include <map>

#include "jtlib/allocator.hpp"

namespace jt 
{
    template<typename K, typename V> using map = std::map<K, V, std::less<K>, allocator<std::pair<const K, V>>>;
}

#endif // JT_LIB_MAP_HPP
