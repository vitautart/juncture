#ifndef JT_LIB_PRINT_HPP
#define JT_LIB_PRINT_HPP

#include <cstdio>

namespace jt
{
    template <typename T>
    inline void println(const char* format, T data)
    {
        printf(format, data);
        printf("\n");
    }

    inline void println(const char* str)
    {
        printf("%s\n", str);
    }

    template <typename T>
    inline void print(const char* format, T data)
    {
        printf(format, data);
    }
}

#endif // JT_LIB_PRINT_HPP
