#ifndef JT_LIB_DATATYPES_HPP
#define JT_LIB_DATATYPES_HPP

#include <cinttypes>
#include <type_traits>

namespace jt
{      
    template <typename T1, typename T2>
    struct pair
    {
        T1 first;
        T2 second;
    };

    template <typename T1, typename T2, typename T3>
    struct triple
    {
        T1 first;
        T2 second;
        T3 third;
    };

    template<typename T, typename U>
    struct variant
    {
        variant() = delete;
        explicit variant(T data) noexcept : mType{0}, mData{ .mFirst = data} {}
        explicit variant(U data) noexcept : mType{1}, mData{ .mSecond = data} {}

        template <typename G>
        auto get() const noexcept -> G
        {
            static_assert(std::is_same<G, T>::value || std::is_same<G, U>::value);
            if constexpr (std::is_same<G, T>::value)
                return mData.mFirst;
            else if constexpr (std::is_same<G, U>::value)
                return mData.mSecond;
        }

        template <typename G>
        auto is() const noexcept -> bool
        {
            static_assert(std::is_same<G, T>::value || std::is_same<G, U>::value);
            return (std::is_same<G, T>::value && mType == 0) 
                || (std::is_same<G, U>::value && mType == 1);
        }

    private:
        int mType;
        union Data
        {
            T mFirst;
            U mSecond;
        } mData;
    };

    template<typename T>
    struct option
    {
        option(T data) noexcept : mData(data), mIsSome(true) {};
        option() noexcept : mIsSome(false) {};
        operator bool() const noexcept { return mIsSome; }
        auto value() noexcept -> T { return mData; }
    private:
        bool mIsSome;
        T mData;
    };

    struct Position2D
    {
        int32_t x;
        int32_t y;
    };

    struct Size2D
    {
        uint32_t width;
        uint32_t height;
    };

    struct Size3D
    {
        uint32_t width;
        uint32_t height;
        uint32_t depth;
    };
}

#endif // JT_LIB_DATATYPES_HPP
