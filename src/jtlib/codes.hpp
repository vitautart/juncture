#ifndef JT_LIB_CODES_HPP
#define JT_LIB_CODES_HPP

namespace jt
{   
    enum class Code
    {
        OK,
        RENDERER_RESIZE_REQUIRE,
        RENDERER_ERROR,
        IO_ERROR,
        SURFACE_ERROR,
    };

    template <typename Data, typename CodeType>
    struct out
    {
        Data data;
        CodeType error;
    };
}

#endif // JT_LIB_CODES_HPP
