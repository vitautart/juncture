#ifndef JT_LIB_VECTOR_HPP
#define JT_LIB_VECTOR_HPP

#include <vector>

#include "jtlib/allocator.hpp"

namespace jt 
{
    template<typename T> using vector = std::vector<T, allocator<T>>;
}

#endif // JT_LIB_VECTOR_HPP
