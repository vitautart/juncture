#ifndef JTLIB_TAG_HPP
#define JTLIB_TAG_HPP

#include <concepts>

namespace jt
{
    template<typename What, typename... Args>
        constexpr bool is_present = (std::is_same_v<What, Args> || ...);

    template<typename Tag>
    struct tag
    {
        typename Tag::value data;

        template<template <typename...> typename T, typename ...Args>
        tag(const T<Args...>& input) requires is_present<Tag, Args...>
        {
            data = Tag::get(input);
        }
    };
}

#endif

