#ifndef JT_LIB_LIST_HPP
#define JT_LIB_LIST_HPP

#include <list>

#include "jtlib/allocator.hpp"

namespace jt 
{
    template<typename T> using list = std::list<T, allocator<T>>;
}

#endif // JT_LIB_LIST_HPP
