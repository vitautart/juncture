#ifndef JT_LIB_UNIQUE_PTR_HPP
#define JT_LIB_UNIQUE_PTR_HPP

#include <memory>

#include "jtlib/print.hpp"

namespace jt
{
    template<typename T, typename... Args>
    std::unique_ptr<T> make_unique(Args&&... args) noexcept
    {
        auto ptr = new (std::nothrow) T(std::forward<Args>(args)...);
        if (ptr) [[likely]]
            return std::unique_ptr<T>(ptr);
        else [[unlikely]]
        {
            println("jt::allocator: out of memory!");
            std::terminate();
        }
    }
}
#endif // JT_LIB_UNIQUE_PTR_HPP
