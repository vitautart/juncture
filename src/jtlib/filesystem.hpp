#ifndef JT_LIB_FILESYSTEM_HPP
#define JT_LIB_FILESYSTEM_HPP

#include "jtlib/codes.hpp"
#include "jtlib/vector.hpp"

#include <bits/types/FILE.h>
#include <cstdint>
#include <cstdio>
#include <malloc.h>
#include <new>

namespace jt
{   

    class FileHandle 
    {
    public:
        FileHandle() = delete;
        FileHandle(const char* pFilename, const char* pMode) noexcept
        {
            mRawFile = fopen(pFilename, pMode);
        }
        FileHandle(const FileHandle&) = delete;
        auto operator = (const FileHandle&) = delete;

        auto getRawFile() noexcept -> FILE*
        {
            return mRawFile;
        }

        ~FileHandle()
        {
            if (mRawFile)
                fclose(mRawFile);
        }
    private:
        FILE* mRawFile = nullptr;
    };

    inline auto readFile(const char* filename, bool binary, jt::vector<uint8_t>& data) noexcept -> Code
    {
        const char* mode = binary ? "rb" : "r";
        auto file = FileHandle(filename, mode);
        auto fp = file.getRawFile();
        if(!fp)
            return jt::Code::IO_ERROR;

        int seek_res = fseek(fp, 0, SEEK_END);
        if (seek_res != 0)
            return jt::Code::IO_ERROR;

        long m_size = ftell(fp);
        if(m_size == -1L)
            return jt::Code::IO_ERROR;

        long null_term = binary ? 0 : 1;
        
        long size = m_size + null_term;

        data.resize(size);
        rewind(fp);

        size_t read_res = fread(data.data(), 1, m_size, fp);
        if(read_res != m_size)
            return jt::Code::IO_ERROR;

        if (!binary)
            data.back() = '\0';

        return jt::Code::OK;
    }
}

#endif // JT_LIB_FILESYSTEM_HPP
