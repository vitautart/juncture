#ifndef JT_LIB_STRING_HPP
#define JT_LIB_STRING_HPP

#include <string>

#include "jtlib/allocator.hpp"

namespace jt 
{
    using string = std::basic_string<char, std::char_traits<char>, allocator<char>>;
}

#endif // JT_LIB_STRING_HPP
