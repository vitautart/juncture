#ifndef JT_LIB_ALLOCATOR_HPP
#define JT_LIB_ALLOCATOR_HPP

#include <new>
#include <exception>

#include "jtlib/print.hpp"

#if 0
void* operator new (std::size_t size, const std::nothrow_t& tag) noexcept
{
    printf("MALLOC");
    return malloc(size);
}
#endif

namespace jt
{
    template <typename T>
    struct allocator
    {
        using value_type = T;
        [[nodiscard]] T* allocate(size_t n) noexcept
        {
            auto ptr = static_cast<T*>(::operator new(sizeof(T) * n, std::nothrow));

            if (ptr) [[likely]]
                return ptr;
            else [[unlikely]]
            {
                println("jt::allocator: out of memory!");
                std::terminate();
            }
        }

        void deallocate(T* p, size_t n) noexcept
        {
            ::operator delete(p);
        }
    };

    template <typename T, typename U>
    bool operator == (allocator<T>, allocator<U>) noexcept
    {
        return true;
    }

    template <typename T, typename U>
    bool operator != (allocator<T>, allocator<U>) noexcept
    {
        return false;
    }

    //  WHY?
    //  BECAUSE IF WE DON'T HAVE VALID MEMORY, 
    //  BEST SOLUTION JUST TERMINATE
    inline auto if_null_terminate(void* ptr) noexcept
    {
        if (!ptr)
        {
            println("jt::allocator: out of memory!");
            std::terminate();
        }
    }
}

#endif // JT_LIB_ALLOCATOR_HPP
