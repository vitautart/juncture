#include "surface.hpp"
#include <cstdint>
#include <unistd.h>

#define GLFW_EXPOSE_NATIVE_X11
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#include <cassert>

#include "jtlib/codes.hpp"
#include "jtlib/allocator.hpp"
#include "native_surface.hpp"

namespace jt 
{
    struct Surface::Impl
    {
        auto create(int pWidth, int pHeight, const char* pName, bool pResizable) noexcept -> jt::Code;
        auto destroy() noexcept -> void;

        GLFWwindow* mWindow;
        NativeSurface mNativeSurface;
        static int WINDOWS_COUNT;
        std::function<void (Size2D)> mOnFramebufferResizeCB = nullptr;
        std::function<void(flags::Keys, flags::InputAction, flags::InputModFlags)> mOnKeyCB = nullptr;
    };
}

int jt::Surface::Impl::WINDOWS_COUNT = 0;

jt::Surface::Surface(jt::Surface&& pSurface) noexcept
{
    mImpl = pSurface.mImpl;
    pSurface.mImpl = nullptr;
}

auto jt::Surface::operator= (jt::Surface&& pSurface) noexcept -> jt::Surface&
{
    mImpl = pSurface.mImpl;
    pSurface.mImpl = nullptr;
    return *this;
}

jt::Surface::~Surface()
{
    if (mImpl)
    {
        mImpl->destroy();
        delete mImpl;
    }
}

auto jt::Surface::make(int pWidth, int pHeight, const char* pName, bool pResizable) noexcept -> jt::out<jt::Surface, jt::Code>
{
    Surface r;
    r.mImpl = new (std::nothrow) Surface::Impl;
    if_null_terminate(r.mImpl);
    auto code = r.mImpl->create(pWidth, pHeight, pName, pResizable);
    return {std::move(r), code};
}

auto jt::Surface::shouldClose() const noexcept -> bool
{
    return glfwWindowShouldClose(mImpl->mWindow);
}

auto jt::Surface::setShouldClose(bool pShouldClose) noexcept -> void
{
    glfwSetWindowShouldClose(mImpl->mWindow, pShouldClose);
}

auto jt::Surface::pollEvents() noexcept -> void
{
    glfwPollEvents();
}

auto jt::Surface::waitEvents() noexcept -> void
{
    glfwWaitEvents();
}

auto jt::Surface::getNativeSurface() const noexcept -> const NativeSurface&
{
    return mImpl->mNativeSurface;
}

auto jt::Surface::getWindowSize() const noexcept -> Size2D
{
    int width, height;
    glfwGetWindowSize(mImpl->mWindow, &width, &height);
    return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
}

auto jt::Surface::getFramebufferSize() const noexcept -> Size2D
{
    int width, height;
    glfwGetFramebufferSize(mImpl->mWindow, &width, &height);
    return { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
}

auto jt::Surface::setFramebufferResizeCB(std::function<void(Size2D)> pCallback) noexcept -> void
{   
    if (pCallback && !mImpl->mOnFramebufferResizeCB)
    {
        glfwSetFramebufferSizeCallback(mImpl->mWindow, [](GLFWwindow* window, int w, int h)
        {
            auto surface = (Surface::Impl*)glfwGetWindowUserPointer(window);
            if (surface->mOnFramebufferResizeCB)
                surface->mOnFramebufferResizeCB({static_cast<uint32_t>(w), static_cast<uint32_t>(h)});
        });
    }
    mImpl->mOnFramebufferResizeCB = pCallback;
}

auto jt::Surface::setKeyCB(std::function<void(flags::Keys, flags::InputAction, flags::InputModFlags)> pCallback) noexcept -> void
{
    if (pCallback && !mImpl->mOnKeyCB)
    {
        glfwSetKeyCallback(mImpl->mWindow, [](GLFWwindow* window, int key, int scanCode, int action, int mods)
        {
            auto surface = (Surface::Impl*)glfwGetWindowUserPointer(window);
            if (surface->mOnKeyCB)
                surface->mOnKeyCB((flags::Keys)key, (flags::InputAction)action, (flags::InputModFlags)mods);
        });
    }
    mImpl->mOnKeyCB = pCallback;
}

auto jt::Surface::Impl::create(int pWidth, int pHeight, const char* pName, bool pResizable = false) noexcept -> jt::Code
{
    if (!glfwInit())
    {
        assert(false);
        return jt::Code::SURFACE_ERROR;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_RESIZABLE, pResizable ? GLFW_TRUE : GLFW_FALSE);
    //glfwWindowHint(GLFW_FLOATING, GLFW_TRUE);
    //glfwWindowHint(GLFW_MAXIMIZED, GLFW_FALSE);

    mWindow = glfwCreateWindow(pWidth, pHeight, pName, nullptr, nullptr);
    if (!mWindow)
    {
        if(WINDOWS_COUNT < 1) glfwTerminate();
        return jt::Code::SURFACE_ERROR;
    }

    glfwSetWindowUserPointer(mWindow, this);

    WINDOWS_COUNT++;

    //glfwWaitEventsTimeout(0.008);

#if __linux__
    mNativeSurface.mDisplay = glfwGetX11Display();
    mNativeSurface.mWindow = glfwGetX11Window(mWindow);
#endif

    return jt::Code::OK;
}


auto jt::Surface::Impl::destroy() noexcept -> void 
{
    glfwDestroyWindow(mWindow);
    WINDOWS_COUNT--;
    if(WINDOWS_COUNT < 1) glfwTerminate();
}
