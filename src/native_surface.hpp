#ifndef JT_NATIVE_SURFACE_HPP
#define JT_NATIVE_SURFACE_HPP

#if __linux__
#include <X11/X.h>
#include <X11/Xlib.h>
#elif _WIN32 || _WIN64 
#include <Windows.h>
#endif

namespace jt
{
struct NativeSurface
{
#if __linux__
    Window mWindow;
    Display* mDisplay;
#endif
};
}
#endif // JT_NATIVE_SURFACE_HPP 
