#ifndef JT_RENDERER_EXT_HPP
#define JT_RENDERER_EXT_HPP

#include "renderer.hpp"
#include <cassert>

namespace jt
{
    // Alternative more compact API for Renderer::createDescriptorSet
    class ResourceBinder
    {
    public:
        ResourceBinder(Renderer& pRenderer) noexcept : mRenderer(pRenderer){}
        auto newSet(const DescriptorLayout* layout) noexcept -> ResourceBinder&
        {
            mResources.emplace_back();
            mLayouts.push_back(layout);
            return *this;
        }
        auto addBuffer (const BufferRegion& buffer) noexcept -> ResourceBinder&
        {
            assert(!mResources.empty());
            mResources.back().push_back(jt::variant<BufferRegion, ImageData>(buffer));
            return *this;
        }
        auto addImage (const ImageData& image) noexcept -> ResourceBinder&
        {
            assert(!mResources.empty());
            mResources.back().push_back(jt::variant<BufferRegion, ImageData>(image));
            return *this;
        }
        auto clear() noexcept -> ResourceBinder&
        {
            mLayouts.clear();
            mResources.clear();
            return *this;
        }
        auto bind() noexcept -> out<jt::vector<const jt::DescriptorSet*>, Code>
        {
            return mRenderer.createDescriptorSets(mLayouts, mResources);
        }
    private:
        jt::vector<DescriptorSetResources> mResources;
        jt::vector<const DescriptorLayout*> mLayouts;
        Renderer& mRenderer;
    };

    // TODO: maybe add dynamic implementation of this API also.
    template<size_t BufferCount, size_t ImageCount = 0>
    class BarrierBuilder
    {
    public:
        BarrierBuilder(Renderer& pRenderer) noexcept : mRenderer{pRenderer}
        {
            static_assert(BufferCount != 0 || ImageCount != 0);
        }
        auto addBuffer(const jt::BufferRegionBarrier& pBufferRegion) noexcept -> BarrierBuilder&
        {
            assert(mCurrentBuffer < BufferCount);
            mBuffers[mCurrentBuffer] = pBufferRegion;
            mCurrentBuffer++;
            return *this;
        }
        auto addImage(const jt::ImageDataBarrier& pImageData) noexcept -> BarrierBuilder&
        {
            assert(mCurrentImage < ImageCount);
            mImages[mCurrentImage] = pImageData;
            mCurrentImage++;
            return *this;
        }
        auto setSrcStages(jt::flags::PipelineStageFlags pStages) noexcept -> BarrierBuilder&
        {
            mSrcStages = pStages;
            return *this;
        }
        auto setDstStages(jt::flags::PipelineStageFlags pStages) noexcept -> BarrierBuilder&
        {
            mDstStages = pStages;
            return *this;
        }
        auto cmd(const CommandBuffer* pCBuffer) noexcept -> void
        {
            mRenderer.cmdBarrier(pCBuffer, mSrcStages, mDstStages, mCurrentBuffer, 
                    mBuffers, mCurrentImage, mImages);
        }
    private:
        BarrierBuilder();
        jt::flags::PipelineStageFlags mSrcStages;
        jt::flags::PipelineStageFlags mDstStages;
        jt::BufferRegionBarrier mBuffers[BufferCount];
        jt::ImageDataBarrier mImages[ImageCount];
        uint32_t mCurrentBuffer = 0;
        uint32_t mCurrentImage = 0;
        jt::Renderer& mRenderer;
    };
}

#endif // JT_RENDERER_EXT_HPP
