#pragma once

//#include <concepts>
//#include <type_traits>
#include <math.h>
#include <cinttypes>
#include <cstdio>
#include <type_traits>

namespace jth {

template <typename T>
concept Scalar =  requires (T s)
{ 
    {s + s} -> std::same_as<T>;
    {s * s} -> std::same_as<T>;
    {s - s} -> std::same_as<T>;
    {  - s} -> std::same_as<T>;
} && !requires (T s) { {s[0]}; };

template <typename T>
concept Vector = Scalar<typename T::field> && requires (T v)
{ 
    typename T::field;
    {T::rank()} -> std::same_as<size_t>;
    {v[1]} -> std::convertible_to<typename T::field>;
    {v + v} -> std::same_as<T>;
    {v * v} -> std::same_as<T>;
    {v - v} -> std::same_as<T>;
    {  - v} -> std::same_as<T>;
};

template<Scalar S, size_t count> struct semantic { };
template<Scalar S> struct semantic<S, 2> { S x,y; };
template<Scalar S> struct semantic<S, 3> { S x,y,z; };
template<Scalar S> struct semantic<S, 4> { S x,y,z,w; };

template <Scalar S, size_t c>
union vec
{
// : compiletime
    using type = vec<S, c>;
    using field = S;
    consteval static auto rank() -> size_t  { return c; }

// : data
    S data[c];
    semantic<S, c> semantix;

// : mutators
    inline auto operator->() noexcept -> semantic<S, c>* { return &semantix; };
    inline auto operator[](size_t i) noexcept -> S& { return data[i]; }

    inline auto operator+=(const type& v) noexcept -> void
    {
        data[0] += v.data[0];
        data[1] += v.data[1];
        if constexpr (c > 2) data[2] += v.data[2];
        if constexpr (c > 3) data[3] += v.data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            data[i] += v.data[i];
    }
    inline auto operator-=(const type& v) noexcept -> void
    {
        data[0] -= v.data[0];
        data[1] -= v.data[1];
        if constexpr (c > 2) data[2] -= v.data[2];
        if constexpr (c > 3) data[3] -= v.data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            data[i] -= v.data[i];
    }

// : non-mutators
    inline constexpr auto operator->() const noexcept -> const semantic<S, c>* { return &semantix; };
    inline constexpr auto operator[](size_t i) const noexcept -> const S& { return data[i]; }

    inline constexpr auto operator+(const type& v) const noexcept -> type
    {
        type res;
        res.data[0] = data[0] + v.data[0];
        res.data[1] = data[1] + v.data[1];
        if constexpr (c > 2) res.data[2] = data[2] + v.data[2];
        if constexpr (c > 3) res.data[3] = data[3] + v.data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            res.data[i] = data[i] + v.data[i];
        return res;
    }
    inline constexpr auto operator*(const type& v) const noexcept -> type
    {
        type res;
        res.data[0] = data[0] * v.data[0];
        res.data[1] = data[1] * v.data[1];
        if constexpr (c > 2) res.data[2] = data[2] * v.data[2];
        if constexpr (c > 3) res.data[3] = data[3] * v.data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            res.data[i] = data[i] * v.data[i];
        return res;
    }
    inline constexpr auto operator*(const S& v) const noexcept -> type
    {
        type res;
        res.data[0] = data[0] * v;
        res.data[1] = data[1] * v;
        if constexpr (c > 2) res.data[2] = data[2] * v;
        if constexpr (c > 3) res.data[3] = data[3] * v;
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            res.data[i] = data[i] * v;
        return res;
    }

    inline constexpr auto operator-(const type& rhs) const noexcept -> type
    {
        type res;
        res.data[0] = data[0] - rhs.data[0];
        res.data[1] = data[1] - rhs.data[1];
        if constexpr (c > 2) res.data[2] = data[2] - rhs.data[2];
        if constexpr (c > 3) res.data[3] = data[3] - rhs.data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            res.data[i] = data[i] - rhs.data[i];
        return res;
    }
    inline constexpr auto operator-() const noexcept -> type 
    {
        type res;
        res.data[0] = - data[0];
        res.data[1] = - data[1];
        if constexpr (c > 2) res.data[2] = - data[2];
        if constexpr (c > 3) res.data[3] = - data[3];
        if constexpr (c > 4) for (size_t i = 4; i < c; i++) 
            res.data[i] = - data[i];
        return res;
    }



    static inline auto zero() noexcept -> vec<S, c>
    {
        return {0};
    }
};

using vec2 = vec<float, 2>;
using vec3 = vec<float, 3>;
using vec4 = vec<float, 4>;

using dvec2 = vec<double, 2>;
using dvec3 = vec<double, 3>;
using dvec4 = vec<double, 4>;

using ivec2 = vec<int32_t, 2>;
using ivec3 = vec<int32_t, 3>;
using ivec4 = vec<int32_t, 4>;

using uvec2 = vec<uint32_t, 2>;
using uvec3 = vec<uint32_t, 3>;
using uvec4 = vec<uint32_t, 4>;

template<Scalar S, size_t c>
inline constexpr auto operator*(const S& s, const vec<S, c>& v) noexcept -> vec<S, c>
{
    vec<S, c> res;
    res.data[0] = s * v.data[0];
    res.data[1] = s * v.data[1];
    if constexpr (c > 2) res.data[2] = s * v.data[2];
    if constexpr (c > 3) res.data[3] = s * v.data[3];
    if constexpr (c > 4) for (size_t i = 4; i < v.rank(); i++) 
        res.data[i] = s * v.data[i];
    return res;
}

// TODO: maybe refactore and benchmark it to one for and lenghtSq check
template<Scalar S, size_t c> requires std::is_floating_point_v<S>
inline auto equal(const vec<S, c> v1, const vec<S, c> v2, S delta) noexcept -> bool
{
    for (size_t i = 0; i < c; i++)
            if (abs(v1[i] - v2[i]) > delta)
                return false;
    return true;
}

template<Scalar S, size_t c> requires std::is_integral_v<S>
inline auto equal(const vec<S, c> v1, const vec<S, c> v2) noexcept -> bool
{
    for (size_t i = 0; i < c; i++)
            if (v1[i] != v2[i])
                return false;
    return true;
}

template <Vector T>
inline auto sum (const T& v) noexcept -> typename T::field
{
    typename T::field res = 0;
    for (int i = 0; i < v.rank(); i++)
        res += v.data[i];
    return res;
}

template <Vector T>
inline auto dot (const T& v1, const T& v2) noexcept -> typename T::field
{
    if constexpr (T::rank() == 2)
        return v1[0] * v2[0] + v1[1] * v2[1];
    else if constexpr (T::rank() == 3)
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
    else if constexpr (T::rank() == 4)
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3];
    else
        return sum(v1 * v2);
}

template <Vector T>
inline auto length_sq (const T& v1) noexcept -> typename T::field
{
    if constexpr (T::rank() == 2)
        return v1[0] * v1[0] + v1[1] * v1[1];
    else if constexpr (T::rank() == 3)
        return v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
    else if constexpr (T::rank() == 4)
        return v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2] + v1[3] * v1[3];
    else
        return sum(v1 * v1);
}

template<Scalar S, size_t c> requires std::is_floating_point_v<S>
inline auto length (const vec<S, c>& v1) noexcept -> S
{
    if constexpr (c == 2 && sizeof(S) <= 4)
        return sqrtf(v1[0] * v1[0] + v1[1] * v1[1]);
    else if constexpr (c == 2 && sizeof(S) == 8)
        return sqrt(v1[0] * v1[0] + v1[1] * v1[1]);
    else if constexpr (c == 3 && sizeof(S) <= 4)
        return sqrtf(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);
    else if constexpr (c == 3 && sizeof(S) == 8)
        return sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2]);
    else if constexpr (c == 4 && sizeof(S) <= 4)
        return sqrtf(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2] + v1[3] * v1[3]);
    else if constexpr (c == 4 && sizeof(S) == 8)
        return sqrt(v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2] + v1[3] * v1[3]);
    else
        return sqrt(sum(v1 * v1));
}

template<Scalar S, size_t c> requires std::is_floating_point_v<S>
inline auto norm (const vec<S, c>& v) noexcept -> vec<S, c>
{
    auto s = 1 / length(v);
    return v * s;
}

template <Scalar S>
inline auto cross(const vec<S, 3>& v1, const vec<S, 3> v2) noexcept -> vec<S, 3>
{
    return 
    {
        v1[1] * v2[2] - v1[2] * v2[1],
        - v1[0] * v2[2] + v1[2] * v2[0],
        v1[0] * v2[1] - v1[1] * v2[0]
    };
}

template <Scalar S>
inline auto cross(const vec<S, 2>& v1, const vec<S, 2> v2) noexcept -> S
{
    return v1[0] * v2[1] - v1[1] * v2[0];
}

template <typename T>
concept Matrix = Scalar<typename T::field> && requires (T v)
{ 
    typename T::field;
    {T::rank()} -> std::same_as<vec<size_t, 2>>;
    {v[1]} -> std::convertible_to<typename T::field>;
    {v + v} -> std::same_as<T>;
    {v * v} -> std::same_as<T>;
    {v - v} -> std::same_as<T>;
    {  - v} -> std::same_as<T>;
};

// Column-major matrix
// r - elements in row
// c - elements in collumn
template <Scalar S, size_t r, size_t c>
struct mat 
{
// : compiletime
    using type = mat<S, r, c>;
    using type_transposed = mat<S, c, r>;
    using field = S;
    constexpr static auto rank() -> const vec<size_t, 2>  { return {r, c}; }

// : data
    //S data[r * c];
    vec<S, r> data[c];

// : mutators
    inline auto operator[](size_t i) -> vec<S, r>& { return data[i]; }

// : non-mutators
    inline auto operator[](size_t i) const -> const vec<S, r>& { return data[i]; }
    inline auto operator+(const type& m) const -> type
    {
        type res;
        for (size_t i = 0; i < c; i++)
            res.data[i] = data[i] + m.data[i];
        return res;
    }
    inline auto operator-(const type& m) const -> type
    {
        type res;
        for (size_t i = 0; i < c; i++)
            res.data[i] = data[i] - m.data[i];
        return res;
    }
    inline auto operator*(const S& s) const -> type
    {
        type res;
        for (size_t i = 0; i < c; i++)
            res.data[i] = data[i] * s;
        return res;
    }

    inline auto transpose() const noexcept -> type_transposed
    {
        type_transposed res;
        for (int col = 0; col < c; col++)
            for (int row = 0; row < r; row++)
                res[row][col] = data[col][row];
        return res;
    }

    static inline auto zero() noexcept -> mat<S, r, c>
    {
        return {0};
    }

    static inline auto id() noexcept -> mat<S, c, c>
    {
        if constexpr (c == 2)
            return 
            {
                1, 0,
                0, 1,
            };
        else if constexpr (c == 3)
            return 
            {
                1, 0, 0,
                0, 1, 0,
                0, 0, 1,
            };
        else if constexpr (c == 4)
            return 
            {
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1,
            };
        else
        {
            auto result = mat<S, c, c>::zero();
            for (size_t i = 0; i < c; i++)
                result[c][c] = static_cast<S>(1);
            return result;
        }
    }
};

template <Scalar S, size_t r, size_t c, size_t c2>
inline auto operator*(const mat<S, r, c>& m1, const mat<S, c, c2>& m2) noexcept -> mat<S, r, c2>
{
    mat<S, r, c2> res;
    mat<S, c, r> transp = m1.transpose();
    for (size_t col = 0; col < c2; col++)
        for (size_t row = 0; row < r; row++)
            res[col][row] = dot(transp.data[row], m2.data[col]);
    return res;
}

// TODO: maybe refactore and benchmark it to one for and lenghtSq check
template<Scalar S, size_t r, size_t c> requires std::is_floating_point_v<S>
inline auto equal(const mat<S, r, c> m1, const mat<S, r, c> m2, S delta) noexcept -> bool
{
    for (size_t col = 0; col < c; col++)
        for (size_t row = 0; row < r; row++)
            if (abs(m1[col][row] - m2[col][row]) > delta)
                return false;
    return true;
}

template<Scalar S, size_t r, size_t c> requires std::is_integral_v<S>
inline auto equal(const mat<S, r, c> m1, const mat<S, r, c> m2) noexcept -> bool
{
    for (size_t col = 0; col < c; col++)
        for (size_t row = 0; row < r; row++)
            if (m1[col][row] != m2[col][row])
                return false;
    return true;
}

using mat2 = mat<float, 2, 2>;
using mat3 = mat<float, 3, 3>;
using mat4 = mat<float, 4, 4>;

using dmat2 = mat<double, 2, 2>;
using dmat3 = mat<double, 3, 3>;
using dmat4 = mat<double, 4, 4>;

template<Scalar S> requires std::is_floating_point_v<S>
inline auto translate(const vec<S, 3>& v) noexcept -> mat<S, 4, 4>
{
    return 
    {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        v.data[0], v.data[1], v.data[2], 1
    };
}

// Axis must be normalized, angle in radians
template<Scalar S> requires std::is_floating_point_v<S>
inline auto rot(const vec<S, 3>& axis, S angle) noexcept -> mat<S, 4, 4>
{
    const S c = static_cast<float>(cos(angle));
    const S s = static_cast<float>(sin(angle));

    auto term = (1 - c) * axis;

    mat<S, 4, 4> output;
    output[0][0] = term[0] * axis[0] + c;
    output[0][1] = term[0] * axis[1] + s * axis[2];
    output[0][2] = term[0] * axis[2] - s * axis[1];
    output[0][3] = 0;

    output[1][0] = term[1] * axis[0] - s * axis[2];
    output[1][1] = term[1] * axis[1] + c;
    output[1][2] = term[1] * axis[2] + s * axis[0];
    output[1][3] = 0;

    output[2][0] = term[2] * axis[0] + s * axis[1];
    output[2][1] = term[2] * axis[1] - s * axis[0];
    output[2][2] = term[2] * axis[2] + c;
    output[2][3] = 0;

    output[3] = vec<S, 4>{0, 0, 0, 1};

    return output;
}

template<Scalar S> requires std::is_floating_point_v<S>
inline auto ortho_proj_rh(S width, S height, S nearZ, S farZ) noexcept -> mat<S, 4, 4>
{
    auto output = mat<S, 4, 4>::zero();
    output.data[0][0] = 2 / width;
    output.data[1][1] = -2 / height;
    output.data[2][2] = 1 / (nearZ - farZ);
    output.data[3][2] = nearZ * output.data[2][2];
    output.data[3][3] = 1;
    return output;
}

// Perspective projection for NDC(x : [-1, 1]; y [-1, 1]; z : [0, 1])
// RHS(x : right; y : down; z : from us) -> RHS(x: right; y : up; z : on us)
// So basicaly this is a transformation from vulkan ndc to opengl view conventional coorinate system,
// or other isomorphic transformations.
// In glm library it is glm::perspectiveRH_ZO with flliped [1][1] element.
template<Scalar S> requires std::is_floating_point_v<S>
inline auto persp_proj_rh(S verticalFov, S width, S height, S nearZ, S farZ) noexcept -> mat<S, 4, 4>
{
    static_assert(sizeof(S) <= 8);
    S f; 
    if constexpr (sizeof(S) <= 4)
        f = 1.0f / tanf(verticalFov * 0.5f);
    else if constexpr (sizeof(S) == 8)
        f = 1.0 / tan(verticalFov * 0.5);

    S near_far = 1 / (nearZ - farZ);

    auto output = mat<S, 4, 4>::zero();

    output[0][0] = f * height / width;
    output[1][1] = -f;
    output[2][2] = farZ * near_far;

    output[2][3] = -1;
    output[3][2] = (farZ * nearZ) * near_far;
    return output;
}

template<Scalar S> requires std::is_floating_point_v<S>
inline auto look_at_rh(vec<S, 3> pos, vec<S, 3> target, vec<S, 3> up) noexcept -> mat<S, 4, 4>
{
    mat<S, 4, 4> output;

    vec<S, 3> f = norm(target - pos);
    vec<S, 3> s = norm(cross(f, up));
    vec<S, 3> u = cross(s, f);

    output[0][0] = s[0];
    output[0][1] = u[0];
    output[0][2] = -f[0];
    output[0][3] = 0;

    output[1][0] = s[1];
    output[1][1] = u[1];
    output[1][2] = -f[1];
    output[1][3] = 0;

    output[2][0] = s[2];
    output[2][1] = u[2];
    output[2][2] = -f[2];
    output[2][3] = 0;

    output[3][0] = -dot(s, pos);
    output[3][1] = -dot(u, pos);
    output[3][2] = dot(f, pos);
    output[3][3] = 1;
    return output;
}

template<Scalar S> requires std::is_floating_point_v<S>
inline auto look_at_lh(vec<S, 3> pos, vec<S, 3> target, vec<S, 3> up) noexcept -> mat<S, 4, 4>
{
    mat<S, 4, 4> output;

    vec<S, 3> f = norm(target - pos);
    vec<S, 3> s = norm(cross(up, f));
    vec<S, 3> u = cross(f, s);

    output[0][0] = s[0];
    output[0][1] = u[0];
    output[0][2] = f[0];
    output[0][3] = 0;

    output[1][0] = s[1];
    output[1][1] = u[1];
    output[1][2] = f[1];
    output[1][3] = 0;

    output[2][0] = s[2];
    output[2][1] = u[2];
    output[2][2] = f[2];
    output[2][3] = 0;

    output[3][0] = -dot(s, pos);
    output[3][1] = -dot(u, pos);
    output[3][2] = -dot(f, pos);
    output[3][3] = 1;
    return output;
}

} // jth
