#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 col;

layout (set = 0, binding = 0) uniform Camera 
{
    mat4x4 viewProj;
} camera;

layout (set = 0, binding = 1) uniform Model
{
    mat4x4 trf;
} model;

layout (location = 0) out vec3 out_color;

void main()
{
	gl_Position = camera.viewProj * model.trf * vec4(pos, 1);
    out_color = col;
}
