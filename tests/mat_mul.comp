#version 450
#extension GL_ARB_separate_shader_objects : enable

//   gl_NumWorkGroups
//       This variable contains the number of work groups passed to the dispatch function.
//
//   gl_WorkGroupID
//       This is the current work group for this shader invocation. Each of the XYZ components will be on the half-open range [0, gl_NumWorkGroups.XYZ).
//
//   gl_LocalInvocationID
//       This is the current invocation of the shader within the work group. Each of the XYZ components will be on the half-open range [0, gl_WorkGroupSize.XYZ).
//
//   gl_GlobalInvocationID
//       This value uniquely identifies this particular invocation of the compute shader among all invocations of this compute dispatch call. It's a short-hand for the math computation:
//   gl_WorkGroupID * gl_WorkGroupSize + gl_LocalInvocationID;

layout(local_size_x = 64) in;

layout (set = 0, binding = 0) readonly buffer FirstData
{
    mat4 data[];
} first;

layout (set = 0, binding = 1) readonly buffer SecondData
{
    mat4 data[];
} second;

layout (set = 0, binding = 2) writeonly buffer OutData
{
    mat4 data[];
} outData;

void main()
{
    uint id = gl_GlobalInvocationID.x;
    outData.data[id] = first.data[id] * second.data[id];
}
