#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 col;
layout (location = 2) in vec2 uv;

layout (set = 0, binding = 0) uniform ScalerObject 
{
    float scaler;
} scalerObj;

layout (location = 0) out vec3 out_color;
layout (location = 1) out vec2 out_uv;

void main()
{
	gl_Position = vec4(scalerObj.scaler * pos, 1);
    out_color = vec3(uv.x, uv.y, 0);
    out_uv = uv;
}
