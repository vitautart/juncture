#include <algorithm>
#include <cstddef>
#include <iostream>
#include <cassert>
#include <vector>
#include <cstring>

#include "jtlib/vector.hpp"
#include "surface.hpp"
#include "renderer.hpp"
#include "renderer_ext.hpp"
#include "jtlib/codes.hpp"

#define WIDTH 600
#define HEIGHT 400

#define IMAGE_WIDTH 128
#define IMAGE_HEIGHT 128

struct Vertex3D
{
    float pos[3];
    float col[3];
    float uv[2];
};

struct PixelR8G8B8A8
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

jt::vector<PixelR8G8B8A8> generateImage(size_t w, size_t h)
{
    jt::vector<PixelR8G8B8A8> result(w * h);

    for (size_t y = 0; y < h; y++)
        for (size_t x = 0; x < w; x++)
        {
            if ((y / 8) % 2)
                result[x + y * w] = {255, 255, 0, 255};
            else
                result[x + y * w] = {0, 0, 0, 255};
        }

    return result;
}

int main()
{
    std::vector<Vertex3D> vertices  = 
    {
        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.8}, {0, 0}},
        {{0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.0}, {1, 0}},

        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.8, 0.8, 0.8}, {0, 0}},
        {{-0.5f, -0.5f, 0.0f}, {0.8, 0.0, 0.0}, {0, 1}},
    };
    std::vector<Vertex3D> vertices2  = 
    {
        {{0.8f, 0.0f, 0.1f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{0.0f, 0.8f, 0.1f}, {0.0, 0.8, 0.8}, {0, 0}},
        {{0.8f, 0.8f, 0.1f}, {0.0, 0.8, 0.0}, {1, 0}},

        {{0.8f, 0.0f, 0.1f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{0.0f, 0.8f, 0.1f}, {0.8, 0.8, 0.8}, {0, 0}},
        {{0.0f, 0.0f, 0.1f}, {0.8, 0.0, 0.0}, {0, 1}},
    };
    auto [surface, err1] = jt::Surface::make(WIDTH, HEIGHT, "Integration test");
    assert(err1 == jt::Code::OK);


    surface.setKeyCB([sf = &surface](jt::flags::Keys key, jt::flags::InputAction, jt::flags::InputModFlags)
    {
        if (key == jt::flags::Keys::KEY_ESCAPE)
            sf->setShouldClose(true);
    });

    jt::Renderer::Parameters params = 
    {
        .anisotropyEnabled = true,
        .simultaneousFrames = 2,
    };
    auto [renderer, err2] = jt::Renderer::make({WIDTH, HEIGHT}, surface.getNativeSurface(), 
            "Integration test", params);
    assert(err2 == jt::Code::OK);
    auto simFramesCount = renderer.getSimultaneousFrameCount();

    auto [pass, err3] = renderer.createPassOnscreen(true);
    assert(err3 == jt::Code::OK);

    jt::vector<const jt::CommandBuffer*> cbs;
    for (size_t i = 0; i < simFramesCount; i++)
    {
        auto [cb, err4] = renderer.createCommandBuffer();
        assert(err4 == jt::Code::OK);
        cbs.push_back(cb);
    }


    auto [vs, err5] = renderer.createVertexShader("build/simple_vert.spv");
    assert(err5 == jt::Code::OK);

    auto [fs, err6] = renderer.createFragmentShader("build/simple_frag.spv");
    assert(err6 == jt::Code::OK);

    jt::Viewport viewport = { .pos = {0, 0}, .size = {WIDTH, HEIGHT} };

    bool dynamicViewport = false;
    bool testDepth = true;
    bool writeDepth = true;
    bool alphaTransparent = false;

    jt::vector<jt::VertexInputBinding> bindings = 
    {
        {
            .mBinding = 0,
            .mStride = sizeof(Vertex3D),
            .mIsInstanced = false
        }
    };

    jt::vector<jt::VertexInputAttribute> attributes = 
    {
        {
            .mBinding = 0,
            .mLocation = 0,
            .mOffset = offsetof(Vertex3D, pos),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 1,
            .mOffset = offsetof(Vertex3D, col),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 2,
            .mOffset = offsetof(Vertex3D, uv),
            .mFormat = jt::flags::Format::FORMAT_R32G32_SFLOAT
        }
    };

    jt::vector<jt::Descriptor> descriptors = 
    {
        {
            .mBinding = 0,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
        },
        {
            .mBinding = 1,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_FRAGMENT,
        }
    };
    auto [descLayout, err61] = renderer.createDescriptorLayout(descriptors);
    assert(err61 == jt::Code::OK);

    auto [pipeline, err7] = renderer.createPipelineGraphics(pass, vs, fs, {descLayout}, bindings, attributes, viewport, 
            jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            jt::flags::PolygonMode::POLYGON_MODE_FILL,
            jt::flags::CullMode::CULL_MODE_NONE,
            jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
            dynamicViewport, testDepth, writeDepth, alphaTransparent);
    assert(err7 == jt::Code::OK);

    auto [stagingBuffer, err8] = renderer.createBufferUnmapped(vertices.size() * sizeof(Vertex3D), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU);
    assert(err8 == jt::Code::OK);

    auto [vertexBuffer, err9] = renderer.createBufferUnmapped(vertices.size() * sizeof(Vertex3D),
           jt::flags::BUFFER_USAGE_TRANSFER_DST | jt::flags::BUFFER_USAGE_VERTEX, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
    assert(err9 == jt::Code::OK);

    void* stagingBufferData2;
    auto [stagingBuffer2, err82] = renderer.createBufferMapped(vertices2.size() * sizeof(Vertex3D), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU, &stagingBufferData2);
    assert(err82 == jt::Code::OK);

    auto [vertexBuffer2, err92] = renderer.createBufferUnmapped(vertices2.size() * sizeof(Vertex3D),
           jt::flags::BUFFER_USAGE_TRANSFER_DST | jt::flags::BUFFER_USAGE_VERTEX, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
    assert(err92 == jt::Code::OK);

    auto [scalerBuffer, err91] = renderer.createBufferUnmapped(sizeof(float), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);

    auto [stagingBufferForImage, err11] = renderer.createBufferUnmapped(64 * 64 * sizeof(PixelR8G8B8A8), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err11 == jt::Code::OK);
    
    auto [image, err12] = renderer.createImage2D({64, 64}, 
            jt::flags::Format::FORMAT_R8G8B8A8_SRGB, 1, 1, 
            jt::flags::ImageUsageFlagBits::IMAGE_USAGE_SAMPLED | 
            jt::flags::ImageUsageFlagBits::IMAGE_USAGE_TRANSFER_DST, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
    assert(err12 == jt::Code::OK);

    jt::flags::SamplerAddressMode addressMode[3] = 
    {
        jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_REPEAT,
        jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
        jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_REPEAT
    };
    auto [sampler, err13] = renderer.createSampler(jt::flags::SamplerFilter::SAMPLER_FILTER_LINEAR,
            jt::flags::SamplerFilter::SAMPLER_FILTER_LINEAR,
            jt::flags::SamplerMipmapMode::SAMPLER_MIPMAP_MODE_LINEAR,
            addressMode,
            true, 1,
            jt::flags::SamplerBorderColor::SAMPLER_BORDER_COLOR_BLACK);
    assert(err13 == jt::Code::OK);

    auto [dSets, err10] = jt::ResourceBinder(renderer)
        .newSet(descLayout)
        .addBuffer({.mBuffer = scalerBuffer, .mOffset = 0, .mRange = sizeof(float)})
        .addImage({ .mImage = image, .mSampler = sampler})
        .bind();
    auto descriptorSet = dSets.back(); 
    assert(err10 == jt::Code::OK);
    
    float scaler = 1.0f;
    bool upScale = false;
    float clearColor[] = {0.05f, 0.05f, 0.05f, 1.0f};
    
    auto imageData = generateImage(64, 64);

    while (!surface.shouldClose())
    {
        auto [frame, errDraw] = renderer.drawBegin();
        {
            auto cb = cbs[frame];
            renderer.recordCmdBegin(cb);
            {
                {
                    static bool onceLoad = false;
                    if (!onceLoad)
                    {
                        renderer.writeBufferUnmapped(vertices.data(), stagingBuffer, 0, 0, 
                                vertices.size() * sizeof(Vertex3D));
                        /*renderer.fillBuffer(vertices2.data(), stagingBuffer2, 0, 0, 
                                vertices2.size() * sizeof(Vertex3D));*/
                        memcpy(stagingBufferData2, vertices2.data(), vertices2.size() * sizeof(Vertex3D));
                        renderer.writeBufferUnmapped(imageData.data(), stagingBufferForImage, 0, 0,
                                imageData.size() * sizeof(PixelR8G8B8A8));
                        renderer.cmdLoadVertexData(cb, stagingBuffer, vertexBuffer);
                        renderer.cmdLoadVertexData(cb, stagingBuffer2, vertexBuffer2);
                        renderer.cmdLoadTextureData(cb, stagingBufferForImage, image);
                        onceLoad = true;
                    }

                    renderer.writeBufferUnmapped(&scaler, scalerBuffer, 0, 0, sizeof(float));

                    if (scaler > 1.2f)
                        upScale = false;
                    else if (scaler < 0.8f)
                        upScale = true;

                    if (upScale)
                        scaler += 0.005f;
                    else
                        scaler -= 0.005f;
                }
                renderer.cmdPassBegin(cb, pass, clearColor);
                {
                    renderer.cmdBindPipeline(cb, pipeline);
                    renderer.cmdBindDescriptorSets(cb, pipeline, {descriptorSet}, {});

                    renderer.cmdBindVertexBuffer(cb, vertexBuffer, 0);
                    renderer.cmdDraw(cb, vertices.size(), 1);
                    
                    renderer.cmdBindVertexBuffer(cb, vertexBuffer2, 0);
                    renderer.cmdDraw(cb, vertices2.size(), 1);
                }
                renderer.cmdPassEnd(cb, pass);
            }
            renderer.recordCmdEnd(cb);
            renderer.addCommandBuffer(cb);
            
        }
        renderer.drawSubmitLeave();
        surface.pollEvents();
    }

    return 0;
}
