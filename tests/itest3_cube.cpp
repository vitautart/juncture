#include <algorithm>
#include <cstddef>
#include <iostream>
#include <cassert>
#include <vector>

#include "renderer_ext.hpp"
#include "surface.hpp"
#include "renderer.hpp"
#include "jtlib/codes.hpp"
#include "../external/jath/jath.hpp"

#define INDIRECT_DRAW 1

#define WIDTH 600
#define HEIGHT 400

struct Vertex3D
{
    float pos[3];
    float normal[3];
    float col[3];
};

int main()
{
    std::vector<Vertex3D> vertices  = 
    {
        {{ 0.5f, -0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{-0.5f,  0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{ 0.5f,  0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{-0.5f, -0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},

        {{ 0.5f, -0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{-0.5f,  0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{ 0.5f,  0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{-0.5f, -0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},

        {{ 0.5f, -0.5f, -0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{-0.5f, -0.5f,  0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{ 0.5f, -0.5f,  0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{-0.5f, -0.5f, -0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},

        {{ 0.5f,  0.5f, -0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{-0.5f,  0.5f,  0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{ 0.5f,  0.5f,  0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{-0.5f,  0.5f, -0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},

        {{-0.5f,  0.5f, -0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f, -0.5f,  0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f,  0.5f,  0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f, -0.5f, -0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},

        {{ 0.5f,  0.5f, -0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f, -0.5f,  0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f,  0.5f,  0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f, -0.5f, -0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
    };

    std::vector<uint16_t> indices = 
    {
        0, 1, 2, 1, 0, 3,
        4, 5, 6, 5, 4, 7,   // offset 4
        8, 9, 10, 9, 8, 11, // offset 8
        12, 13, 14, 13, 12, 15, // offset 12
        16, 17, 18, 17, 16, 19, // offset 16
        20, 21, 22, 21, 20, 23, // offset 20
    };

    auto [surface, err1] = jt::Surface::make(WIDTH, HEIGHT, "Integration test");
    assert(err1 == jt::Code::OK);


    surface.setKeyCB([sf = &surface](jt::flags::Keys key, jt::flags::InputAction, jt::flags::InputModFlags)
    {
        if (key == jt::flags::Keys::KEY_ESCAPE)
            sf->setShouldClose(true);
    });

    jt::Renderer::Parameters params = 
    {
        .anisotropyEnabled = true,
        .simultaneousFrames = 2,
    };
    auto [renderer, err2] = jt::Renderer::make({WIDTH, HEIGHT}, surface.getNativeSurface(), 
            "Integration test", params);
    assert(err2 == jt::Code::OK);
    auto simFramesCount = renderer.getSimultaneousFrameCount();
    auto availableMultisamples = renderer.getAvailableMultisampling();
    for (const auto sample : availableMultisamples)
    {
        printf("sample: %i\n", sample);
    }

    auto [pass, err3] = renderer.createPassOnscreen(true, availableMultisamples.back());
    assert(err3 == jt::Code::OK);

    jt::vector<const jt::CommandBuffer*> cbs;
    for (size_t i = 0; i < simFramesCount; i++)
    {
        auto [cb, err4] = renderer.createCommandBuffer();
        assert(err4 == jt::Code::OK);
        cbs.push_back(cb);
    }

    auto [vs, err5] = renderer.createVertexShader("build/simple3d_vert.spv");
    assert(err5 == jt::Code::OK);

    auto [fs, err6] = renderer.createFragmentShader("build/simple3d_frag.spv");
    assert(err6 == jt::Code::OK);

    jt::Viewport viewport = { .pos = {0, 0}, .size = {WIDTH, HEIGHT} };

    bool dynamicViewport = false;
    bool testDepth = true;
    bool writeDepth = true;
    bool alphaTransparent = false;

    jt::vector<jt::VertexInputBinding> bindings = 
    {
        {
            .mBinding = 0,
            .mStride = sizeof(Vertex3D),
            .mIsInstanced = false
        }
    };

    jt::vector<jt::VertexInputAttribute> attributes = 
    {
        {
            .mBinding = 0,
            .mLocation = 0,
            .mOffset = offsetof(Vertex3D, pos),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 1,
            .mOffset = offsetof(Vertex3D, normal),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 2,
            .mOffset = offsetof(Vertex3D, col),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        }
    };

    jt::vector<jt::Descriptor> descriptors = 
    {
        {
            .mBinding = 0,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
        },
        {
            .mBinding = 1,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
        }
    };
    auto [descLayout, err61] = renderer.createDescriptorLayout(descriptors);
    assert(err61 == jt::Code::OK);

    auto [pipeline, err7] = renderer.createPipelineGraphics(pass, vs, fs, {descLayout}, bindings, attributes, viewport, 
            jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            jt::flags::PolygonMode::POLYGON_MODE_FILL,
            jt::flags::CullMode::CULL_MODE_NONE,
            jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
            dynamicViewport, testDepth, writeDepth, alphaTransparent);
    assert(err7 == jt::Code::OK);

    // Now after creation of pipeline we can remove shader modules
    renderer.destroyShader(vs);
    renderer.destroyShader(fs);

    auto verticesSizeByte = vertices.size() * sizeof(Vertex3D);
    auto indeciesSizeByte = indices.size() * sizeof(uint16_t);
    auto verteciesAndIndeciesSize = verticesSizeByte + indeciesSizeByte;
    auto [stagingBuffer, err8] = renderer.createBufferMapped(verteciesAndIndeciesSize, 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU);
    assert(err8 == jt::Code::OK);
    auto [vertexBuffer, err9] = renderer.createBufferUnmapped(verteciesAndIndeciesSize,
            jt::flags::BUFFER_USAGE_TRANSFER_DST | 
            jt::flags::BUFFER_USAGE_VERTEX | 
            jt::flags::BUFFER_USAGE_INDEX, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
    assert(err9 == jt::Code::OK);

    jt::IndexedIndirectCommand command = 
    {
        .mIndexCount = (uint32_t)indices.size(),
        .mInstanceCount = 1,
        .mFirstIndex = 0,
        .mVertexOffset = 0, 
        .mFirstInstance = 1
    };
    auto [commandDescBuffer, err93] = renderer.createBufferMapped(sizeof(jt::IndexedIndirectCommand), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_INDIRECT,
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err93 == jt::Code::OK);

    auto [projBuffer, err91] = renderer.createBufferMapped(sizeof(jth::mat4), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err91 == jt::Code::OK);

    auto [rotBuffer, err92] = renderer.createBufferMapped(sizeof(jth::mat4), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err92 == jt::Code::OK);
    
    /*jt::Renderer::DescriptorSetResources descriptorResources = {
            jt::variant<jt::Renderer::BufferRegion, jt::Renderer::ImageData>(jt::Renderer::BufferRegion {
                .mBuffer = projBuffer,
                .mOffset = 0,
                .mRange = sizeof(jth::mat4)}),
            jt::variant<jt::Renderer::BufferRegion, jt::Renderer::ImageData>(jt::Renderer::BufferRegion {
                .mBuffer = rotBuffer,
                .mOffset = 0,
                .mRange = sizeof(jth::mat4)})
    };
    auto [descriptorSet, err10] = renderer.createDescriptorSet(descLayout, descriptorResources);*/
    auto [descriptorSet, err10] = jt::ResourceBinder(renderer)
        .newSet(descLayout)
        .addBuffer({.mBuffer = projBuffer, .mOffset = 0, .mRange = sizeof(jth::mat4)})
        .addBuffer({.mBuffer = rotBuffer, .mOffset = 0, .mRange = sizeof(jth::mat4)})
        .bind();

    assert(err10 == jt::Code::OK);
    

    float clearColor[] = {0.05f, 0.05f, 0.05f, 1.0f};
    float angle = 0;
    while (!surface.shouldClose())
    {
        auto [frame, errDraw] = renderer.drawBegin();
        {
            auto cb = cbs[frame];
            renderer.recordCmdBegin(cb);
            {
                {
                    static bool onceLoad = false;
                    if (!onceLoad)
                    {
                        renderer.writeBufferMapped(vertices.data(), stagingBuffer, 0, 0, verticesSizeByte);
                        renderer.writeBufferMapped(indices.data(), stagingBuffer, 0, verticesSizeByte, indeciesSizeByte);
                        renderer.writeBufferMapped({command}, commandDescBuffer, 0, 0, 1);

                        renderer.cmdLoadVertexData(cb, stagingBuffer, vertexBuffer, 0, 0, verticesSizeByte);
                        renderer.cmdLoadIndexData(cb, stagingBuffer, vertexBuffer, verticesSizeByte, verticesSizeByte, indeciesSizeByte);
                        onceLoad = true;
                    }
                    
                    auto trs = jth::translate<float>({0, 0, -1.8});
                    auto rot = jth::rot<float>(jth::norm(jth::vec3{1, 1, 1}), angle);
                    auto model = trs * rot;

                    auto proj = jth::persp_proj_rh<float>(3.14f/2, float(WIDTH), float(HEIGHT), 0.01f, 10.0f);
                    static float x = 0;
                    static float y = 0;
                    static float z = 0;
                    jth::vec3 pos = {x, y,  z};
                    //x += 0.01f; // camera -> right
                    //y += 0.01f; // camera -> up
                    //z += 0.01f; // camera -> on us
                    jth::vec3 target = {0 + x, 0 + y, -1.8f + z};
                    auto view = jth::look_at_rh(pos, target, {0, 1, 0});
                    auto viewProj = proj * view;

                    angle += 3.14f/256;
                    if (angle > 3.14 * 2) angle = 0;
 
                    renderer.writeBufferMapped(&viewProj, projBuffer, 0, 0, sizeof(jth::mat4));
                    renderer.writeBufferMapped(&model, rotBuffer, 0, 0, sizeof(jth::mat4));
                }
                renderer.cmdPassBegin(cb, pass, clearColor);
                {
                    renderer.cmdBindPipeline(cb, pipeline);
                    renderer.cmdBindDescriptorSets(cb, pipeline, descriptorSet, {});

                    renderer.cmdBindVertexBuffer(cb, vertexBuffer, 0);
                    renderer.cmdBindIndexBuffer(cb, vertexBuffer, verticesSizeByte, jt::flags::IndexType::TYPE_UINT16);
#if INDIRECT_DRAW == 1
                    renderer.cmdDrawIndexedIndirect(cb, commandDescBuffer, 0, 1, sizeof(jt::IndexedIndirectCommand));
#else
                    renderer.cmdDrawIndexed(cb, indices.size(), 1);
#endif

                }
                renderer.cmdPassEnd(cb, pass);
            }
            renderer.recordCmdEnd(cb);
            renderer.addCommandBuffer(cb);
            
        }
        renderer.drawSubmitLeave();
        surface.pollEvents();
    }

    return 0;
}
