#include <algorithm>
#include <cstddef>
#include <iostream>
#include <cassert>
#include <vector>
#include <cstring>

#include "flags.hpp"
#include "jtlib/vector.hpp"
#include "surface.hpp"
#include "renderer.hpp"
#include "renderer_ext.hpp"
#include "jtlib/codes.hpp"
#include "../external/jath/jath.hpp"

#define WIDTH 600
#define HEIGHT 400

#define IMAGE_WIDTH 128
#define IMAGE_HEIGHT 128

struct Vertex3D
{
    float pos[3];
    float normal[3];
    float col[3];
};

struct Vertex3DUV
{
    float pos[3];
    float col[3];
    float uv[2];
};

struct PixelR8G8B8A8
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

jt::vector<PixelR8G8B8A8> generateImage(size_t w, size_t h)
{
    jt::vector<PixelR8G8B8A8> result(w * h);

    for (size_t y = 0; y < h; y++)
        for (size_t x = 0; x < w; x++)
        {
            if ((y / 8) % 2)
                result[x + y * w] = {255, 255, 0, 255};
            else
                result[x + y * w] = {0, 0, 0, 255};
        }

    return result;
}

struct OffscreenData
{
    std::vector<Vertex3D> vertices  = 
    {
        {{ 0.5f, -0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{-0.5f,  0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{ 0.5f,  0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},
        {{-0.5f, -0.5f, 0.5f},  {0, 0,  1}, {1.0, 0.0, 0.0}},

        {{ 0.5f, -0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{-0.5f,  0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{ 0.5f,  0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},
        {{-0.5f, -0.5f, -0.5f},  {0, 0, -1}, {0.0, 1.0, 0.0}},

        {{ 0.5f, -0.5f, -0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{-0.5f, -0.5f,  0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{ 0.5f, -0.5f,  0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},
        {{-0.5f, -0.5f, -0.5f},  {0, -1, 0}, {0.0, 0.0, 1.0}},

        {{ 0.5f,  0.5f, -0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{-0.5f,  0.5f,  0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{ 0.5f,  0.5f,  0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},
        {{-0.5f,  0.5f, -0.5f},  {0,  1, 0}, {0.6, 0.3, 0.0}},

        {{-0.5f,  0.5f, -0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f, -0.5f,  0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f,  0.5f,  0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},
        {{-0.5f, -0.5f, -0.5f},  {-1,  0, 0}, {0.0, 0.6, 0.3}},

        {{ 0.5f,  0.5f, -0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f, -0.5f,  0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f,  0.5f,  0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
        {{ 0.5f, -0.5f, -0.5f},  { 1,  0, 0}, {6.0, 0.0, 0.3}},
    };

    std::vector<uint16_t> indices = 
    {
        0, 1, 2, 1, 0, 3,
        4, 5, 6, 5, 4, 7,   // offset 4
        8, 9, 10, 9, 8, 11, // offset 8
        12, 13, 14, 13, 12, 15, // offset 12
        16, 17, 18, 17, 16, 19, // offset 16
        20, 21, 22, 21, 20, 23, // offset 20
    };

    jt::PassOffscreen pass;
    const jt::Pipeline* pipeline;
    const jt::Buffer* stagingBuffer;
    const jt::Buffer* vertexBuffer;
    const jt::Buffer* projBuffer;
    const jt::Buffer* rotBuffer;
    jt::ImageTarget<jt::TargetType::COLOR> colorImage;
    jt::ImageTarget<jt::TargetType::DEPTH> depthImage;
    jt::ImageTarget<jt::TargetType::RESOLVE> presentImage;
    jt::vector<const jt::DescriptorSet*> descriptorSets;

    float angle = 0;
    size_t verticesSizeByte;
    size_t indeciesSizeByte;

    auto create(jt::Renderer& renderer) -> void
    {
        auto sampling = jt::flags::Multisampling::MULTISAMPLING_4;
        auto colorBlueprint = jt::TargetBlueprint 
        {
            .mUsage = 0,
            .mFormat = jt::flags::Format::FORMAT_R8G8B8A8_SRGB,
        };

        auto presentBlueprint = jt::TargetBlueprint
        {
            .mUsage = jt::flags::ImageUsageFlagBits::IMAGE_USAGE_SAMPLED | 
                jt::flags::ImageUsageFlagBits::IMAGE_USAGE_TRANSFER_DST,
            .mFormat = jt::flags::Format::FORMAT_R8G8B8A8_SRGB,
        };


        jt::Size2D imageSize = {IMAGE_WIDTH, IMAGE_HEIGHT};
        auto res1 = renderer.createPassOffscreen(imageSize, 
                colorBlueprint, {0}, {{presentBlueprint, sampling}});
        assert(res1.error == jt::Code::OK);
        pass = res1.data;

        auto res11 = renderer.createImageColorTarget(imageSize, colorBlueprint, sampling);
        assert(res11.error == jt::Code::OK);
        colorImage = res11.data;

        auto res12 = renderer.createImageDepthTarget(imageSize, 0, sampling);
        assert(res12.error == jt::Code::OK);
        depthImage = res12.data;

        auto res13 = renderer.createImageResolveTarget(imageSize, presentBlueprint);
        assert(res13.error == jt::Code::OK);
        presentImage = res13.data; 
        
        auto [vs, err5] = renderer.createVertexShader("build/simple3d_vert.spv");
        assert(err5 == jt::Code::OK);

        auto [fs, err6] = renderer.createFragmentShader("build/simple3d_frag.spv");
        assert(err6 == jt::Code::OK);

        jt::Viewport viewport = { .pos = {0, 0}, .size = imageSize };

        bool dynamicViewport = false;
        bool testDepth = true;
        bool writeDepth = true;
        bool alphaTransparent = false;

        jt::vector<jt::VertexInputBinding> bindings = 
        {
            {
                .mBinding = 0,
                .mStride = sizeof(Vertex3D),
                .mIsInstanced = false
            }
        };

        jt::vector<jt::VertexInputAttribute> attributes = 
        {
            {
                .mBinding = 0,
                .mLocation = 0,
                .mOffset = offsetof(Vertex3D, pos),
                .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
            },
            {
                .mBinding = 0,
                .mLocation = 1,
                .mOffset = offsetof(Vertex3D, normal),
                .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
            },
            {
                .mBinding = 0,
                .mLocation = 2,
                .mOffset = offsetof(Vertex3D, col),
                .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
            }
        };

        jt::vector<jt::Descriptor> descriptors = 
        {
            {
                .mBinding = 0,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
            },
            {
                .mBinding = 1,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
            }
        };
        auto [descLayout, err61] = renderer.createDescriptorLayout(descriptors);
        assert(err61 == jt::Code::OK);

        auto res2 = renderer.createPipelineGraphics(pass, vs, fs, {descLayout}, bindings, attributes, viewport, 
                jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                jt::flags::PolygonMode::POLYGON_MODE_FILL,
                jt::flags::CullMode::CULL_MODE_NONE,
                jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
                dynamicViewport, testDepth, writeDepth, alphaTransparent);
        assert(res2.error == jt::Code::OK);
        pipeline = res2.data;

        // Now after creation of pipeline we can remove shader modules
        renderer.destroyShader(vs);
        renderer.destroyShader(fs);

        verticesSizeByte = vertices.size() * sizeof(Vertex3D);
        indeciesSizeByte = indices.size() * sizeof(uint16_t);
        auto verteciesAndIndeciesSize = verticesSizeByte + indeciesSizeByte;
        auto res3 = renderer.createBufferMapped(verteciesAndIndeciesSize, 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU);
        assert(res3.error == jt::Code::OK);
        stagingBuffer = res3.data;

        auto res4 = renderer.createBufferUnmapped(verteciesAndIndeciesSize,
                jt::flags::BUFFER_USAGE_TRANSFER_DST | 
                jt::flags::BUFFER_USAGE_VERTEX | 
                jt::flags::BUFFER_USAGE_INDEX, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
        assert(res4.error == jt::Code::OK);
        vertexBuffer = res4.data;

        auto res5 = renderer.createBufferMapped(sizeof(jth::mat4), 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
        assert(res5.error == jt::Code::OK);
        projBuffer = res5.data;

        auto res6 = renderer.createBufferMapped(sizeof(jth::mat4), 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
        assert(res6.error == jt::Code::OK);
        rotBuffer = res6.data;
        
        auto res7 = jt::ResourceBinder(renderer)
            .newSet(descLayout)
            .addBuffer({.mBuffer = projBuffer, .mOffset = 0, .mRange = sizeof(jth::mat4)})
            .addBuffer({.mBuffer = rotBuffer, .mOffset = 0, .mRange = sizeof(jth::mat4)})
            .bind();
        assert(res7.error == jt::Code::OK);
        descriptorSets = res7.data;
    };

    auto update(jt::Renderer& renderer)
    {
        auto trs = jth::translate<float>({0, 0, -1.8});
        auto rot = jth::rot<float>(jth::norm(jth::vec3{1, 1, 1}), angle);
        auto model = trs * rot;

        auto proj = jth::persp_proj_rh<float>(3.14f/2, float(IMAGE_WIDTH), float(IMAGE_HEIGHT), 0.01f, 10.0f);
        static float x = 0;
        static float y = 0;
        static float z = 0;
        jth::vec3 pos = {x, y,  z};
        //x += 0.01f; // camera -> right
        //y += 0.01f; // camera -> up
        //z += 0.01f; // camera -> on us
        jth::vec3 target = {0 + x, 0 + y, -1.8f + z};
        auto view = jth::look_at_rh(pos, target, {0, 1, 0});
        auto viewProj = proj * view;

        angle += 3.14f/256;
        if (angle > 3.14 * 2) angle = 0;

        renderer.writeBufferMapped(&viewProj, projBuffer, 0, 0, sizeof(jth::mat4));
        renderer.writeBufferMapped(&model, rotBuffer, 0, 0, sizeof(jth::mat4));
    }
};

struct OnscreenData
{
    std::vector<Vertex3DUV> vertices  = 
    {
        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.8}, {0, 0}},
        {{0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.0}, {1, 0}},

        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.8, 0.8, 0.8}, {0, 0}},
        {{-0.5f, -0.5f, 0.0f}, {0.8, 0.0, 0.0}, {0, 1}},
    };
    jt::PassOnscreen pass;
    const jt::Pipeline* pipeline;
    const jt::Buffer* stagingBuffer;
    const jt::Buffer* vertexBuffer;
    const jt::Buffer* scalerBuffer;
    const jt::Buffer* stagingBufferForImage;
    const jt::Sampler* sampler;
    const jt::DescriptorSet* descriptorSet;
    float scaler = 1.0f;
    bool upScale = false;

    auto create(jt::Renderer& renderer, const jt::Image* image) -> void
    {
        auto res = renderer.createPassOnscreen(true);
        assert(res.error == jt::Code::OK);
        pass = res.data;

        auto [vs, err5] = renderer.createVertexShader("build/simple_vert.spv");
        assert(err5 == jt::Code::OK);

        auto [fs, err6] = renderer.createFragmentShader("build/simple_frag.spv");
        assert(err6 == jt::Code::OK);

        jt::Viewport viewport = { .pos = {0, 0}, .size = {WIDTH, HEIGHT} };

        bool dynamicViewport = false;
        bool testDepth = true;
        bool writeDepth = true;
        bool alphaTransparent = false;

        jt::vector<jt::VertexInputBinding> bindings = 
        {
            {
                .mBinding = 0,
                .mStride = sizeof(Vertex3DUV),
                .mIsInstanced = false
            }
        };

        jt::vector<jt::VertexInputAttribute> attributes = 
        {
            {
                .mBinding = 0,
                .mLocation = 0,
                .mOffset = offsetof(Vertex3DUV, pos),
                .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
            },
            {
                .mBinding = 0,
                .mLocation = 1,
                .mOffset = offsetof(Vertex3DUV, col),
                .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
            },
            {
                .mBinding = 0,
                .mLocation = 2,
                .mOffset = offsetof(Vertex3DUV, uv),
                .mFormat = jt::flags::Format::FORMAT_R32G32_SFLOAT
            }
        };

        jt::vector<jt::Descriptor> descriptors = 
        {
            {
                .mBinding = 0,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
            },
            {
                .mBinding = 1,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_FRAGMENT,
            }
        };
        auto [descLayout, err61] = renderer.createDescriptorLayout(descriptors);
        assert(err61 == jt::Code::OK);

        auto res2 = renderer.createPipelineGraphics(pass, vs, fs, {descLayout}, bindings, attributes, viewport, 
                jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
                jt::flags::PolygonMode::POLYGON_MODE_FILL,
                jt::flags::CullMode::CULL_MODE_NONE,
                jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
                dynamicViewport, testDepth, writeDepth, alphaTransparent);
        assert(res2.error == jt::Code::OK);
        pipeline = res2.data;

        auto res3 = renderer.createBufferUnmapped(vertices.size() * sizeof(Vertex3DUV), 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU_TO_GPU);
        assert(res3.error == jt::Code::OK);
        stagingBuffer = res3.data;

        auto res4 = renderer.createBufferUnmapped(vertices.size() * sizeof(Vertex3DUV),
               jt::flags::BUFFER_USAGE_TRANSFER_DST | jt::flags::BUFFER_USAGE_VERTEX, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
        assert(res4.error == jt::Code::OK);
        vertexBuffer = res4.data;

        auto res5 = renderer.createBufferUnmapped(sizeof(float), 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
        assert(res5.error == jt::Code::OK);
        scalerBuffer = res5.data;

        auto res6 = renderer.createBufferUnmapped(64 * 64 * sizeof(PixelR8G8B8A8), 
                jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
        assert(res6.error == jt::Code::OK);
        stagingBufferForImage = res6.data;
        
        /*auto res7 = renderer.createImage2D({64, 64}, 
                jt::flags::Format::FORMAT_R8G8B8A8_SRGB, 1, 1, 
                jt::flags::ImageUsageFlagBits::IMAGE_USAGE_SAMPLED | 
                jt::flags::ImageUsageFlagBits::IMAGE_USAGE_TRANSFER_DST, 
                jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
        assert(res7.error == jt::Code::OK);
        image = res7.data;*/

        jt::flags::SamplerAddressMode addressMode[3] = 
        {
            jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_REPEAT,
            jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT,
            jt::flags::SamplerAddressMode::SAMPLER_ADDRESS_MODE_REPEAT
        };
        auto res8 = renderer.createSampler(jt::flags::SamplerFilter::SAMPLER_FILTER_NEAREST,
                jt::flags::SamplerFilter::SAMPLER_FILTER_NEAREST,
                jt::flags::SamplerMipmapMode::SAMPLER_MIPMAP_MODE_LINEAR,
                addressMode,
                true, 1,
                jt::flags::SamplerBorderColor::SAMPLER_BORDER_COLOR_BLACK);
        assert(res8.error == jt::Code::OK);
        sampler = res8.data;

        auto [dSets, err10] = jt::ResourceBinder(renderer)
            .newSet(descLayout)
            .addBuffer({.mBuffer = scalerBuffer, .mOffset = 0, .mRange = sizeof(float)})
            .addImage({ .mImage = image, .mSampler = sampler})
            .bind();
        descriptorSet = dSets.back(); 
        assert(err10 == jt::Code::OK);
    };

    auto update(jt::Renderer& renderer) -> void
    {
        renderer.writeBufferUnmapped(&scaler, scalerBuffer, 0, 0, sizeof(float));

        if (scaler > 1.2f)
            upScale = false;
        else if (scaler < 0.8f)
            upScale = true;

        if (upScale)
            scaler += 0.005f;
        else
            scaler -= 0.005f;
    };
};

int main()
{
    auto [surface, err1] = jt::Surface::make(WIDTH, HEIGHT, "Integration test");
    assert(err1 == jt::Code::OK);

    surface.setKeyCB([sf = &surface](jt::flags::Keys key, jt::flags::InputAction, jt::flags::InputModFlags)
    {
        if (key == jt::flags::Keys::KEY_ESCAPE)
            sf->setShouldClose(true);
    });

    jt::Renderer::Parameters params = 
    {
        .anisotropyEnabled = true,
        .simultaneousFrames = 2,
    };
    auto [renderer, err2] = jt::Renderer::make({WIDTH, HEIGHT}, surface.getNativeSurface(), 
            "Integration test", params);
    assert(err2 == jt::Code::OK);

    auto simFramesCount = renderer.getSimultaneousFrameCount();
    
    jt::vector<const jt::CommandBuffer*> cbs;
    for (size_t i = 0; i < simFramesCount; i++)
    {
        auto [cb, err4] = renderer.createCommandBuffer();
        assert(err4 == jt::Code::OK);
        cbs.push_back(cb);
    }

    OffscreenData dataOFS = {};
    dataOFS.create(renderer);

    OnscreenData dataONS = {};
    dataONS.create(renderer, dataOFS.presentImage);   

    float clearColor[] = {0.05f, 0.05f, 0.05f, 1.0f};
    float clearColor2[] = {0.5f, 0.05f, 0.05f, 1.0f};
    
    auto imageData = generateImage(64, 64);

    while (!surface.shouldClose())
    {
        auto [frame, errDraw] = renderer.drawBegin();
        {
            auto cb = cbs[frame];
            renderer.recordCmdBegin(cb);
            {
                {
                    static bool onceLoad = false;
                    if (!onceLoad)
                    {
                        renderer.writeBufferUnmapped(dataONS.vertices.data(), dataONS.stagingBuffer, 0, 0, 
                                dataONS.vertices.size() * sizeof(Vertex3DUV));
                        renderer.cmdLoadVertexData(cb, dataONS.stagingBuffer, dataONS.vertexBuffer);
                        renderer.writeBufferMapped(dataOFS.vertices.data(), dataOFS.stagingBuffer, 0, 0, dataOFS.verticesSizeByte);
                        renderer.writeBufferMapped(dataOFS.indices.data(), dataOFS.stagingBuffer, 0, dataOFS.verticesSizeByte, dataOFS.indeciesSizeByte);
                        renderer.cmdLoadVertexData(cb, dataOFS.stagingBuffer, dataOFS.vertexBuffer, 0, 0, dataOFS.verticesSizeByte);
                        renderer.cmdLoadIndexData(cb, dataOFS.stagingBuffer, dataOFS.vertexBuffer, dataOFS.verticesSizeByte, dataOFS.verticesSizeByte, dataOFS.indeciesSizeByte);

                        onceLoad = true;
                    }

                    dataONS.update(renderer);
                    dataOFS.update(renderer);

                }

                renderer.cmdPassBegin(cb, dataOFS.pass, clearColor, dataOFS.colorImage, dataOFS.depthImage, dataOFS.presentImage);
                {
                    renderer.cmdBindPipeline(cb, dataOFS.pipeline);
                    renderer.cmdBindDescriptorSets(cb, dataOFS.pipeline, dataOFS.descriptorSets, {});

                    renderer.cmdBindVertexBuffer(cb, dataOFS.vertexBuffer, 0);
                    renderer.cmdBindIndexBuffer(cb, dataOFS.vertexBuffer, dataOFS.verticesSizeByte, jt::flags::IndexType::TYPE_UINT16);

                    renderer.cmdDrawIndexed(cb, dataOFS.indices.size(), 1);
                }
                renderer.cmdPassEnd(cb, dataOFS.pass);
                jt::ImageDataBarrier barrier = 
                {
                    .mImage = dataOFS.presentImage,
                    .mSrcAccess = jt::flags::MemoryAccessFlagBits::MEMORY_ACCESS_MEMORY_WRITE_BIT,
                    .mDstAccess = jt::flags::MemoryAccessFlagBits::MEMORY_ACCESS_MEMORY_READ_BIT,
                    .mOldLayout = jt::flags::ImageLayout::IMAGE_LAYOUT_PRESENT_SRC_KHR,
                    .mNewLayout = jt::flags::ImageLayout::IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
                };
                jt::BarrierBuilder<0,1>(renderer)
                    .addImage(barrier)
                    .setSrcStages(jt::flags::PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
                    .setDstStages(jt::flags::PIPELINE_STAGE_FRAGMENT_SHADER_BIT)
                    .cmd(cb);
                renderer.cmdPassBegin(cb, dataONS.pass, clearColor2);
                {

                    renderer.cmdBindPipeline(cb, dataONS.pipeline);
                    renderer.cmdBindDescriptorSets(cb, dataONS.pipeline, {dataONS.descriptorSet}, {});



                    renderer.cmdBindVertexBuffer(cb, dataONS.vertexBuffer, 0);
                    renderer.cmdDraw(cb, dataONS.vertices.size(), 1);
                }
                renderer.cmdPassEnd(cb, dataONS.pass);
            }
            renderer.recordCmdEnd(cb);
            renderer.addCommandBuffer(cb);
            
        }
        renderer.drawSubmitLeave();
        surface.pollEvents();
    }

    return 0;
}
