#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <vector>
#include <ctime>


#include "flags.hpp"
#include "jtlib/vector.hpp"
#include "surface.hpp"
#include "renderer.hpp"
#include "renderer_ext.hpp"
#include "jtlib/codes.hpp"
#include "../external/jath/jath.hpp"

#define WIDTH 600
#define HEIGHT 400

#define MATRIXCOUNT 1024

struct Vertex3D
{
    float pos[3];
    float col[3];
    float uv[2];
};

auto checkMatricesGPUCPU(const std::vector<jth::mat4>& cpuMatrices, const jth::mat4* gpuMatrices)
{
    for (size_t i = 0; i < cpuMatrices.size(); i++)
        if (!jth::equal(cpuMatrices[i], gpuMatrices[i], 0.01f))
        {
            auto cpu = cpuMatrices[i];
            auto gpu = gpuMatrices[i];
            printf ("Matrix %i failed.\n", (int)i);
            printf("%f %f %f %f\n", cpu[0][0], cpu[0][1], cpu[0][2], cpu[0][3]);
            printf("%f %f %f %f\n", cpu[1][0], cpu[1][1], cpu[1][2], cpu[1][3]);
            printf("%f %f %f %f\n", cpu[2][0], cpu[2][1], cpu[2][2], cpu[2][3]);
            printf("%f %f %f %f\n", cpu[3][0], cpu[3][1], cpu[3][2], cpu[3][3]);

            printf ("---------------\n");
            printf("%f %f %f %f\n", gpu[0][0], gpu[0][1], gpu[0][2], gpu[0][3]);
            printf("%f %f %f %f\n", gpu[1][0], gpu[1][1], gpu[1][2], gpu[1][3]);
            printf("%f %f %f %f\n", gpu[2][0], gpu[2][1], gpu[2][2], gpu[2][3]);
            printf("%f %f %f %f\n", gpu[3][0], gpu[3][1], gpu[3][2], gpu[3][3]);
            assert(false);
        }
    printf("MATRIX MULTIPLICATIONS ARE EQUAL ON CPU AND GPU WITH DELTA: %f\n", 0.01f);
}

auto main() -> int
{
    std::srand(std::time(nullptr));
    std::vector<jth::mat4> first(MATRIXCOUNT);
    std::vector<jth::mat4> second(MATRIXCOUNT);
    std::vector<jth::mat4> outHost(MATRIXCOUNT);
    const auto matrixSizeByte = MATRIXCOUNT * sizeof(jth::mat4);   
    for (size_t i = 0; i < first.size(); i++)
        for (size_t x = 0; x < jth::mat4::rank()->x; x++) for (size_t y = 0; y < jth::mat4::rank()->y; y++)
            first[i][x][y] = 100 * ((float)std::rand() / (float)RAND_MAX);

    for (size_t i = 0; i < second.size(); i++)
        for (size_t x = 0; x < jth::mat4::rank()->x; x++) for (size_t y = 0; y < jth::mat4::rank()->y; y++)
            second[i][x][y] = 100 * ((float)std::rand() / (float)RAND_MAX);

    for (size_t i = 0; i < MATRIXCOUNT; i++)
        outHost[i] = first[i] * second[i];


    std::vector<Vertex3D> vertices  = 
    {
        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.8}, {0, 0}},
        {{0.5f, 0.5f, 0.0f}, {0.0, 0.8, 0.0}, {1, 0}},

        {{0.5f, -0.5f, 0.0f}, {0.0, 0.0, 0.8}, {1, 1}},
        {{-0.5f, 0.5f, 0.0f}, {0.8, 0.8, 0.8}, {0, 0}},
        {{-0.5f, -0.5f, 0.0f}, {0.8, 0.0, 0.0}, {0, 1}},
    };

    auto [surface, err1] = jt::Surface::make(WIDTH, HEIGHT, "Integration test");
    assert(err1 == jt::Code::OK);


    surface.setKeyCB([sf = &surface](jt::flags::Keys key, jt::flags::InputAction, jt::flags::InputModFlags)
    {
        if (key == jt::flags::Keys::KEY_ESCAPE)
            sf->setShouldClose(true);
    });

    jt::Renderer::Parameters params = 
    {
        .anisotropyEnabled = true,
        .vertexStoreAtomicallyBuffer = true,
        .enableComputeDrawMix = true,
        .enableComputeDedicated = true,
        .simultaneousFrames = 2,
    };
    auto [renderer, err2] = jt::Renderer::make({WIDTH, HEIGHT}, surface.getNativeSurface(), 
            "Integration test", params);
    assert(err2 == jt::Code::OK);
    auto simFramesCount = renderer.getSimultaneousFrameCount();

    // Create compute data 
    const jt::Pipeline* compute_pipeline;
    const jt::Shader* compute_shader;
    const jt::CommandBuffer* compute_cb;
    const jt::DescriptorLayout* compute_descLayout;
    const jt::DescriptorSet* compute_desc_set;
    {
        auto [cb, err41] = renderer.createCommandBuffer();
        assert(err41 == jt::Code::OK);
        compute_cb = cb;

        auto [s, err42] = renderer.createComputeShader("build/mat_mul_comp.spv");
        assert(err42 == jt::Code::OK);
        compute_shader = s;

        jt::vector<jt::Descriptor> descriptors = 
        {
            {
                .mBinding = 0,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_COMPUTE,
            },
            {
                .mBinding = 1,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_COMPUTE,
            },
            {
                .mBinding = 2,
                .mCount = 1,
                .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_COMPUTE,
            }
        };
        compute_descLayout = renderer.createDescriptorLayout(descriptors).data;

        auto [p, err44] = renderer.createPipelineCompute(compute_shader, {compute_descLayout});
        assert(err44 == jt::Code::OK);
        compute_pipeline = p;
    }
    auto firstBufferCPU = renderer.createBufferMapped(matrixSizeByte, 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC,
        jt::flags::MemoryUsageType::MEMORY_TYPE_CPU).data;
    auto firstBufferGPU = renderer.createBufferUnmapped(matrixSizeByte, 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_DST | 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_STORAGE,
        jt::flags::MemoryUsageType::MEMORY_TYPE_GPU).data;
    auto secondBufferCPU = renderer.createBufferMapped(matrixSizeByte, 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC,
        jt::flags::MemoryUsageType::MEMORY_TYPE_CPU).data;
    auto secondBufferGPU = renderer.createBufferUnmapped(matrixSizeByte, 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_DST | 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_STORAGE,
        jt::flags::MemoryUsageType::MEMORY_TYPE_GPU).data;
    void* mappedPtrCompute;
    auto [outBuffer, err45] = renderer.createBufferMapped(matrixSizeByte, 
        jt::flags::BufferUsageFlagBits::BUFFER_USAGE_STORAGE,
        jt::flags::MemoryUsageType::MEMORY_TYPE_GPU_TO_CPU, &mappedPtrCompute);
    assert(err45 == jt::Code::OK);
    auto outDataPtr = (jth::mat4*)mappedPtrCompute;


    auto [pass, err3] = renderer.createPassOnscreen(true);
    assert(err3 == jt::Code::OK);

    jt::vector<const jt::CommandBuffer*> cbs;
    for (size_t i = 0; i < simFramesCount; i++)
    {
        auto [cb, err4] = renderer.createCommandBuffer();
        assert(err4 == jt::Code::OK);
        cbs.push_back(cb);
    }

    auto [vs, err5] = renderer.createVertexShader("build/simple2d_vert.spv");
    assert(err5 == jt::Code::OK);

    auto [fs, err6] = renderer.createFragmentShader("build/simple2d_frag.spv");
    assert(err6 == jt::Code::OK);

    jt::Viewport viewport = { .pos = {0, 0}, .size = {WIDTH, HEIGHT} };

    bool dynamicViewport = false;
    bool testDepth = true;
    bool writeDepth = true;
    bool alphaTransparent = false;

    jt::vector<jt::VertexInputBinding> bindings = 
    {
        {
            .mBinding = 0,
            .mStride = sizeof(Vertex3D),
            .mIsInstanced = false
        }
    };

    jt::vector<jt::VertexInputAttribute> attributes = 
    {
        {
            .mBinding = 0,
            .mLocation = 0,
            .mOffset = offsetof(Vertex3D, pos),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 1,
            .mOffset = offsetof(Vertex3D, col),
            .mFormat = jt::flags::Format::FORMAT_R32G32B32_SFLOAT
        },
        {
            .mBinding = 0,
            .mLocation = 2,
            .mOffset = offsetof(Vertex3D, uv),
            .mFormat = jt::flags::Format::FORMAT_R32G32_SFLOAT
        }
    };

    jt::vector<jt::Descriptor> descriptors = 
    {
        {
            .mBinding = 0,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
        },
        {
            .mBinding = 1,
            .mCount = 1,
            .mTupe = jt::flags::DescriptorType::DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .mUsingInStages = jt::flags::ShaderStageBits::SHADER_STAGE_VERTEX,
        }
    };
    auto [descLayout, err61] = renderer.createDescriptorLayout(descriptors);
    assert(err61 == jt::Code::OK);

    auto [pipeline, err7] = renderer.createPipelineGraphics(pass, vs, fs, {descLayout}, bindings, attributes, viewport, 
            jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            jt::flags::PolygonMode::POLYGON_MODE_FILL,
            jt::flags::CullMode::CULL_MODE_NONE,
            jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
            dynamicViewport, testDepth, writeDepth, alphaTransparent);
    assert(err7 == jt::Code::OK);


    auto [stagingBuffer, err8] = renderer.createBufferMapped(vertices.size() * sizeof(Vertex3D), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_TRANSFER_SRC, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err8 == jt::Code::OK);

    auto [vertexBuffer, err9] = renderer.createBufferUnmapped(vertices.size() * sizeof(Vertex3D),
           jt::flags::BUFFER_USAGE_TRANSFER_DST | jt::flags::BUFFER_USAGE_VERTEX, 
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU);
    assert(err9 == jt::Code::OK);

    auto [scalerBuffer, err91] = renderer.createBufferMapped(sizeof(float), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_UNIFORM,
            jt::flags::MemoryUsageType::MEMORY_TYPE_CPU);
    assert(err91 == jt::Code::OK);

    void* mappedPtr;
    auto [readBuffer, err92] = renderer.createBufferMapped(10 * 4 * sizeof(float), 
            jt::flags::BufferUsageFlagBits::BUFFER_USAGE_STORAGE,
            jt::flags::MemoryUsageType::MEMORY_TYPE_GPU_TO_CPU, &mappedPtr);
    assert(err92 == jt::Code::OK);
    
    auto [descriptorSets, err10] = jt::ResourceBinder(renderer)
        .newSet(descLayout)
        .addBuffer({.mBuffer = scalerBuffer, .mOffset = 0, .mRange = sizeof(float)})
        .addBuffer({.mBuffer = readBuffer, .mOffset = 0, .mRange = 10 * 4 * sizeof(float)})
        .newSet(compute_descLayout)
        .addBuffer({.mBuffer = firstBufferGPU, .mOffset = 0, .mRange = matrixSizeByte})
        .addBuffer({.mBuffer = secondBufferGPU, .mOffset = 0, .mRange = matrixSizeByte})
        .addBuffer({.mBuffer = outBuffer, .mOffset = 0, .mRange = matrixSizeByte})
        .bind();
    assert(err10 == jt::Code::OK);
    
    compute_desc_set = descriptorSets[1];
    const jt::DescriptorSet* graphics_desc_set = descriptorSets[0];
    
    float scaler = 1.0f;
    bool upScale = false;
    float clearColor[] = {0.05f, 0.05f, 0.05f, 1.0f};

    while (!surface.shouldClose())
    {
        renderer.computeBegin();
        {
            static bool onceLoad = false;
            if (!onceLoad)
            {
                onceLoad = true;
                renderer.recordCmdBegin(compute_cb);
                {

                    renderer.writeBufferMapped(first.data(), firstBufferCPU, 0, 0, matrixSizeByte);
                    renderer.writeBufferMapped(second.data(), secondBufferCPU, 0, 0, matrixSizeByte);
 
                    renderer.cmdCopyBuffer(compute_cb, firstBufferCPU, firstBufferGPU);
                    renderer.cmdCopyBuffer(compute_cb, secondBufferCPU, secondBufferGPU);

                    jt::BarrierBuilder<2>(renderer)
                        .addBuffer({ .mRegion = { firstBufferGPU, 0, matrixSizeByte},
                                .mSrcAccess = jt::flags::MEMORY_ACCESS_MEMORY_WRITE_BIT,
                                .mDstAccess = jt::flags::MEMORY_ACCESS_SHADER_READ_BIT})
                        .addBuffer({ .mRegion = { secondBufferGPU, 0, matrixSizeByte },
                                .mSrcAccess = jt::flags::MEMORY_ACCESS_MEMORY_WRITE_BIT,
                                .mDstAccess = jt::flags::MEMORY_ACCESS_SHADER_READ_BIT})
                        .setSrcStages(jt::flags::PIPELINE_STAGE_TRANSFER_BIT)
                        .setDstStages(jt::flags::PIPELINE_STAGE_COMPUTE_SHADER_BIT)
                        .cmd(compute_cb);
                    
                    renderer.cmdBindPipeline(compute_cb, compute_pipeline);
                    renderer.cmdBindDescriptorSets(compute_cb, compute_pipeline, {compute_desc_set}, {});
                    renderer.cmdDispatch(compute_cb, MATRIXCOUNT / 64, 1, 1);
                }
                renderer.recordCmdEnd(compute_cb);
            }
        }
        renderer.addCommandBuffer(compute_cb);
        renderer.computeSubmitWait();

        {
            static bool onceLoad = false;
            if (!onceLoad) 
            {
                checkMatricesGPUCPU(outHost, outDataPtr);
                onceLoad = true;
            }
        }
        auto [frame, errDraw] = renderer.drawBegin();
        {
            auto cb = cbs[frame];
            renderer.recordCmdBegin(cb);
            {
                {
                    static bool onceLoad = false;
                    if (!onceLoad)
                    {
                        renderer.writeBufferMapped(vertices.data(), stagingBuffer, 0, 0, 
                                vertices.size() * sizeof(Vertex3D));

                        renderer.cmdLoadVertexData(cb, stagingBuffer, vertexBuffer);
                        onceLoad = true;
                    }

                    renderer.writeBufferMapped(&scaler, scalerBuffer, 0, 0, sizeof(float));
                    static uint64_t rate = 0;
                    if ((rate % 64) == 0)
                    {
                        float* floatPtr = (float*)mappedPtr;
                        printf("%f %f %f %f\n", floatPtr[2*4 + 0], floatPtr[2*4 + 1], floatPtr[2*4 + 2], floatPtr[2*4 + 3]); 
                    }
                    rate++;
                    if (scaler > 1.2f)
                        upScale = false;
                    else if (scaler < 0.8f)
                        upScale = true;

                    if (upScale)
                        scaler += 0.005f;
                    else
                        scaler -= 0.005f;
                }
                renderer.cmdPassBegin(cb, pass, clearColor);
                {
                    renderer.cmdBindPipeline(cb, pipeline);
                    renderer.cmdBindDescriptorSets(cb, pipeline, {graphics_desc_set}, {});

                    renderer.cmdBindVertexBuffer(cb, vertexBuffer, 0);
                    renderer.cmdDraw(cb, vertices.size(), 1);
                }
                renderer.cmdPassEnd(cb, pass);
            }
            renderer.recordCmdEnd(cb);
            renderer.addCommandBuffer(cb);
            
        }
        renderer.drawSubmitLeave();
        surface.pollEvents();
    }

    return 0;
}
