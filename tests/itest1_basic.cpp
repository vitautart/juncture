#include <iostream>
#include <cassert>

#include "flags.hpp"
#include "surface.hpp"
#include "renderer.hpp"
#include "jtlib/codes.hpp"

#define WIDTH 600
#define HEIGHT 400
#define RESIZABLE true

int main()
{
    auto [surface, err1] = jt::Surface::make(WIDTH, HEIGHT, "itest1_basic", RESIZABLE);
    assert(err1 == jt::Code::OK);

    auto [width, height] = surface.getFramebufferSize();
    jt::Renderer::Parameters params = 
    {
        .simultaneousFrames = 1,
    };
    auto [renderer, err2] = jt::Renderer::make({width, height}, surface.getNativeSurface(), "itest1_basic", params);
    assert(err2 == jt::Code::OK);

    surface.setFramebufferResizeCB([r = &renderer](jt::Size2D size)
    {
        r->resize(size);
    });

    auto [pass, err3] = renderer.createPassOnscreen(true, jt::flags::Multisampling::MULTISAMPLING_2);
    assert(err3 == jt::Code::OK);

    auto [cb, err4] = renderer.createCommandBuffer();
    assert(err4 == jt::Code::OK);

    auto [vs, err5] = renderer.createVertexShader("build/simple_trig_vert.spv");
    assert(err5 == jt::Code::OK);

    auto [fs, err6] = renderer.createFragmentShader("build/simple_trig_frag.spv");
    assert(err6 == jt::Code::OK);

    jt::Viewport viewport = { .pos = { 0, 0}, .size = {(uint32_t)width, (uint32_t)height} };

    bool dynamicViewport = true;
    bool testDepth = false;
    bool writeDepth = false;
    bool alphaTransparent = false;

    auto [pipeline, err7] = renderer.createPipelineGraphics(pass, vs, fs, {}, {}, {}, viewport, 
            jt::flags::PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            jt::flags::PolygonMode::POLYGON_MODE_FILL,
            jt::flags::CullMode::CULL_MODE_NONE,
            jt::flags::FrontFace::FRONT_FACE_COUNTER_CLOCKWISE,
            dynamicViewport, testDepth, writeDepth, alphaTransparent);
    assert(err7 == jt::Code::OK);

    float clearColor[] = {0.05f, 0.05f, 0.05f, 1.0f};
    while (!surface.shouldClose())
    {
        auto size = surface.getFramebufferSize();
        jt::Viewport currentViewport = { .pos {0, 0}, .size = size };

        auto [frame, code] = renderer.drawBegin();
        assert(code == jt::Code::OK || code == jt::Code::RENDERER_RESIZE_REQUIRE);
        if (code == jt::Code::RENDERER_RESIZE_REQUIRE)
        {
            auto size = surface.getFramebufferSize();
            renderer.resize(size);
        }
        else
        {
            renderer.recordCmdBegin(cb);
            renderer.cmdPassBegin(cb, pass, clearColor);
            renderer.cmdBindPipeline(cb, pipeline);
            renderer.cmdSetViewport(cb, currentViewport);
            renderer.cmdDraw(cb, 3, 1);
            renderer.cmdPassEnd(cb, pass);
            renderer.recordCmdEnd(cb);
            renderer.addCommandBuffer(cb);
        }
        code = renderer.drawSubmitWait();
        assert(code == jt::Code::OK || code == jt::Code::RENDERER_RESIZE_REQUIRE);
        if (code == jt::Code::RENDERER_RESIZE_REQUIRE)
        {
            auto size = surface.getFramebufferSize();
            renderer.resize(size);
        }
        surface.pollEvents();
    }

    return 0;
}
